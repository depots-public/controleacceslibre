<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2023 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
require __DIR__ . '/vendor/autoload.php'; // autoloader composer
require_once('vendor/villejuif/badge/Config.php');
require_once('vendor/villejuif/badge/Html.php');
require_once('vendor/villejuif/badge/Db.php');
require_once('vendor/villejuif/badge/Debug.php');
require_once('vendor/villejuif/badge/model/Session.php');
require_once('vendor/villejuif/badge/model/Log.php');

/*
 * Découpage de l'url
 *
 * index.php?module/action/id
 */
if (isset($_SERVER['REQUEST_URI'])) {
    // api, navigateur, curl
    $url      = $_SERVER['REQUEST_URI'];
    $url_base = "https://". $_SERVER['HTTP_HOST']. $_SERVER['SCRIPT_NAME'];
    $web = true;
} else {
    // cli
    $url = implode("?", $_SERVER['argv']);
    $web = false;
}    
$url_data = parse_url($url);
$module = $action = $url_query = null;
if (isset($url_data['query'])) {
    $url_query = explode('/', $url_data['query']);
    $module =
        (isset($url_query[0]) && $url_query[0]>'' && preg_match("~^[A-Za-z0-9\-\ ]*$~", $url_query[0]) ? $url_query[0] : null);
    $action =
        (isset($url_query[1]) && $url_query[1]>'' && preg_match("~^[A-Za-z0-9\-\ ]*$~", $url_query[1]) ? $url_query[1] : null);
    $id =
        (isset($url_query[2]) && $url_query[2]>"" && preg_match("~^[0-9]+$~", $url_query[2]) ?  $url_query[2] : null);
}
unset($url, $url_data);
    
// détection du mode d'appel du fichier
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    // api
    $isAjax = true;
}else{
    // navigateur ou cli
    $isAjax = false;
    ob_start();
}

/*
 * Initialisation et ouverture session
 */
$dbs = [
    "mariadb" => new \Badge\Db(\Badge\Config::$dbConfig),
    "mssql"   => new \Badge\Db(\Badge\Config::$dbSQLConfig),
];
$session = new \Badge\Model\Session($dbs);
$objects = ['dbs' => $dbs, 'session' => $session, 'module' => $module];

/*
 * Démarrage page HTML
 */

If($isAjax === false && $web === true){
    _echo(HTML_header($action, $module));
    _echo(HTML_menu($session));
}
/*
 * On bascule vers le bon contrôleur
 */
if (isset($module)) {
    require_once('vendor/villejuif/badge/model/'.ucfirst($module).'.php');
    require_once('vendor/villejuif/badge/controller/'.ucfirst($module).'.php');
} else {
    require_once('vendor/villejuif/badge/model/Session.php');
    require_once('vendor/villejuif/badge/controller/Session.php');
}

If($isAjax === false && $web === true) {
    _echo(HTML_footer());
    ob_end_flush();
}
