# Contrôle d'accès libre

* [Licence libre](LICENSE.md)
* [Présentation et utilisation](README.md)
* [Illustrations et copies d'écran](SCREENSHOT.md)
* [Installation](INSTALL.md)

# Illustrations et copies d'écran

## Identification

![Identification par identifiant et mot-de-passe de l'annuaire de l'organisation](img_doc/badgeAffectation.png "Identification dans l'application reposant sur l'annuaire de l'organisation")

![Une fois identifié, vue sur mon groupe et mes droits](img_doc/identifie.png "Une fois identifié, vue sur mon groupe et mes droits")

## Habilitations

![Liste des groupes d'habilitation](img_doc/habilitations.png "Liste des groupes d'habilitation")

![Edition des habilitations d'un groupe](img_doc/habilitationEdition.png "Edition des habilitations d'un groupe")

## Sites ou groupes de lecteurs

![Liste des sites](img_doc/sites.png "Liste des sites ou gropues de lecteurs")

![Détails d'un site](img_doc/site.png "Détail d'un site")

## Lecteurs

[1] Liste des lecteurs
![Liste des lecteurs](img_doc/lecteurs.png "Liste des lecteurs")

[2] Détails d'un lecteur
![Détails d'un lecteur](img_doc/lecteur.png "Détail d'un lecteur")

[3] Qui a accès à un lecteur
![Les accès permis sur ce lecteur](img_doc/lecteurAcces.png "Les accès à ce lecteur")

## Horaires

![Liste des horaires](img_doc/horaires.png "Liste des horaires")

![Edition d'un profil d'horaires](img_doc/habilitationEdition.png "Edition d'un profil d'horaires")

## Parcours d'usages

Ci-dessous, les différents parcours d'usage pour le suivi et la modification des droits d'accès des badges.

![Parcours d'usages](img_doc/parcours.png "Parcours d'usages")

## Badges

[4] Recherche de badges

### Critères

[5] les critères de recherche de badges
![Critères objet](img_doc/badgeCriteres.png "Critères objet")

![Critères opérateur](img_doc/badgeCriteres2.png "Critères opérateur")

### Résultat et sélections

[6] Résultat de la recherche et [7] Sélection des badges

![Résultat de la recherche et sélection](img_doc/badgeResultat.png "Résultat de la recherche et sélection")

### Affectation de droits

[8] Affectation des droits pour les badges sélectionnés

![Affectation de droits](img_doc/badgeAffectation.png "Affectation de droits")

![Affectation programmée de droits supplémentaires et programmation](img_doc/badgeAffectation2.png "Affectation programmée de droits supplémentaires")

### Information sur un badge

[9] Détail d'un badge et ses droit

![Détail d'un badge](img_doc/badgeInfo.png "Détail d'un badge")

![Actions sur un badge](img_doc/badgeInfoActions.png "Actions sur un badge")

[11] Modification du statut d'un badge

![Edition d'un badge](img_doc/badgeEdition.png "Edition d'un badge")

[10] Recopie de droits d'un badge sur un autre

![Recopie des droits d'un badge sur un autre](img_doc/badgeRecopieDroits.png "Recopie des droits d'un badge sur un autre")

## Journal des accès

![Journal des accès selon l'événement](img_doc/journal.png "Journal des accès selon l'événement")

[13] Notification des changements de droits au responsable de site

![Courriel de notification des changements de droits](img_doc/journalNotification.png "Courriel de notification des changements de droits")

## Logs de l'application

[12] Recherche dans les logs de l'application

![Recherche dans les logs de l'application](img_doc/logs.png "Recherche dans les logs de l'application")

Les logs de l'application sont conservés 12 mois puis supprimés.
