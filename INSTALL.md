# Contrôle d'accès libre

* [Licence libre](LICENSE.md)
* [Présentation et utilisation](README.md)
* [Illustrations et copies d'écran](SCREENSHOT.md)
* [Installation](INSTALL.md)

<h1>Installation</h1>

Il vous faut un serveur Apache2 avec PHP7, ainsi qu'un service MariaDB.

Dans Gnu-Linux Debian
``` apt update
apt install apache2 php php-pdo-mysql php-pdo-odbc wget php-cli php-zip unzip php-ldap
```
Ajouter le client ODBC-MSSQL :
https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver16#debian18
Par exemple en version 11 :
``` sudo su
curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
curl https://packages.microsoft.com/config/debian/11/prod.list > /etc/apt/sources.list.d/mssql-release.list
apt update
ACCEPT_EULA=Y apt-get install -y msodbcsql18
```
ou alors
``` apt install mssql-tools
```

Installation pilote mssql
``` apt install php-pear php-dev
apt install unixodbc unixodbc-dev
pecl install sqlsrv
pecl install pdo_sqlsrv
cd /etc/php/7.4/mods-available
echo "; priority=20 \nextension=sqlsrv.so \n" > sqlsrv.ini
echo "; priority=20 \nextension=pdo_sqlsrv.so \n" > pdo_sqlsrv.ini
cd /etc/php/7.4/apache2/conf.d
ln -s ../../mods-available/sqlsrv.ini 20-sqlsrv.ini
ln -s ../../mods-available/pdo_sqlsrv.ini 20-pdo_sqlsrv.ini
systemctl reload apache2
```

Télécharger le dépot et le déposer dans un dossier, par exemple /var/www/badges
et donner les droits à l'utilisateur Apache2 (www-data sur Debian) de lire et écrire.
``` mkdir /var/www/badges
cd /var/www/badges
chown -R . www-data
```

Configurer le host de apache2 par exemple https://badges.mondomaine.

Le serveur sur lequel sera installé l'application (Apache2-PHP7) devra pouvoir
atteindre le serveur MsSQL sur le port 3306.

Créer une base de données MariaDB et exécuter sql/badges.sql.

Configurer votre serveur web (Apache2 ou autre), de préférence en httpS.

Recopier le fichier vendor/villejuif/badge/Config-dist.php en Config.php

Renseigner le fichier vendor/villejuif/badge/Config.php
Il vous faudra pour cela :
- URL de votre application (https://badges.mondomaine)
- MariaDB : serveur, compte, base (créée ci-dessus)
- MsSQL : serveur, compte et base SenatorFX.
- LDAP : serveur, compte consultation.
  Création du groupe APP_BADGES_TOTAL et ajout d'utilisateurs à ce groupe.
  Optionnellement créer un groupe APP_BADGES_VISU et y ajouter des utilisateurs.
- Adapter le texte de la mention CNIL à votre organisation.

Installation de Composer et des bibliothèques
Dans le dossier de base de l'application :
``` cd /var/www/bagdes
wget -O composer-setup.php http://getcomposer.org/installer
php composer-setup.php --install-sir=/usr/local/bin --filename=composer
sudo -u www-data composer update
```


<h1>Tâches automatiques</h1>

Ajouter un fichier /etc/cron.d/badges avec le contenu suivant :
``` bash
# save right state every 30 minutes every night between 0 and 3 am
11,41 0,1,2 * * *	root /usr/bin/php /var/www/html/badges.mondomaine/index.php etat/save >> /var/log/badges-cron.log
# send diff to responsible
20 3 * * * 		root /usr/bin/php /var/www/html/badges.mondomaine/index.php etat/diff >> /var/log/badges-cron.log
# archive old right states
50 5 * * * 		root /usr/bin/php /var/www/html/badges.mondomaine/index.php etat/archive >> /var/log/badges-cron.log
# differ program
5,20,35,50 * * * *      root /usr/bin/php /var/www/html/badges.mondomaine/index.php prog/differ >> /var/log/badges-cron.log
``` 

En prenant soin d'adapter les chemins.

Vous pouvez adapter l'apparence en modifiant la feuille de style dont le fichier
est 'vendor/villejuif/bagde/css/badges.css' et notamment la référence de l'entête 
'vendor/villejuif/bagde/img/header.png' dans l'entrée 'body header'.

