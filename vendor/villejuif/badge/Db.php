<?php
/*
 * GestionDeBadges
 * 
 * Copyright Copyright 2004-2009
 * - Melanie Bats <melanie POINT bats CHEZ utbm POINT fr>
 * - Thomas Petazzoni <thomas POINT petazzoni CHEZ enix POINT org>
 * 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace Badge;

/*
 * Perform a SQL query
 *
 * @param[in] The SQL query to be performed
 */

class Db
{
    public    
            $pdo,
            $currentStatement;
    private
            $dbConfig;

    public function __construct($dbConfig)
    {
        $this->dbConfig = $dbConfig;
        try {
            $this->pdo = new \PDO($dbConfig['dsn'], $dbConfig['user'], $dbConfig['pass']);
        } catch (\PDOException $e) {
            _echo(HTML_error("Probleme de connexion &agrave; la base de donn&eacute;es sur ".$dbConfig['dsn']." : ".$e->getMessage()));
        }
    }
    
    // Retourne le nom de la table de l'objet indiqué
    public function getTableName(string $object)
    {
        return $this->dbConfig['tables'][$object];
    }

    public function query (string $query, ?array $params = null)
    {
        $this->currentStatement = $this->pdo->prepare($query);
        return $this->currentStatement->execute($params);
    }

    public function insertid () :?int
    {
        return $this->pdo->lastInsertId();
    }

    public function fetchObject ()
    {
        return $this->getStatement()->fetchObject();
    }

    public function fetchAllAsArray ()
    {
        return $this->getStatement()->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function fetchRow ()
    {
        return $this->getStatement()->fetch(\PDO::FETCH_ASSOC);
    }

    public function freeResult () :void
    {
        $this->currentStatement = null;
    }

    // Attention, ne fonctionne pas après un SELECT !
    public function numRows () :int
    {
        return $this->getStatement()->rowCount();
    }
    
    public function getStatement()
    {
        return $this->currentStatement;
    }
    
    public function quoteSmart(string $s) : string {
        //return $this->pdo->quote($s); // ajoute ' au début et à la fin
        return str_replace("'", "''", $s);
    }
    
} // end class
