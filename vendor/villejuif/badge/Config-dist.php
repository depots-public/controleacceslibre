<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace Badge;

date_default_timezone_set('Europe/Paris');
setlocale(LC_ALL, 'fr_FR.UTF-8');
error_reporting(E_ALL); # Devel

class Config{

    /*
     * Paramètres SQL (MySql ou MariaDb)
     */
    public static $dbConfig = [
        'dsn'       => 'mysql:host=mariadb.mondomaine;dbname=badges;port=3306;charset=utf8mb4',
        'user'      => 'identifianbasemariadb',
        'pass'      => 'motdepassebasemariadb',
        'tables'    => array(
            //'badge'     => 'b_badge',
            'groupe'    => 'b_groupe',
            //'horaire'   => 'b_horaire',
            //'journal'   => 'b_journal',
            //'lecteur'   => 'b_lecteur',
            'lecteurS'  => 'b_lecteur',
            'log'       => 'b_log',
            //'profil'    => 'b_profil',
            //'droit'     => 'b_profil_lecteur_horaire',
            'site'      => 'b_site',
            'etat'      => 'b_etat',
            'prog'      => 'b_prog'             // programmation différée des badges
        )
    ];
    /*
     * Paramètres SQL Server
     */
    public static $dbSQLConfig = [
        'dsn'       => 'sqlsrv:server=sqldb.villejuif.fr;Database=SENATOR',
        'user'      => 'identifiantbasesql',
        'pass'      => 'motdepassebasesql',
        'tables'    => array(
            'badge'     => 'Badge',
            'badgeS'    => 'StatutBadge',
            //'groupe'    => null,
            'horaire'   => 'PlageHoraire',
            'horaireC'  => 'Creneau',             // plageHoraireId de plageHoraire
            'horaireS'  => 'PlageHoraireSociete', // plageHoraireId de plageHoraire
            'journal'   => 'Evenement',
            'journalT'  => 'TypeEvenement',
            'lecteur'   => 'Lecteur',
            //'lecteurS'  => 'LecteurSociete',
            //'log'       => null,
            'profil'    => 'Groupe',
            'droit'     => 'GroupeLecteurPlage',    // indexPlage de plageHoraire
            'societe'   => 'Societe',
            'commit'    => 'ModificationParametres'
        )
    ];
    
    /*
     * Configuration LDAP Active directory pour librairie AdLDAP2
     */
    public static $ldapConfig = [
        // Mandatory Configuration Options
        'hosts'            => ['dc1.mondomaine', 'dc2.mondomaine'],
        'base_dn'          => 'dc=mondomaine,dc=fr',
        'username'         => 'admintranet@mondomaine.fr',
        'password'         => 'motdepassecompteldap',

        // Optional Configuration Options
        'schema'           => \Adldap\Schemas\ActiveDirectory::class,
        'account_prefix'   => '',
        'account_suffix'   => '@mondomaine.fr',
        'port'             => 389,
        'follow_referrals' => false,
        'use_ssl'          => false,
        'use_tls'          => false,
        'version'          => 3,
        'timeout'          => 5,

        // Custom LDAP Options
        'custom_options'   => []
    ];

    // Texte de la mention pour la protection des données personnelles
    // affichée ou envoyée par courriel
    public static $mentionCnil = <<<CNIL
<p><i>Les informations recueillies sont n&eacute;cessaires pour la gestion des contrôles d'accès.<br/>
Elles font l'objet d'un traitement informatique et sont destin&eacute;es à la direction de la sécurité,
prévention et médiation de la mairie de Villejuif. En application des articles 39 et suivants de la loi du 6 janvier 1978 modifi&eacute;e,
ainsi que du Règlement général pour la protection des données,
vous b&eacute;n&eacute;ficiez d'un droit d'acc&egrave;s, de rectification et de suppression aux informations
qui vous concernent.<br/>
Si vous souhaitez exercer un de ces droits, veuillez adresser un courrier au responsable de traitement.</i></p>\n
CNIL;
}
