<?php
/*
 * ControleAccesLibre
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

define('I_BADGE', "<i class='fas fa-id-badge'></i>"); //"<img src='img/badge.png' class='font'/>&nbsp;");
define('I_JOURNAL', "<i class='fas fa-book'></i>");
define('I_HORAIRE', "<i class='fas fa-clock'></i>");
define('I_SITE', "<i class='fas fa-city'></i>");
define('I_LECTEUR', "<i class='fas fa-door-closed'></i>");
define('I_GROUPE', "<i class='fas fa-users-cog'></i>");
define('I_LOG', "<i class='fas fa-eye'></i>");
define('I_SESSION', "<i class='fas fa-id-badge'></i>");

function HTML_header($action, $module) : string
{
    $html = <<<HEAD0
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
  <title>
HEAD0;
  $html .= (isset($module) ? ucfirst($module). " - " : ""). (isset($action) ? ucfirst($action). " - " : "");
  $html .= <<<HEAD1
Gestion des contrôles d'accès</title>
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta name="generator" content="Geany 1.33" />

  <link rel="icon" href="vendor/villejuif/badge/img/favicon.ico" />
          
  <link rel="stylesheet" type="text/css" href="vendor/villejuif/badge/js/jquery-ui/jquery-ui.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="vendor/villejuif/badge/css/badges.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="vendor/villejuif/badge/css/all.css" />

  <script src="vendor/villejuif/badge/js/jquery-3.4.1.min.js"></script>
  <script src="vendor/villejuif/badge/js/jquery-ui/jquery-ui.min.js"></script>
</head>

<body>

HEAD1;
  $html .= "  <header id='{$module}'><div id='entete'>\n";
  return $html;
}

function HTML_menu($session) : string
{
    $class="class='menu-item menu-item-type-post_type menu-item-object-page'";
    $html = "<nav><ul id='nav2' class='menu'>\n";

    if (count($session->droits['lireBadges'])>0 || count($session->droits['editBadges'])>0) {
        $html .= " <li id='nav_badge' {$class}><a href='?badge/list'>". I_BADGE. "Badges</a></li>\n";
    }
    if (count($session->droits['journal'])>0) {
        $html .= " <li id='nav_journal' {$class}><a href='?journal/list'>". I_JOURNAL. "Journal</a></li>\n";
    }
    if ($session->droits['horaire']) {
        $html .= " <li id='nav_horaire' {$class}><a href='?horaire/list'>". I_HORAIRE. "Horaires</a></li>\n";
    }
    if ($session->droits['site']) {
        $html .= " <li id='nav_site' {$class}><a href='?site/list'>". I_SITE. "Sites</a></li>\n";
    }
    if ($session->droits['lecteur'] || count($session->droits['lireBadges'])>0) {
        $html .= " <li id='nav_lecteur' {$class}><a href='?lecteur/list'>". I_LECTEUR. "Lecteurs</a></li>\n";
    }
    if ($session->droits['groupe']) {
        $html .= " <li id='nav_groupe' {$class}><a href='?groupe/list'>". I_GROUPE. "Habilitations</a></li>\n";
    }
    if ($session->droits['log']) {
        $html .= " <li id='nav_log' {$class}><a href='?log/list'>". I_LOG. "Logs</a></li>\n";
    }
    if (isset($session) && is_object($session) && $session->isLogon()) {
        $html .= "<li id='nav_logout' {$class}><a href='index.php?session/logout'><i class='fas fa-sign-out-alt'></i>&nbsp;D&eacute;connexion</a></li>\n";
    } else {
        $html .= "<li id='nav_login' {$class}><a href='index.php?session/login'><i class='fas fa-sign-in-alt'></i>&nbsp;Identification</a></li>\n";
    }
    $html .= "</ul></nav></div></header>\n";
    $html .= "<section id='si'>";
    return $html;
}

function HTML_footer() : string
{
    $html = <<<BOTTOM
  </section>

  <footer>
    <p class='copy'>
        <a href='https://gitlab.villejuif.fr/depots-public/controleacceslibre/'>Contrôle d'accès par badges</a>
        <a href='https://gitlab.villejuif.fr/depots-public/controleacceslibre/-/blob/master/LICENSE.md'>GNU-AGPL v3</a> &copy; 2020-2023
        <a href='https://www.villejuif.fr'>Mairie de Villejuif</a>
    </p>
  </footer>

</body>

</html>
BOTTOM;
    return $html;
}

function HTML_message($str) : string
{
    global $web;
    return $web ? "<p class='message'>". $str. "</p>" : "MESSAGE:". $str. "\n";
}

function HTML_error($str) : string
{
    global $web;
    return $web ? "<p class='error'>". $str. "</p>" : "ERREUR: ". $str. "\n";
}

function HTML_debug($str) : string
{
    global $web;
    return $web ? "<code><p class='debug'>". htmlentities($str). "</p></code>" : $str. "\n";
}

/* array (value, display, level)
 */
function HTML_array2options($array, $default, $empty=true)
{
    $html = ($empty==true ? "<option". ($default===null ? " selected" : ""). "></option>" : "");
    foreach($array as $record)
    {
      $html .= "<option value='{$record[0]}'".
        ($record[0]==$default ? " selected" : "").
        ">". str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;", max(0,$record[2]-1)). ($record[2]>0 ? "&#8627;&nbsp;" : ""). $record[1]. "</option>";
    }
    return $html;
}

/* array (value, display, level)
 */
function HTML_array2checkboxes($array, $defaults, $fieldname)
{
    $html = ""; //($empty==true ? "<option". ($default===null ? " selected" : ""). "></option>" : "");
    foreach($array as $record)
    {
      $html .= "<br/><label for='{$fieldname}_{$record[0]}' style='width:220px;'>".
        str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;", max(0,$record[2]-1)). ($record[2]> 1 ? "&#8627;&nbsp;" : ""). $record[1]. "</label>".
        "<input type='checkbox' id='{$fieldname}_{$record[0]}' name='{$fieldname}[{$record[0]}]' value='{$record[0]}'".
        (in_array($record[0], $defaults) ? " checked" : ""). " />";
    }
    return $html;
}

/*
 * array (value, display, level, links, interactive{dragable,dropable})
 */
function HTML_array2list_($array, $_level=0, $keyfrom=0)
{
    $html = "<ul style='min-height:0.5em;'>\n";
    while (isset($array[$keyfrom])) {
      if ($array[$keyfrom][2] == $_level) {
        // on est au bon endroit, on affiche
        $record = $array[$keyfrom];
        $html .= "<li id='_id{$record[0]}' {$record[4]}>{$record[1]}{$record[3]}";
        // on lance le niveau inférieur
        $html .= HTML_array2list($array, $_level+1, $keyfrom+1);
        $html .= "</li>\n";
      }
      $keyfrom++;
    }
    $html .= "</ul>\n";
    return $html;
}

/*
 * array = (id, HTML_label, level, HTML_action, class)
 */
function HTML_array2list($array)
{
    $html = "\n<ul>"; $ul=1;
    $level=0;
    foreach($array as $key=>$record) {
      while ($key!=0 && $record[2]<=$level) {
        $html .= "</ul></li>"; $ul--;
        $level--;
      }
      if ($record[2]>$level) {
        $level++;
      }
      $html .= "\n". str_repeat("  ", $record[2]). "<li id='_id{$record[0]}' {$record[4]}>{$record[1]}{$record[3]}";
      $html .= "<ul". ($record[4]!='class="nodrop"' ? " class='sortable'>" : ">"); $ul++;
    }
    if ($ul>0) {
       $html .= "</ul>\n"; $ul--;
    }
    while (0<$level) {
      $html .= "\n". str_repeat("  ", $level). "</li></ul>\n"; $ul-=1;
      $level--;
    }
    while ($ul>0) {
       $html .= (count($array)>0 ? "</li>" : ""). "</ul>\n"; $ul--;
    }
    return $html;
}

function HTML_array2table($array, $except = array())
{
    $html = "<table><tr>";
    foreach($array[0] as $label=>$null) {
        if (!in_array($label, $except)) {
            $html.= "<th>{$label}</th>";
        }
    }
    $html .= "</tr>";
    $class='odd';
    foreach($array as $record) {
        $html .= "<tr class='$class'>";
        foreach($record as $key=>$val) {
            if (! in_array($key, $except)) {
                $html .= "<td>$val</td>";
            }
        }
        $html .= "</tr>";
        $class = ($class=='odd' ? 'even' : 'odd');
    }
    $html .= "</table>";
    return $html;
}


/*

indent_xhtml(string &$source, string $indenter = ' ')

Function description:
This function is used for indenting (formatting) a messy target xhtml page
into a properly indentet page.
It requires all the tags in the page to be properly closed.
It also formats the stuff between <pre> tags, so beware.


$source
This variable has the original xhtml/xml source in it.
By default its included by reference, but you can take off the & and uncomment
the return line at the bottom to make it return the indentet string without
modifying the old one.

$indenter
This variable is the indenter character(s) used for identing (duh).
example:
indent_xhtml($source, $indenter = ' ');
+-------------------+
|<html>             |
| <head>            |
|  <title>          |
+-------------------+
indent_xhtml($source, $indenter = '     ');
+-------------------+
|<html>             |
|    <head>         |
|        <title>    |
+-------------------+

*/

function HTML_indent($source, $indenter = '  ') 
{
    global $lastIndentLevel, $web;
    if (! $web) {
        return $source;
    }
    // Remove all pre-existing formatting.
    // Remove all newlines.
    $source = str_replace("\n", '', $source);
    $source = str_replace("\r", '', $source);
    // Remove all tabs.
    $source = str_replace("\t", '', $source);
    // Remove all space after ">" and before "<".
    $source = preg_replace("/>( )*/", ">", $source);
    $source = preg_replace("/( )*</", "<", $source);

    // Iterate through the source.
    $level = isset($lastIndentLevel) ? $lastIndentLevel : 0;
    $source_len = strlen($source);
    $array = array();
    $pt = 0;
    while ($pt < $source_len) {
        if ($source[$pt] === '<') {
            // We have entered a tag.
            // Remember the point where the tag starts.
            $started_at = $pt;
            $tag_level = 1;
            // If the second letter of the tag is "/", assume its an ending tag.
            if ($source[$pt+1] === '/') {
                $tag_level = -1;
            }
            // If the second letter of the tag is "!", assume its an "invisible" tag.
            if ($source[$pt+1] === '!') {
                $tag_level = 0;
            }
            // Iterate throught the source until the end of tag.
            while ($source[$pt] !== '>') {
                $pt++;
            }
            // If the second last letter is "/", assume its a self ending tag.
            if ($source[$pt-1] === '/') {
                $tag_level = 0;
            }
            $tag_lenght = $pt+1-$started_at;

            // Decide the level of indention for this tag.
            // If this was an ending tag, decrease indent level for this tag..
            if ($tag_level === -1) {
                $level--;
            }
            // Place the tag in an array with proper indention.
            $array[] = str_repeat($indenter, $level).substr($source, $started_at, $tag_lenght);
            // If this was a starting tag, increase the indent level after this tag.
            if ($tag_level === 1) {
                $level++;
            }
            // if it was a self closing tag, dont do shit.
        }
        // Were out of the tag.
        // If next letter exists...
        if (($pt+1) < $source_len) {
            // ... and its not an "<".
            if ($source[$pt+1] !== '<') {
                $started_at = $pt+1;
                // Iterate through the source until the start of new tag or until we reach the end of file.
                while ($source[$pt] !== '<' && $pt < $source_len) {
                    $pt++;
                }
                // If we found a "<" (we didnt find the end of file)
                if ($source[$pt] === '<') {
                    $tag_lenght = $pt-$started_at;
                    // Place the stuff in an array with proper indention.
                    $array[] = str_repeat($indenter, $level).substr($source, $started_at, $tag_lenght);
                }
            // If the next tag is "<", just advance pointer and let the tag indenter take care of it.
            } else {
                $pt++;
            }
        // If the next letter doesnt exist... Were done... well, almost..
        } else {
            break;
        }
    }
    // Replace old source with the new one we just collected into our array.
    $source = implode("\n", $array);
    $lastIndentLevel = $level;
    return $source. "\n";
}

function _h2($string) : string
{
    return "<h2>{$string}</h2>";
}

function _h3($string) : string
{
    return "<h3>{$string}</h3>";
}

function _echo($xml) : void
{
    echo HTML_indent($xml, '  ');
}
