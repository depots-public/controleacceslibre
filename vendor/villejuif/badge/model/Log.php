<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace Badge\Model;
/*
 * Gestion des logs de cette application
 * Couvre les modifications mais aussi les recherches
 */

class Log {
    public 
            $id,
            $datetime,    # quand
            $par,         # qui a fait
            $action,      # quoi
            $sur_objet,   # sur quel objet
            $sur_id,      # dont l'id est
            $sur_valeur,  # valeur affectée ou résultante
            $filter, // array
            $error   = false, 
            $message = "";

    private
            $dbs,
            $objects;
    
    public function __construct(array $objects)
    {
        $this->objects = $objects;
        $this->dbs     = $objects['dbs'];
        $this->error   = false;
        $this->message = "";
        
        $this->filter = array();
        
        return $this;
    }

    private function save() : bool
    {
        $sql = ($this->id>0 ? "UPDATE " : "INSERT INTO ");
        $sql .= "{$this->dbs['mariadb']->getTableName('log')} SET " .
          "par=?,".
          "action=?,".
          "sur_objet=?,".
          "sur_valeur=?";
        $params = [$this->par, $this->action, $this->sur_objet, json_encode($this->sur_valeur)];
        if ($this->sur_id != "" || $this->sur_id > 0) {
          $sql .= ", sur_id=?";
          $params[] = $this->sur_id;
        }
        if ($this->id>0) {
          $sql .= " WHERE id=?";
          $params[] = $this->id;
        }        
        $ret = $this->dbs['mariadb']->query($sql, $params);
        if ($ret === false) {
            $this->error = true;
            $this->message = HTML_error("Impossible d'enregistrer les logs");
        }
        if ($this->id == 0) {
          $this->id = $this->dbs['mariadb']->insertid();
        }
        return ! $this->error;
    }

    public function get() : bool
    {
        $query = "SELECT * ".
            "FROM {$this->dbs['mariadb']->getTableName('log')} ".
            "WHERE id=?";
        $this->dbs['mariadb']->query ($query, [$this->id]);
        if (! ($record = $this->dbs['mariadb']->fetchObject()))
        {
            $this->message = "Aucun log avec l'id ". $this->id;
            $this->error = true;
        } else {
          $this->par         = $record->par;
          $this->action      = $record->action;
          $this->sur_objet   = $record->sur_objet;
          $this->sur_id      = $record->sur_id;
          $this->sur_valeur  = "<div class='json'>". nl2br(
            str_replace(' ', '&nbsp;',
              json_encode(
                json_decode($record->sur_valeur, true), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
              )
            )).
           "</div>";

          $this->datetime = date('d/m/Y H:i:s', strtotime($record->datetime));

          $this->dbs['mariadb']->freeResult();
          $this->message = "";
          $this->error = false;
        }
        return ! $this->error;
    }

    public function add(string $action, string $objet, $id = null, $valeur = null, string $par = "") : bool
    {
        global $web;
        $this->par = ($par != '') ? $par : (isset($_SESSION['login']) ? $_SESSION['login'] : ($web ? $_SERVER["REMOTE_ADDR"] : "cli"));
        $this->action = $action;
        $this->sur_objet = $objet;
        $this->sur_id = $id;

        if (is_object($valeur)) {
          $this->sur_valeur = clone $valeur;
          unset(//$this->sur_valeur->dbs,
            //$this->sur_valeur->objects,
            $this->sur_valeur->site,
            $this->sur_valeur->profil
            /*$this->sur_valeur->listeStatuts,
            $this->sur_valeur->listeOperateurs,
            $this->sur_valeur->listeChamps*/
            );
        } else {
          $this->sur_valeur = $valeur;
        }

        return $this->save();
    }

    public function HTML_view() : string
    {
        $html = "<fieldset><legend>Log - n° {$this->id}</legend>";

        $fieldList = array('datetime'=>"Quand",
          'par'=>"Acteur",
          'action'=>"Action",
          'sur_objet'=>"Sur",
          'sur_id'=>"Id",
          'sur_valeur'=>"Valeur"
        );
        foreach ($fieldList as $fieldIndex=>$fieldName) {
          if ($this->$fieldIndex > '') {
            $html .= "<p><label class='inline' for='__{$fieldIndex}'>{$fieldName} :</label> ".
             "<strong>".
             $this->$fieldIndex.
             "</strong></p>";
          }
        }
        $html .= "</fieldset>\n";

        return $html;
    }

    private function _valueList($champ)
    {
        $rep = array();

        $query = "SELECT $champ as val FROM {$this->dbs['mariadb']->getTableName('log')} ".
            "WHERE $champ <> '' ".
            "GROUP BY $champ ORDER BY $champ;";
        $ret = $this->dbs['mariadb']->query ($query);
        if ($ret === false) {
            $this->error=true;
            $this->message .= HTML_error("Impossible de lire les logs");
        } else {
          while ($record = $this->dbs['mariadb']->fetchObject($ret)) {
            $rep[] = $record->val;
          }
          $this->dbs['mariadb']->freeResult($ret);
        }
        return ($this->error ? false : $rep);
    }

    private function _optionList($champ, $defaultValue) : string
    {
        $rep = $this->_valueList($champ);
        $html = "<option></option>"; // Choisir $champ
        foreach($rep as $val) $html .= "<option". ($val==$defaultValue ? " selected='selected'" : ""). ">$val</option>";
        return $html;
    }

    private function _initFilter() : void {
        foreach (
            array(
                'id', 'avant', 'apres', 'par', 'action', 'sur_objet', 'sur_id'
            ) as $field
        ) {
            if (!isset($this->filter[$field])) $this->filter[$field]=null;
        }
        
    }

    public function HTML_searchForm() : string
    {
        $this->_initFilter();
        $html = "<fieldset><legend>Filtres sur les logs de l'application</legend>";
        $html .= "<form method='post' action='index.php?log/list/'>".
          "<label for='__id'>Id du log</label><input id='__id' type='text' name='_filter[id]' value='". $this->filter['id']. "'><br />".
          "<label for='__apres'>Entre le</label><input id='__apres' type='date' name='_filter[apres]' value='". $this->filter['apres']. "'>".
          "<label for='__avant' class='inline'>et le</label><input id='__avant' type='date' name='_filter[avant]' value='". $this->filter['avant']. "'>".
          "<label for='__par' class='inline'>par</label><select id='__par' name='_filter[par]'>". $this->_optionList('par', $this->filter['par']). "</select><br/>".

          "<label for='__action'>Action</label><select id='__action' name='_filter[action]'>". $this->_optionList('action', $this->filter['action']). "</select>".
          "<label for='__objet' class='inline'>sur l'objet</label><select id='__objet' name='_filter[sur_objet]'>". $this->_optionList('sur_objet', $this->filter['sur_objet']). "</select>".
          "<label for='__sur_id' class='inline'>dont l'id est</label><input id='__sur_id' name='_filter[sur_id]' value='". $this->filter['sur_id']. "'><br />".
          "<input type='submit' value='Mettre à jour la liste des résultats'/>";
        $html .= "</form></fieldset>";
        return $html;
    }

    public function HTML_searchResult($title = "Résultats") : string
    {
        if (! $this->objects['session']->droits['log']) {
          $this->message = "Vous n'avez pas l'habilitation suffisante.";
          return $this->message;
          return false;
        }
        $html = "<div id='log_details'></div>";
        $html .= "<fieldset id='log_results'><legend>{$title}</legend>";
        if ($this->filter) {
            if ($res = $this->_searchQuery()) {
                $list_id = $this->_searchList($res);
                if (count($list_id) == 0) {
                    $html .= HTML_message("Aucun log avec ces filtres");
                } else {
                    //$html .= "<script type='text/javascript'>var logList = ". json_encode($res). ";</script>";
                    $html .= "<script type='text/javascript' src='vendor/villejuif/badge/js/log.js'></script>";
                    $html .= HTML_array2table($list_id);
                    if (count($list_id) == 101) {
                        $html .= HTML_message("Il y a peut-être d'autres réponses, mais seules 100 sont affichées.");
                    }
                }
            }
        }
        $html .= "</fieldset>";
        return $html;
    }

    /*
     * execute une requête en fonction des filtres
     * id (int) : id du log
     * par (str) : personne qui a réalisé l'action
     * action (str) : nom de l'action appelée, par exemple "save"
     * sur_objet (str) : nom du type d'objet, par exemple "badge", "groupe", "profil"
     * sur_id (str) : id de l'objet - c'est une chaine parce que les numéros
     *            des badges dépasse les nombres autorisés en (int)
     * Les filtres s'ajoutent les uns aux autres et sont tous optionnels.
     * Le résultat se limite aux 100 derniers logs.
     */
    private function _searchQuery()
    {
        $query = "SELECT id FROM {$this->dbs['mariadb']->getTableName('log')} ".
          " WHERE (0=0)";
        $params = array();
        foreach(array('id', 'par', 'action', 'sur_objet', 'sur_id') as $field) {
          if (array_key_exists($field, $this->filter) && $this->filter[$field] != '') {
            $query .= " AND {$field}=:{$field}";
            $params[$field] = $this->filter[$field];
          }
        }
        if (isset($this->filter['apres']) && $this->filter['apres'] != '') {
          $query .= " AND DATE(datetime)>= :apres";
          $params['apres'] = $this->filter['apres'];
        }
        if (isset($this->filter['avant']) && $this->filter['avant'] != '') {
          $query .= " AND DATE(datetime)<= :avant";
          $params['avant'] = $this->filter['avant'];
        }
        $query .= " ORDER BY datetime DESC LIMIT 100";
        $ret = $this->dbs['mariadb']->query ($query, $params);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Erreur dans la requête "). HTML_debug($query);
        } else {
            $rep = array();
            while ($record = $this->dbs['mariadb']->fetchObject($ret)) {
                $rep[] = $record->id;
            }
            $this->dbs['mariadb']->freeResult($ret);
        }
        return ($this->error ? false : $rep);
    }

    private function _searchList($list) :array
    {
        $array = array();
        foreach ($list as $id) {
          $this->id = $id;
          $this->get();
          $array[] = array(
            'Log_Id'=> $this->sur_valeur>'' ?
              "<a href='#log_details' onclick='javascript:detail({$this->id});' title='Afficher le détail'>". $this->id ."</a>" :
              $this->id,
            'Date'=>$this->datetime,
            'Par'=>$this->par,
            'Action'=>$this->action,
            'Objet'=>$this->sur_objet,
            'Id'=>$this->sur_id
          );
        }

        return $array;
    }

    /*
     * appelé après l'envoi d'un formulaire de filtres
     * charge  la propriété $this->filter avec les valeurs des champs
     */
    function fromSearchForm()
    {
        if (!isset($_POST['_filter']) | !is_array($_POST['_filter'])) return false;

        foreach(
            array(
                'id'=>FILTER_SANITIZE_NUMBER_INT,
                'avant'=>FILTER_SANITIZE_STRING,
                'apres'=>FILTER_SANITIZE_STRING,
                'par'=>FILTER_SANITIZE_STRING,
                'action'=>FILTER_SANITIZE_STRING,
                'sur_objet'=>FILTER_SANITIZE_STRING,
                'sur_id'=>FILTER_SANITIZE_NUMBER_INT
            ) as $field=>$sanitize) {
          $this->filter[$field] = $_POST['_filter'][$field]>"" ? filter_var($_POST['_filter'][$field], $sanitize) : "";
        }
    }

    
    public function archivage() : string
    {
        $query = "DELETE FROM {$this->dbs['mariadb']->getTableName('log')} ".
            "WHERE datetime < ?";
        $values = array(date('Y-m-d', strtotime("-12 month")));
        
        $result = $this->dbs['mariadb']->query ($query, $values);
        
        $n = $this->dbs['mariadb']->numRows();

        if ($result === false) {
            $this->message .= "Problème dans la requête d'archivage\n";
            $this->error = true;
            return "Erreur.";
        } else {
            return "Nombre d'enregistrements supprimés : ". $n. "\n";
        }
    }

}
