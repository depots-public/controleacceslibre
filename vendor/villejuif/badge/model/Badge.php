<?php

/*
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2023 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

namespace Badge\Model;

require_once('Horaire.php');
require_once('Lecteur.php');
require_once('Profil.php');
require_once('Site.php');
require_once('Prog.php');

class Badge {

    public
        $numero, // le numéro sert d'identifiant unique dans la base ; c'est une chaîne et pas un nombre (en fait, un bigint)
        $nom, $prenom, // porteur du badge
        $profil_id, // profil de droits du badge
        $statut,
        $dateDebutValidite, $dateFinValidite, // au format datetime
        $profil, // objet profil
        $error = false,
        $message = "";
    public static         // recopie des éléments qui sont dans la table StatutBadge
    /* liste des statuts des badges. Tout changement ici doit être également
     * reporté dans badge.js dans la fonction enableAutocompleteStatut()
     */
        $listeStatuts = [// TODO : alimenter ce tableau avec ce qui est dans la table
            0 => "En service",
            1 => "Suspendu",
            2 => "Volé"];
    private
        $dbs,
        $objects;
    private static
        $sentVarToJs = false;

    /*
     * la liste des opérateurs sert dans les filtres sur les badges
     * court : affichage en html de l'opérateur
     * conjonction : qui associe l'opérateur dans la requête SQL
     * sql : écriture en SQL de l'opérateur %s est la valeur recherchée
     *
     * Attention, les opérateurs supérieur ou égal >= et inférieur < sont
     * convertis dans la fonction resultQuery_result en ligne 520.
     * égal et différents sont aussi convertis en lignes 542 et 557
     */
    public static $listeOperateurs = [
        "contient" => array(
            'court' => "&ni;",
            'conjonction' => "OR",
            'sql' => " LIKE '%%%s%%'"
        ),
        "égal" => array(
            "court" => "=",
            "conjonction" => "OR",
            "sql" => "='%s'"
        ),
        "commence par" => array(
            "court" => "=%",
            "conjonction" => "OR",
            "sql" => " LIKE '%s%%'"
        ),
        "différent" => array(
            "court" => "&ne;",
            "conjonction" => "AND",
            "sql" => "&lsaquo;&rsaquo;'%s'"
        ),
        "ne contient pas" => array(
            "court" => "&notin;",
            "conjonction" => "AND",
            "sql" => " NOT LIKE '%%%s%%'"
        ),
        "supérieur à" => array(
            "court" => "&ge;",
            "conjonction" => "OR",
            "sql" => "&rsaquo;='%s'"
        ),
        "inférieur à" => array(
            "court" => "&le;",
            "conjonction" => "OR",
            "sql" => "&lsaquo;'%s'"
        ),
        "est vide" => array(
            "court" => "&empty;",
            "conjonction" => "OR",
            "sql" => "=''"
        ),
        "n‘est pas vide" => array(
            "court" => "!&empty;",
            "conjonction" => "AND",
            "sql" => "&lsaquo;&rsaquo;''"
        )
    ];
    /*
     * la liste des champs permet aussi de construire les filtres
     *
     * objet : {
     *   tableSql : nom de la table SQL
     *   champSql : champ(s) de la table dans le(s)quel(s) on cherche la valeur
     *   champQslId : champ de l'id unique de la table pour autocompletion
     * }
     */
    public static $listeChamps = [
        "badge" => array(
            "tableSql" => "Badge",
            "champSql" => ["numeroBadge", "nom", "prenom"],
            "champSqlId" => "numeroBadge"
        ),
        "site" => array(
            "tableSql" => "Lecteur",
            "champSql" => "lecteurId",
            "champSqlId" => "lecteurId"
        ),
        "lecteur" => array(
            "tableSql" => "Lecteur",
            "champSql" => "libelle",
            "champSqlId" => "lecteurId"
        ),
        "horaire" => array(
            "tableSql" => "PlageHoraire",
            "champSql" => "libelle",
            "champSqlId" => "plageHoraireId"
        ),
        "statut" => array(
            "tableSql" => "StatutBadge",
            "champSql" => "libelle",
            "champSqlId" => "statutBadgeId"
        )
    ];

    public function __construct(array $objects) {
        $this->objects = $objects;
        $this->dbs = $objects['dbs'];
        //self::$listeOperateurs = JSON_decode(self::$listeOperateurs, true, 10,  JSON_THROW_ON_ERROR);
        //self::$listeChamps = JSON_decode(self::$listeChamps, true, 5,  JSON_THROW_ON_ERROR);
        $this->profil = new Profil($objects);
    }

    /*
     * charge l'objet des valeurs récupérées dans la base à partir de l'id
     * retourne true/false selon le succès
     */

    public function get() : bool {
        $query = "SELECT b.*, bs.libelle AS statut " .
                "FROM {$this->dbs['mssql']->getTableName('badge')} AS b " .
                "LEFT JOIN {$this->dbs['mssql']->getTableName('badgeS')} AS bs ON b.statutId=bs.statutBadgeId " .
                "WHERE numeroBadge=?";
        $this->dbs['mssql']->query($query, [$this->numero]);
        if (!($record = $this->dbs['mssql']->fetchObject())) {
            $this->message = "Aucun badge avec le numéro " . $this->numero;
            $this->error = true;
            return false;
        }
        $this->nom = $record->nom;
        $this->prenom = $record->prenom;
        $this->profil_id = $record->groupeId;
        $this->profil = new Profil($this->objects);
        $this->profil->id = $this->profil_id;
        $this->statut = $record->statut;
        $this->dateDebutValidite = $record->dateDebutValidite;
        $this->dateFinValidite = $record->dateFinValidite;
        $this->datemaj = $record->dateModification;

        $this->dbs['mssql']->freeResult();

        $this->message = "";
        $this->error = false;
        return true;
    }

    /*
     * met à jour le statut et le profil du badge dans la base
     * retourne true/false selon succès
     */

    public function save() : bool {
        if ($this->numero == 0) {
            // on ne crée pas de badge dans cette application
            $this->message .= HTML_error("Erreur pas de numéro de badge");
            _echo($this->message);
            die();
        }
        // retrouver le code statut
        $statutId = array_search($this->statut, self::$listeStatuts);
        // construction de la requête
        $sql = "UPDATE " . $this->dbs['mssql']->getTableName('badge') .
                " SET groupeId=?, statutId=?, dateDebutValidite=?, dateFinValidite=?" .
                " WHERE numeroBadge=?";
        $ret = $this->dbs['mssql']->query($sql, [
            $this->profil_id,
            $statutId,
            $this->dateDebutValidite > "" ? $this->dateDebutValidite : NULL,
            $this->dateFinValidite > "" ? $this->dateFinValidite : NULL,
            $this->numero
        ]);

        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible d'enregistrer le badge");
        } else {
            /* if ($this->numero==0) { // pas de création de badge dans cette application
              $this->numero = $this->dbs['mssql']->insertid();
              } */
            $this->commit();
            // enregistrer dans les logs
            $log = (new log($this->objects))->add('save', 'badge', $this->numero, $this);
            unset($log);
        }
        //var_dump($this->dateDebutValidite, $this->dateFinValidite);
        return !$this->error;
    }

    /*
     * Commande la mise à jour du badge sur les centrales
     *
     * Uniquement modification d'un badge (ni création, ni suppression)
     */

    private function commit(): bool {
        // commit avec le numéro badge
        $sql = "INSERT INTO " . $this->dbs['mssql']->getTableName('commit') .
                " (zoneModifiee, numeroBadge, action)" .
                " VALUES (0, ?, 2)";
        $ret = $this->dbs['mssql']->query($sql, [$this->numero]);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de commande la mise à jour sur les centrales");
        }
        return !$this->error;
    }

    private function HTML_sourceCopie(): string {
        $html = "<aside id='_badgeRef' class='hidden'>"
                . "<fieldset><legend>Sélection d'un " . I_BADGE . "badge source</legend>";
        // champ autocomplétion
        $html .= "<form>";
        $html .= "<input type='hidden' id='_champ' name='_champ' value='badge' />";
        $html .= "<input type='hidden' id='_operateur' name='_operateur' value='égal' />";

        $html .= "<label for='_valeur' class='inline'>Nom, prénom ou numéro</label> : "
                . "<input id='_valeur' name='_valeur' type='text' onkeypress='javascript:selectValeur(this);' />"
                . "<span id='_loading' class='loading' style='display:none;'><i class='fas fa-circle-notch fa-spinner'></i></span>"
                . "<input id='_vid' name='_vid' type='hidden' />"
                . "<div id='_valeur-container' style='display:inline'></div>";
        $html .= "<input type='button' id='_add' href='#' onclick='javascript:copyShowRights(this); return false;'"
                . " class='hidden button' value='Voir ses droits' />";
        $html .= "<p id ='_copy'><input type='button' href='#' onclick='javascript:copyRights(this); return false;'"
                . " disabled value='Recopier les droits' />";
        $html .= "<input id='_q' name='_q' type='hidden' /><div id='_qtext' class='hidden'></div>";

        $html .= $this->_HTML_sendVarToJs();
        // on envoie le filtre par défaut
        $html .= <<<SCRIPT01
<script type='text/javascript'>
  var a = { badge: {
            'n‘est pas vide' : [ 'n‘est pas vide' ]
            }
          };
  document.getElementById('_q').value = JSON.stringify(a);
  /* select2text(); */
  selectAutocomplete();
</script>
SCRIPT01;

        $html .= "</form>";
        $html .= "</fieldset>";

        $html .= "<div id='_badgeRefProfil' class='new'><span id='_resultLoading' class='loading' style='display:none'>"
                . "<i class='fas fa-circle-notch fa-spinner'></i></span></div>";
        $html .= "</aside>";
        return $html;
    }

    public function HTML_view(): string {
        if ($this->objects['session']->is_allowed('editBadges', 0)) {
            $html = $this->HTML_sourceCopie();
        } else {
            $html = $this->_HTML_sendVarToJs();
        }
        $html .= "<fieldset><legend>" . I_BADGE . "Badge {$this->numero}</legend>";
        $html .= "<input id='_badgeComplet' type='hidden' value=\"{$this->numero} {$this->nom} {$this->prenom}\" />";
        $html .= "<input id='_badgeNumero' type='hidden' value=\"{$this->numero}\" />";
        $html .= "<p><label for='__numero' class='inline'>Numéro</label> : {$this->numero} / ". dechex($this->numero). "</p>";
        $html .= "<p><label for='__nom' class='inline'>Nom</label> : {$this->nom}<br/>";
        $html .= " <label for='__prenom'  class='inline'>Prénom</label> : {$this->prenom}</p>";

        if ($this->dateDebutValidite != "" || $this->dateFinValidite != "") {
            $html .= "<p><label for='__validite' class='inline'>Dates validité :</label> ";
            if ($this->dateDebutValidite != "") {
                $html .= " depuis le " . date('d/m/Y H:i:s', strtotime($this->dateDebutValidite));
            }
            if ($this->dateFinValidite != "") {
                $html .= " jusqu'au " . date('d/m/Y H:i:s', strtotime($this->dateFinValidite));
            }
            $html .= "<p>";
        }

        // Statut
        $html .= "<p><label for='__statut' class='inline'>Statut</label> : {$this->statut}";
        if ($this->objects['session']->is_allowed('editBadges', 0)) {
            $html .= " <a class='button' href='?badge/edit/{$this->numero}'>Modifier " . I_BADGE . "le statut</a>";
        }
        $html .= "</p>";

        // Programmation
        $prog = new Prog($this->objects);
        $prog->numeroBadge = $this->numero;
        if ($prog->getBadge()) {
        $html .= "<p>Ce badge fait l'objet d'une programmation, il est donc verouillé à la modification ({$prog->depuis}->{$prog->jusque}).";
            if ($prog->statut == 'avant') {
                $html .= "<br/><a class='button' href='?prog/cancel/{$this->numero}'>Annuler la programmation</a>";
            } else {
                $html .= "<br/><a class='button' href='?prog/forestall/{$this->numero}'>Avancer la fin de la programmation</a>";
            }
        $html .= "</p>";
        }

        $html .= "</fieldset>";
        
        // Exposer le profil
        //$this->profil->get();
        $html .= "<div id='actualRights'><span id='_resultLoading' class='loading' >" .
                "<i class='fas fa-circle-notch fa-spinner'></i></span></div>";
        //$html .= $this->profil->HTML_view();
        $html .= "<script type='text/javascript'>displayHtmlRights('#actualRights', {$this->numero});</script>";

        // proposer d'appliquer les mêmes droits que quelqu'un d'autre
        if ($this->objects['session']->is_allowed('editBadges', 0)) {
            $html .= "<p><a class='button' href='?badge/editRightsOne/{$this->numero}'>Modifier les droits</a> ";
            $html .= "<a class='button' href='#' onClick='javascript:copyDisplaySide();'>Ecraser les droits depuis un autre badge</a></p>";
        }

        // Proposer lien vers journal pour ce badge
        if ($this->objects['session']->is_allowed('journal')) {
            $html .= "<p><a class='button' href='?journal/list/&_filter[badge]={$this->numero}'>Voir le journal d'accès.</a></p>";
        }

        return $html;
    }

    function HTML_form() {
        $html = "<form name='badge' method='post' action='?badge/fromForm/{$this->numero}'>";
        $html .= "<fieldset><legend>Badge";
        if ($this->numero)
            $html .= " - n° {$this->numero}<input type='hidden' name='__numero' value='{$this->numero}' />";
        $html .= "</legend>";
        $html .= "<p><label for='__nom' class='inline'>Nom<em>*</em></label> : <input type='text' id='__nom' name='__nom' value=\"{$this->nom}\" required minlength='3' readonly /><br/>";
        $html .= " <label for='__prenom'  class='inline'>Prénom<em>*</em></label> : "
                . "<input type='text' id='__prenom' name='__prenom' value=\"{$this->prenom}\" required minlength='3' readonly /></p>";

        $html .= "<p><label for='__dateDebutValidite'>Date de début de validité</label> : " .
                "<input type='datetime-local' name='__dateDebutValidite' id='__dateDebutValidite' value='{$this->dateDebutValidite}' />";
        $html .= "<br/><label for='__dateFinValidite'>Date de fin de validité</label> : " .
                "<input type='datetime-local' name='__dateFinValidite' id='__dateFinValidite' value='{$this->dateFinValidite}' /></p>";

        $html .= "<p><label for='__statut' class='inline'>Statut<em>*</em></label> : <select id='__statut' name='__statut'>";
        foreach (self::$listeStatuts as $key => $statut) {
            $html .= "<option value='{$key}' " . ($this->statut == $statut ? " selected" : "") . ">{$statut}</option>";
        }
        $html .= " </select></p>";
        $html .= "</fieldset>\n";

        $html .= " <input type='submit' value='Enregistrer' /> <a class='button' "
                . "href='?{$this->objects['module']}/list'>Retour à la liste</a>";
        $html .= "</form>\n";
        return $html;
    }

    /*
     * appelé après l'envoi d'un formulaire
     * charge l'objet avec les valeurs des champs
     * retourne le résutat du contrôle sur les valeurs
     */

    function fromForm(): bool {
        //var_dump($_POST);
        if (isset($_POST['__numero']))
            $this->numero = $_POST['__numero'];
        // nom
        $this->nom = filter_var($_POST['__nom'], FILTER_SANITIZE_STRING);
        // prénom
        $this->prenom = filter_var($_POST['__prenom'], FILTER_SANITIZE_STRING);
        // dates validité
        foreach (array(
            'dateDebutValidite' => FILTER_SANITIZE_STRING,
            'dateFinValidite' => FILTER_SANITIZE_STRING,
        ) as $field => $sanitize) {
            $this->$field = isset($_POST["__" . $field]) 
                    && $_POST["__" . $field] > "" ? filter_var($_POST["__" . $field] . ':00', $sanitize) : "";
        }
        // statut
        $this->statut = self::$listeStatuts[$_POST['__statut']];
        //echo "</br/>";
        //var_dump($this);
        //die();
        return !$this->error;
    }

    private function _HTML_sendVarToJs(): string {
        if (self::$sentVarToJs)
            return "";
        self::$sentVarToJs = true;

        $html = "";
        // pour affichage identique entre PHP et JS, on envoie au javascript
        // les pictos des types d'objets à afficher en html
        echo "<script type='text/javascript'>var I_BADGE = \"" . I_BADGE . "\";</script>\n";
        echo "<script type='text/javascript'>var I_SITE = \"" . I_SITE . "\";</script>\n";
        echo "<script type='text/javascript'>var I_LECTEUR = \"" . I_LECTEUR . "\";</script>\n";
        echo "<script type='text/javascript'>var I_HORAIRE = \"" . I_HORAIRE . "\";</script>\n";

        $html .= "<script type='text/javascript'>" .
                "var listeChamps = " . html_entity_decode(json_encode(self::$listeChamps)) . ";" .
                "</script>" .
                "<script type='text/javascript'>" .
                "var listeOperateurs = " . json_encode(self::$listeOperateurs) . ";" .
                "</script>";
        $lecteur = new Lecteur($this->objects);
        // on envoie au javascript l'ensemble des lecteurs
        $html .= "<script type='text/javascript'>var lecteurs = " .
                $lecteur->JSON_array($lecteur->array_listInSite(null, true)) .
                ";</script>";
        if ($lecteur->error) {
            $this->message .= $lecteur->message;
        }
        $site = new Site($this->objects);
        // on envoie au javascript l'ensemble des sites (et groupes de lecteurs)
        $html .= "<script type='text/javascript'>var sites = " .
                $site->JSON_array($site->array_list(true)) .
                ";</script>";
        $horaire = new Horaire($this->objects);
        // on envoie au javascript l'ensemble des "profils" horaires
        $listeHoraires = $horaire->array_list(false);
        // ajout de l'horaire "Jamais"
        $listeHoraires[-1] = array(-1, "Jamais");
        $html .= "<script type='text/javascript'>var horaires = " .
                $horaire->JSON_array($listeHoraires) .
                ";</script>";
        unset($lecteur, $site, $horaire);

        // appel au long javascript qui rend dynamique la page
        $html .= "<script type='text/javascript' src='vendor/villejuif/badge/js/badge.js'></script>";
        // on envoie le contenu de listeChamps au javascript
        $html .= "<script type='text/javascript'>"
                . "var listeChamps = " . html_entity_decode(json_encode(self::$listeChamps)) . ";"
                . "</script>"
                // on envoie le contenu de listeOpérateurs au javascript
                . "<script type='text/javascript'>"
                . "var listeOperateurs = " . json_encode(self::$listeOperateurs) . ";"
                . "</script>";

        return $html;
    }

    /* la page est organisé en zones correspondant aux étapes :
     *
     * | filtre(select)    | sélection | affectation | lecteurs(inchangés) |
     * | résultats |
     *
     * filtre : là où on construit le filtre
     * résultat : où on voit le résultat du filtre
     * sélection : les bagdes sélectionné parmi les résultats (cumulatifs)
     * affectation : les droits qui seront affectés aux badges selon les habilitations
     * lecteurs : lecteurs disponibles, ceux qui n'affectent pas les badges (qui resteront
     *    inchangés lors de la validation)
     */

    function HTML_filterForm() {
        $html = "<form method='get'>";
        $html .= "<fieldset><legend>Critères</legend>";
        // champs
        $html .= "<select id ='_champ' style='width:5em;' name='_champ' onchange='javascript:selectChamp(this);'>";
        $html .= "<option></option>";
        foreach (self::$listeChamps as $champ => $null) {
            $html .= "<option value='{$champ}'>{$champ}</option>";
        }
        $html .= "</select>";
        // opérateurs
        $html .= "<select id='_operateur' name='_operateur' style='width:9em;' onchange='javascript:selectOperateur(this);'>";
        $html .= "<option></option>";
        foreach (self::$listeOperateurs as $champ => $null) {
            $html .= "<option value='{$champ}'>{$champ}</option>";
        }
        $html .= "</select>";
        // valeur
        $html .= "<input id='_valeur' name='_valeur' type='text' onkeypress='javascript:selectValeur(this);'/>"
                . "<span id='_loading' class='loading' style='display:none;'><i class='fas fa-circle-notch fa-spinner'></i></span>"
                . "<input id='_vid' name='_vid' type='hidden' />"
                . "<div id='_valeur-container' style='display:inline'></div>";
        // ajout de la partie de la condition
        $html .= "<a id='_add' href='#' onclick='javascript:selectAdd(); return false;'"
                . " title='ajouter la condition' class='disabled hidden'><i class='fas fa-plus'></i></a>";
        // _q est une chaîne représentant l'ensemble du filtre
        $html .= "<input id='_q' name='_q' type='hidden' size='80' /><div id='_qtext'></div>";
        $html .= "<button class='center' type='button' onclick='selectSubmit(); return false;'>"
                . "Appliquer le filtre et mettre à jour les résultats</button>";
        $html .= "</fieldset>";
        $html .= "</form>";
        // on envoie le contenu de listeChamps au javascript
        $html .= $this->_HTML_sendVarToJs();
        // on envoie le filtre par défaut
        $html .= <<<SCRIPT0
<script type='text/javascript'>
  var a = { statut: {
            'égal': [ 'En service' ]
            },
            badge: {
            'n‘est pas vide' : [ 'n‘est pas vide' ]
            }
          };
  document.getElementById('_q').value = JSON.stringify(a);
  select2text();
  document.getElementById('_champ').value = 'badge';
  document.getElementById('_operateur').value = 'contient';
  document.getElementById('_valeur').value = '';
</script>
SCRIPT0;
        return $html;
    }

    function HTML_result() {
        $html = "<fieldset><legend>Résultat"
                . "<span id='_resultLoading' class='loading' style='display:none;'>"
                . "<i class='fas fa-circle-notch fa-spinner'></i></span>"
                . "<input type='hidden' id='_resultQuery' name='_resultQuery' />"
                . "</legend><div id='_result'></div></fieldset>";
        return $html;
    }

    function HTML_selection() {
        $html = "<fieldset><legend>Sélection</legend>"
                . "<span id='_selectionLoading' class='loading' style='display:none;'>"
                . "<i class='fas fa-circle-notch fa-spinner'></i></span>"
                . "<div id='_selection'></div></fieldset>";
        return $html;
    }

    function HTML_affectation() {
        $html = "<fieldset><legend>Affectation</legend>"
                . "<span id='_affectationLoading' class='loading' style='display:none;'>"
                . "<i class='fas fa-circle-notch fa-spinner'></i></span>"
                . "<div id='_affectation'></div></fieldset>";
        $html .= $this->_HTML_sendVarToJs();
        return $html;
    }

    function HTML_lecteurs() { // inchangés
        $html = "<fieldset><legend>Droits inchangés</legend>"
                . "<div id='_lecteurs'></div>"
                . "</fieldset>";
        return $html;
    }

    public function HTML_editRightsOne(): string {
        $html = "<script type='text/javascript'>"
                . " setTimeout(() => {"
                . "  editRightsDirect('{$this->numero}', '"
                    . htmlspecialchars($this->nom. " ". $this->prenom, ENT_QUOTES). "');"
                . " }, 100); "
                . "</script>";
        return $html;
    }

    /*
     * retourne un tableau de (id, label, value) de badge
     * en fonction d'une chaine recherchée dans
     * numéro (si numérique), nom, prénom (si caractères)
     */

    function array_search($queryString) {
        $array = array();
        $sql = "SELECT numeroBadge AS numero, CONCAT(nom, ' ', prenom) AS np, bs.libelle AS statut"
                . " FROM {$this->dbs['mssql']->getTableName('badge')} AS b"
                . " LEFT JOIN {$this->dbs['mssql']->getTableName('badgeS')} AS bs ON b.statutId=bs.statutBadgeId ";
        $params = [$queryString . '%'];
        if (!is_numeric($queryString)) {
            $sql .= " WHERE nom LIKE ? OR prenom LIKE ?";
            $params[] = $queryString . '%';
        } else {
            $sql .= " WHERE numeroBadge LIKE ?";
        }
        $sql .= " ORDER BY nom, prenom"
                . " OFFSET 0 ROWS FETCH NEXT 20 ROW ONLY";

        $result = $this->dbs['mssql']->query($sql, $params);
        if (!$result) {
            $this->message = "Problème avec la requête de recherche " . HTML_debug($sql);
            $this->error = true;
            echo $this->message;
            return false;
        }
        while ($record = $this->dbs['mssql']->fetchObject()) {
            $array[] = array(
                'id' => $record->numero,
                'value' => $record->numero . " " . $record->np,
                'label' => $record->numero . " " . $record->np . ($record->statut != "En service" ? " ({$record->statut})" : "")
            );
        }
        $this->dbs['mssql']->freeResult($result);
        return $array;
    }

    /*
     * retourne un tableau de (id, label, value) de badge
     * en fonction d'une requête complexe en json
     *
     * Exemple : { badge: { 'égal': [ 'En service' ] } };
     */

    function array_results($queryResult, $page = 1, $dontVerrouilles = true) {
        $array = array();
        $n = 0;
        $prog = new Prog($this->objects);
        while ($record = $this->dbs['mssql']->fetchObject($queryResult)) {
            if ($page === false || floor($n / 20) == $page - 1) {
                $prog->numeroBadge = $record->numero;
                $prog->statut = 'aucun';
                $prog->statutBadge();
                if ($dontVerrouilles || $prog->statut == 'aucun') {
                    $array[] = array(
                        'id' => $record->numero,
                        'numéro' => "<a href='?badge/view/{$record->numero}'>" . I_BADGE . dechex($record->numero). "</a>",
                        'nom' => $record->nom,
                        'prénom' => $record->prenom,
                        'statut' => $record->statut,
                        "<a href='#' onclick='resultInverse(); return false;' title='Inverser la sélection'><i class='fas fa-check-square'></i></a>" .
                        " - <a href='#' onclick='resultVide(); return false;' title='vider'><i class='far fa-check-square'></i></a>"
                        => ($prog->statut == 'aucun' ? 
                            "<input type='checkbox' name='{$record->numero}' value='{$record->nom} {$record->prenom}'>" :
                            "<i class='fas fa-clock' title='Verrouillé par une programmation'></i>")
                    );
                }
            }
            $n++;
        }
        unset($prog);
        return $array;
    }

    /*
     * retourne un tableau de (id, label, value) de badge
     * en fonction d'un nom et d'un prénom
     */

    public function array_searchName(string $nom, string $prenom) {
        $array = array();
        $sql = "SELECT numeroBadge AS numero, CONCAT(nom, ' ', prenom) AS np, bs.libelle AS statut" .
                " FROM {$this->dbs['mssql']->getTableName('badge')} AS b" .
                " LEFT JOIN {$this->dbs['mssql']->getTableName('badgeS')} AS bs ON b.statutId=bs.statutBadgeId ";
        $sql .= " WHERE nom=? AND prenom=?";
        $params = [$nom, $prenom];
        $sql .= " ORDER BY nom, prenom"
                . " OFFSET 0 ROWS FETCH NEXT 20 ROW ONLY";

        $result = $this->dbs['mssql']->query($sql, $params);
        if (!$result) {
            $this->message = "Problème avec la requête de recherche " . HTML_debug($sql);
            $this->error = true;
            echo $this->message;
            return false;
        }
        while ($record = $this->dbs['mssql']->fetchObject()) {
            $array[] = array(
                'id' => $record->numero,
                'value' => $record->numero . " " . $record->np,
                'label' => $record->numero . " " . $record->np . ($record->statut != "En service" ? " ({$record->statut})" : "")
            );
        }
        $this->dbs['mssql']->freeResult($result);
        return $array;
    }

    /*
     * appelé lors d'un appel API depuis javascript
     * donne les éléments hors badges, donc la navigation et les
     * boutons de sélection
     */

    public function HTML_resultNav($queryResult, $page) {
        $nbRep = $this->dbs['mssql']->numRows($queryResult);
        $html = "<nav>{$nbRep} résultats - Page : ";
        for ($nbpage = 1; $nbpage < max(2, 1 + $nbRep / 20); $nbpage++) {
            if ($nbpage == $page) {
                $html .= "<strong>{$nbpage}</strong> ";
            } else {
                $html .= "<a href='#' onclick='resultRefresh({$nbpage}); return false;'; title='Voir la page {$nbpage}'>{$nbpage}</a>";
            }
        }
        $html .= "</nav>";
        $html .= "<div id='result2selection'>" .
                "<a href='#' onclick='result2selection(); return false;' title='Verser les badges cochés'><i class='fas fa-angle-right'></i></a><br/>" .
                "<a href='#' onclick='resultall2selection(); return false;' title='Verser tous les {$nbRep} badges (sauf ceux verrouillés)'><i class='fas fa-angle-double-right'></i></a><br/>" .
                "</div>";
        return $html;
    }

    /*
     * appelé lors d'un appel API depuis javascript
     * retourne la ressource de la requête selon le filtre
     */

    public function resultQuery_result(string $queryString) {
        /*
         *  queryString est sur le modèle
         * de la valeur par défaut : { badge: { 'égal': [ 'En service' ] } }
         * donc champ1: {opérateur1: [ liste des valeurs ], opérateur2: [liste des valeurs]},
         *      champ2: {opérateur1: [ liste des valeurs ], opérateur3: [liste des valeurs]}...
         *
         * Conversion du JSON en array
         */
        $champs = JSON_decode($queryString, true);

        $select = "SELECT DISTINCT badge.numeroBadge AS numero, badge.nom, badge.prenom, " .
                $this->dbs['mssql']->getTableName('badgeS') . ".libelle as statut";
        $join = "FROM {$this->dbs['mssql']->getTableName('badge')} AS badge";
        $join .= " LEFT JOIN {$this->dbs['mssql']->getTableName('badgeS')} " .
                "ON badge.statutId={$this->dbs['mssql']->getTableName('badgeS')}.statutBadgeId";
        $where = "WHERE (0=0)";
        $listeTableSql = array();
        // Construction de la requête en fonction de l'expression en JSON
        // champs
        foreach ($champs as $champ => $operateurs) {
            $tableSql = self::$listeChamps[$champ]['tableSql'];
            $listeTableSql[] = $tableSql;
            $champSql = self::$listeChamps[$champ]['champSql'];
            // conjonctions
            foreach (array("AND", "OR") as $conjonction1) {
                // opérateurs
                foreach ($operateurs as $operateur => $valeurs) {
                    $where1 = " AND (";
                    $sep = "";
                    // valeurs
                    foreach ($valeurs as $valeur) {
                        $conjonction = self::$listeOperateurs[$operateur]['conjonction'];
                        $operateurSqlBrut = self::$listeOperateurs[$operateur]['sql'];
                        $operateurSql = str_replace(
                                array('&lsaquo;', '&rsaquo;'),
                                array('<', '>'),
                                $operateurSqlBrut
                        );
                        // ajout des conjonctions dans la requête
                        if ($conjonction == $conjonction1) {
                            continue;
                        }
                        $where1 .= "{$sep}";
                        if (is_array($champSql)) { // plusieurs valeurs
//TODO: si c'est un site, alors il faut mettre la liste des lecteurs dans ce site en gardant les opérateurs
// recup des lecteurs des sites sélectionnés
                            $where1 .= "(";
                            $sep0 = "";
                            foreach ($champSql as $champSql0) {
                                $where1 .= $sep0 . $tableSql . ".";
                                if (in_array($operateurSqlBrut, array(self::$listeOperateurs['égal']['sql'], self::$listeOperateurs['différent']['sql'])) && $champ != 'statut' && $valeur != '') {
                                    //&& $champSql0!='statut' && $valeur!='') {
                                    // cas avec une valeur id
                                    $valeur0 = substr($valeur, 0, strpos($valeur, ' '));
                                    if ($champ == "site") {
                                        $where1 .= self::$listeChamps[$champ]['champSqlId'] .
                                                ($operateurSqlBrut == self::$listeOperateurs['différent']['sql'] ? " NOT" : "") .
                                                $this->sqlLecteursInSite($valeur0);
                                    } else {
                                        $where1 .= self::$listeChamps[$champ]['champSqlId'] .
                                                sprintf($operateurSql, $this->dbs['mssql']->quoteSmart($valeur0));
                                    }
                                } else {
                                    // cas avec une autre valeur
                                    $where1 .= $champSql0 . sprintf($operateurSql, $this->dbs['mssql']->quoteSmart($valeur));
                                }
                                $sep0 = " {$conjonction} ";
                            }
                            $where1 .= ")";
                        } else { // une seule valeur
                            $where1 .= $tableSql . ".";
                            $this->message .= "<b>$champ</b> $operateurSql ";
//TODO: si c'est un site, alors il faut mettre la liste des lecteurs dans ce site en gardant les opérateurs
                            if (in_array($operateurSqlBrut, array(self::$listeOperateurs['égal']['sql'], self::$listeOperateurs['différent']['sql'])) && $champ != 'statut' && $valeur != '') {
                                // cas avec une valeur id
                                $this->message .= "id ";
                                $valeur0 = substr($valeur, 0, strpos($valeur, ' '));
                                if ($champ == "site") {
                                    $where1 .= self::$listeChamps[$champ]['champSqlId'] .
                                            ($operateurSqlBrut == self::$listeOperateurs['différent']['sql'] ? " NOT" : "") .
                                            $this->sqlLecteursInSite($valeur0);
                                } else {
                                    $where1 .= self::$listeChamps[$champ]['champSqlId'] .
                                            sprintf($operateurSql, $this->dbs['mssql']->quoteSmart($valeur0));
                                }
                            } else {
                                // cas avec une autre valeur
                                $this->message .= "valeur ";
                                $where1 .= $champSql . sprintf($operateurSql, $this->dbs['mssql']->quoteSmart($valeur));
                            }
                        }
                        $sep = " {$conjonction} ";
                    } // end foreach $valeurs
                    $where1 .= ")";
                    if ($where1 != " AND ()") {
                        $where .= $where1;
                    }
                } // end foreach $operateurs
            } // end foreach $conjonctions
        } // end foreach $champs
        // on ajoute les liaisons entre les tables dans le cas où il y en a besoin
        if (in_array($this->dbs['mssql']->getTableName('lecteur'), $listeTableSql)
                //TODO: || in_array($this->dbs['mssql']->getTableName('site'), $listeTableSql)
                || in_array($this->dbs['mssql']->getTableName('horaire'), $listeTableSql)) {
            // profil_lecteur_horaire
            //$join .= " LEFT JOIN {$this->dbs['mssql']->getTableName('profil')} AS profil ON badge.groupeId=profil.groupeId";
            $join .= " LEFT JOIN {$this->dbs['mssql']->getTableName('droit')} AS profil_lecteur_horaire ON badge.groupeId=profil_lecteur_horaire.groupeId";
        }
        if (in_array($this->dbs['mssql']->getTableName('horaire'), $listeTableSql)) {
            // horaireId
            $join .= " LEFT JOIN {$this->dbs['mssql']->getTableName('horaire')} AS horaire ON profil_lecteur_horaire.indexPlage=horaire.indexPlage";
        }
        if (in_array($this->dbs['mssql']->getTableName('lecteur'), $listeTableSql)
        //TODO: || in_array($this->dbs['mssql']->getTableName('site'), $listeTableSql)
        ) {
            // lecteur
            $join .= " LEFT JOIN {$this->dbs['mssql']->getTableName('lecteur')} AS lecteur ON profil_lecteur_horaire.lecteurId=lecteur.lecteurId";
        }
        /* if (in_array($this->dbs['mssql']->getTableName('site'), $listeTableSql)) {
          // site
          // Recherche de tous les lecteurs des sites indiqués
          // Ajout de la table "droit" sauf si déjà mise avant
          // Ajout de la clause
          //$join .= " LEFT JOIN {$this->dbs['mssql']->getTableName('lecteurS')} AS lecteur_site ON lecteur.lecteurId=lecteur_site.lecteurId";
          //$join .= " LEFT JOIN {$this->dbs['mssql']->getTableName('site')} AS site ON lecteur_site.societeId=site.societeId";
          } */

        // Composition de la requête
        $sql = $select . " " . $join . " " . $where . " ORDER BY badge.nom, badge.prenom";

        // exécution de la requête, enfin
        $result = $this->dbs['mssql']->query($sql);
        if (!$result) {
            $this->error = true;
            $this->message .= "Problème lors de l'exécution de la requête" . HTML_debug($sql) . HTML_debug($queryString);
            return false;
        }/* else {
          // TODO : retirer après debug
          _echo("Requête". HTML_debug($sql). HTML_debug($queryString));
          } */
        return $result;
    }

    // renvoie dans un tableau simple la liste des lecteurs d'un site
    private function sqlLecteursInSite(int $site_id): string {
        $lecteur = new Lecteur($this->objects);
        $lecteurs_id = $lecteur->array_listInSite($site_id, true);
        $lecteurs_id[] = -1;
        //var_dump($lecteurs_id);
        unset($lecteur);
        return " IN (" . implode(',', $lecteurs_id) . ")";
    }

    /*
     * converti un tableau de bagdes (numero, nom, prénom, profil)
     * en enregistrement JSON (id, porteur)
     */

    function JSON_array($array) {
        $json = array();
        foreach ($array as $record) {
            $json[] = array(
                'id' => $record['id'], // le numéro brut
                'value' => $record['nom'] . " " . $record['prénom']
            );
        }
        return json_encode($json);
    }

    /*
     * retourne un JSON de la liste des sites autorisés correspondant
     * à l'habilitation editBadges
     */

    function JSON_sitesAutorises() {
        return json_encode($this->objects['session']->mesSitesAutorises('editBadges'));
    }

    /*
     * Fonction appelée à partir du formulaire d'affectation de droits sur
     * plusieurs badges
     * $_POST['selectionF']   = liste des badges
     *  (id => (numero, nom+' '+prenom))
     * $_POST['affectationF'] = liste des droits classés par horaires
     *  (id => (id=>horaire_id, label=>horaire_nom,
     *    lecteurs => (id => (lecteur_id))
     *  ))
     *   attention, horaire_id==-1 pour aucun droit (à retirer)
     *
     *  $_POST['__prog'] sur programmation
     *      depuis $_POST['__prog_depuis'] jusque $_POST['__prog_jusque']
     */

    function fromAffect() {
        $badges = json_decode($_POST['selectionF']);
        $droits = json_decode($_POST['affectationF']);

        // liste des id des profils précédemment utilisés et qui devront
        // faire l'objet d'un test pour savoir s'ils sont encore utilisés
        $anciensProfils = array();

        // en ne changeant que les droits des sites permis à l'utilisateur
        // TODO : il semble qu'on n'en fasse rien.
        $sitesAutorises = $this->objects['session']->mesSitesAutorises('editBadges');

        // pour chaque personne, calculer son nouveau profil
        foreach ($badges as $cebadge) {

            $this->numero = $cebadge[0];
            if (!$this->get()) { // on charge le badge
                $this->message .= HTML_debug($this->numero . " " . $this->message);
                continue;
            }
            $txt_debug = "{$this->nom} {$this->prenom} ({$this->numero}) ";

            $this->profil->get(); // on charge l'actuel profil de badge

            $nouveauProfil = new Profil($this->objects); // on en fait une réplique
            $nouveauProfil->id = $this->profil->id;
            $nouveauProfil->get();
            $nouveauProfil->id = $nouveauProfil->nom = null;

            $lecteur = new lecteur($this->objects);
            // on retire les droits pour les sites permis à l'utilisateur
            // et qui sont affectés (y compris "Jamais")
            foreach ($droits as $cedroit) {
                $horaire_id = $cedroit->id;
                $lecteurs = $cedroit->lecteurs;
                foreach ($lecteurs as $lecteur_id) {
                    $lecteur->id = $lecteur_id;
                    $lecteur->get();
                    if ($lecteur->is_allowed_edit()) {
                        $nouveauProfil->unsetDroit($lecteur_id);
                    }
                }
            }

            // on ajoute ceux provenant du formulaire
            foreach ($droits as $cedroit) {
                $horaire_id = $cedroit->id;
                // on "Jamais", on ne donne pas de droit.
                if ($horaire_id == -1)
                    continue;
                $lecteurs = $cedroit->lecteurs;
                foreach ($lecteurs as $lecteur_id) {
                    $lecteur->id = $lecteur_id;
                    $lecteur->get();
                    if ($lecteur->is_allowed_edit()) {
                        $nouveauProfil->setDroit($lecteur_id, (int) $horaire_id);
                    }
                }
            }

            // s'il y a un changement dans le profil
            if (!$this->profil->is_equal($nouveauProfil->droits)) {
                // on stock l'id de l'ancien profil
                if (!in_array($this->profil->id, $anciensProfils)) {
                    $anciensProfils[] = $this->profil->id;
                }
                //_echo(HTML_debug($txt_debug. "nouveau profil."));
                
                // Y a-t-il programmation ?
                if ($_POST['__prog'] !== 'oui') {
                    // Il n'y a pas de programmation, alors on affecte directement
                    $this->doChangeProfil($nouveauProfil);
                } else {
                    // Il y a une programmation, on envoie vers la classe Prog
                    $prog = new Prog($this->objects);
                    foreach(array('depuis'=>FILTER_SANITIZE_STRING,
                        'jusque'=>FILTER_SANITIZE_STRING) as $field=>$sanitize) {
                        $prog->$field = $_POST['__prog_'.$field]>"" ? filter_var($_POST['__prog_'.$field]. ':00', $sanitize) : null;
                    }
                    $prog->numeroBadge = $this->numero;
                    $prog->profil_origine = $this->profil;
                    $prog->profil_origine_nom = $this->profil->nom;
                    $nouveauProfil->creeNom();
                    $prog->profil_destination = $nouveauProfil;
                    $prog->profil_destination_nom = $nouveauProfil->nom;
                    $prog->nouveau();
                    if ($prog->error) {
                        $this->error = true;
                        $this->message .= $prog->message;
                    }
                }
            } else {
                $this->message .= HTML_debug($txt_debug . "pas de changement.");
            }
        } // end foreach ($badges)

        $this->doCleanProfiles($anciensProfils);

        return $this->error;
    }

    /*
     * Cree un nouveau profil s'il n'existe pas
     * Affecte le profil au badge concerné
     * 
     * Entrées :
     *  $this->numero - badge concerné
     *  $this->profil - actuel profil
     * Paramètre :
     *  $nouveauProfil - profil à affecter
     */
    
    public function doChangeProfil(Profil $nouveauProfil): bool {
        // vérifier qu'un profil n'existe pas déjà avec des droits
        if (($nouveau_profil_id = $nouveauProfil->exists()) == false) {
            // le nouveau profil n'existe pas
            // on crée le nouveau profil
            $nouveauProfil->creeNom();
            $nouveauProfil->save();
            if ($nouveauProfil->error) {
                $this->message .= HTML_error($nouveauProfil->message);
            }
            //_echo(HTML_debug("Création et sauvegarde nouveau profil {$nouveauProfil->nom} ({$nouveauProfil->id})"));
            // on l'affecte au badge
            $this->profil_id = $nouveauProfil->id;
            $this->message .= HTML_debug("Affectation du nouveau profil {$nouveauProfil->nom} ({$nouveauProfil->id})" .
                            " au badge {$this->prenom} {$this->nom} ({$this->numero})");
            $this->save();
            if ($this->error) {
                $this->message .= HTML_error($this->message);
            }
        } else {
            $this->profil_id = $this->profil->id = $nouveau_profil_id;
            $this->profil->get();
            $this->message .= HTML_debug("Affectation d'un profil déjà existant {$this->profil->nom} ({$this->profil->id})" .
                            " au badge {$this->prenom} {$this->nom} ({$this->numero})");
            $this->save();
            if ($nouveauProfil->error) {
                $this->message .= HTML_error($nouveauProfil->message);
            }
        }
        return !$this->error;
    }

    /*
     * Retire les profils qui ne servent plus
     */

    public function doCleanProfiles(array $anciensProfils): bool {
        // regarder s'il n'y a pas d'anciens profils devenus inutiles
        foreach ($anciensProfils as $profilAncien_id) {
            $this->profil_id = $profilAncien_id;
            if (!$this->is_profil_used()) { // pas utilisé ?
                $this->profil->id = $this->profil_id;
                $this->profil->get();
                $this->message .= HTML_debug("L'ancien profil {$this->profil->nom} ({$this->profil->id}) ne sert plus, on le supprime.");
                $this->profil->delete(); // alors on fait le ménage
                $this->error &= $this->profil->error;
                if ($this->profil->error) {
                    $this->message .= HTML_error($this->profil->message);
                }
            }
        }
        return !$this->error;
    }

    // retourne true si le profil est affecté à un badge
    // $this->profil_id doit être renseigné
    function is_profil_used() {
        $query = "SELECT numeroBadge " .
                "FROM {$this->dbs['mssql']->getTableName('badge')} " .
                "WHERE groupeId=?";
        $result = $this->dbs['mssql']->query($query, [$this->profil_id]);
        if (!$result) {
            $this->error = true;
            $this->message .= "Problème lors de l'exécution de la requête" . HTML_debug($query);
            $this->message .= HTML_error($this->message);
            return true;
        }
        $count = $this->dbs['mssql']->fetchObject();
        $this->dbs['mssql']->freeResult();

        return is_object($count);
    }

    // affiche les logs de ce badge
    function HTML_lastlogs() {
        if (!$this->objects['session']->droits['log']) {
            $this->message = "Vous n'avez pas l'habilitation suffisante.";
            return false;
        }
        $log = new log($this->objects);
        $log->filter['sur_objet'] = "badge";
        $log->filter['sur_id'] = $this->numero;
        return $log->HTML_searchResult("Historique des logs");
    }

    public function array_withProfil(int $profil_id) {
        $array = array();
        $query = "SELECT numeroBadge " .
                "FROM {$this->dbs['mssql']->getTableName('badge')} " .
                "WHERE groupeId={$profil_id}";
        $result = $this->dbs['mssql']->query($query);
        /* echo "<hr>";
          var_dump($query); */
        if (!$result) {
            $this->error = true;
            $this->message .= HTML_debug("Problème lors de l'exécution de la requête " . $query);
            $this->message .= HTML_error($this->message);
            return true;
        }
        while ($record = $this->dbs['mssql']->fetchObject()) {
            $array[] = $record->numeroBadge;
        }
        /* echo "<br/>";
          print_r($array); */
        $this->dbs['mssql']->freeResult();
        return $array;
    }

}
