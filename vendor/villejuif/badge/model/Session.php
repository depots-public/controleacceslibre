<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/*
 * Script de gestion des session
 */
namespace Badge\Model;
require_once('Groupe.php');
require_once('Badge.php');

class Session {
    
    public 
            //$login,
            $id = null,
            $destUrl, // url de destination s'il y en avait une
            $error,
            $message,
            $mesSitesAutorises = array(),
            $ad,
            $droits;
    private 
            $dbs,
            $objects,
            $groupe,
            $user;

    public function __construct(array $dbs)
    {
        $this->dbs = $dbs;
        $this->objects = ['dbs' => $this->dbs, 'session' => $this];
        session_start();
        $this->ad = new \Adldap\Adldap();
        $this->ad->addProvider(\Badge\Config::$ldapConfig);
        if ($this->isLogon()) {
            $this->id = $_SESSION['login'];
            $this->user = $this->ad->search()->users()->find($this->id);
        }
        $this->getDroits();
    }

    public function close() :void{
        if ($this->isLogon()) {
            $log = (new Log($this->objects))->add('logout', 'session');
            unset($log);
        }
        session_destroy();
    }

    private function getDroits() : void
    {
        $this->groupe = new Groupe($this->objects);
        $this->droits = array_merge([], $this->groupe->droits);
        Foreach($this->groupe->getAllGroups() as $groupId => $groupName)
        {
            If($groupName == "Defaut" || ($this->user !== null && $this->user->inGroup($groupName, true)))
            {
                $this->groupe->id = $groupId;
                $this->groupe->get();
                foreach(\Badge\Model\Groupe::$groupeDroitsVf as $droit => $comment){ 
                    $this->droits[$droit] |= (bool)$this->groupe->droits[$droit]; 
                }
                foreach(\Badge\Model\Groupe::$groupeDroitsSite as $droit => $comment){ 
                    $this->droits[$droit] = array_unique(array_merge($this->droits[$droit], $this->groupe->droits[$droit])); 
                }
            }
        }
    }

    public function HTML_mesGroupes() : string {
        $html = "<p class='message'>Mes groupes : "; $sep="";
        $this->groupe = new Groupe($this->objects);
        $this->droits = array_merge([], $this->groupe->droits);
        Foreach($this->groupe->getAllGroups() as $groupName)
        {
            If($groupName == "Defaut" || ($this->user !== null && $this->user->inGroup($groupName, true)))
            {
                $html .= $sep. $groupName;
                $sep = ", ";
            }
        }
        $html .= "</p>";
        return $html;
    }

    /*
     * Rend une chaine au format HTML avec les droits des badges aux nom et 
     * prénom de l'utilisateur identifié.
     */
    public function HTML_mesDroits() : string {
        $badge = new \Badge\Model\Badge($this->objects);
        $array = $badge->array_searchName($this->user->sn[0], $this->user->givenname[0]);
        if ($badge->error) { 
            return $html = HTML_error($badge->message);
        }
        $html = "";
        foreach($array as $monBadge) {
            $badge->numero = $monBadge["id"];
            $html .= _h3("Mon badge n°{$badge->numero}");
            $badge->get();
            if ($badge->error) {
                return $html = HTML_error($badge->message);
            }
            $badge->profil->get();
            $html .= $badge->profil->HTML_view();
        }
        return $html;
    }
    
    /*
     * calcule et retourn les sites pour les droits
     *  lireBadges, editBadges, journal
     */
    public function mesSitesAutorises(string $droit) : array
    {
      // les sites autorisés ne sont pas déjà calculés ?
      if (! isset($this->mesSitesAutorises[$droit])) {
        /*
         * on commence par faire la liste de tous les sites
         * parents et enfants
         */
        $mesSitesParentsAutorises = $this->droits[$droit];
        $mesSitesAutorises = array();
        $site = new site($this->objects);
        foreach($mesSitesParentsAutorises as $parent_id) {
          $mesSitesAutorises[$parent_id] = $parent_id;
          $sites_id = $site->array_listOfChildren(true, $parent_id);
          foreach ($sites_id as $record) {
            $mesSitesAutorises[$record[0]] = $record[0];
          }
        }
        unset($site, $mesSitesParentsAutorises);
        $this->mesSitesAutorises[$droit] = $mesSitesAutorises;
        unset($mesSitesAutorises);
      }
      return $this->mesSitesAutorises[$droit];
    }

    /*
     * Retourne vrai ou faux selon que l'utilisateur connecté
     * dispose des droits de session, horaire, site, lecteur, groupe, journal
     * ou sur un site pour les droits lireBadges, editBadges, historique
     * Cf. class.groupe.inc.php
     */
    public function is_allowed(string $droit, int $id_site = 0) : bool
    {
        if (array_key_exists($droit, \Badge\Model\Groupe::$groupeDroitsVf)) {
            return $this->droits[$droit];
        }
        // le droit demandé est inconnu
        if (! array_key_exists($droit, \Badge\Model\Groupe::$groupeDroitsSite)) {
            var_dump("droit inconnu");
            return false;
        }
        // le droit demandé est sur un site
        if ($id_site == 0) {
            return count($this->mesSitesAutorises($droit))>0;
        } else {
            return in_array($id_site, $this->mesSitesAutorises($droit));
        }
    }

    public function HTML_form(array $url_query) :string
    {
    global $url_data;
        $this->close();
        $mentionCnil = \Badge\Config::$mentionCnil;
        $html = <<<SFORM
<form action="index.php?session/fromForm" method='post'>
<table align="form">
  <tr>
    <td>Identifiant&nbsp;:</td>
    <td><input type="text" required name="__session_login" size="30" /></td>
  </tr>
  <tr>
    <td>Mot de passe</td>
    <td><input type="password" required name="__session_motdepasse" size="30" /></td>
  </tr>
  <tr>
    <td>Protection des données</td>
    <td>${mentionCnil}
    <!--<label class='inline' for="__session_avertissement">J'ai bien compris et j'accèpte<input
    type="checkbox" required id="__session_avertissement"  name="__session_avertissement" value="oui" /></label>--></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" value="M'identifier" /></td>
  </tr>
</table>
SFORM;

        if (count($url_query)>0 && $url_query[0] != 'session') {
            $urlDest = implode('/', $url_query);
        } elseif ($this->destUrl > '') {
            $urlDest = $this->destUrl;
        }
        if (isset($urlDest)) {
            $html .= "<input name='__session_urlquery' type='hidden' value='". $urlDest. "' />";
        }
        
        return $html;
    }

    private function isLoginOk(string $login, string $password) : bool
    {
        $ok = false;
        If($this->ad->auth()->attempt($login, $password)){
            $ok = true;
        }
        // Enregistrer dans les logs
        $log = (new log($this->objects))->add('login '. ($ok ? 'ok' : 'nok'), 'session', null, null, $login);
        unset($log);
        return $ok;
    }

    function fromForm() : bool
    {
        if(isset($_POST) && !empty($_POST['__session_login']) && !empty($_POST['__session_motdepasse'])) {
            // recup url de destination
            if (isset($_POST['__session_urlquery']) && $_POST['__session_urlquery']>'') {
                $this->destUrl = $_POST['__session_urlquery'];
            }
            // on recupère le password de la table qui correspond au login du visiteur
            if ($this->isLoginOk($_POST['__session_login'], $_POST['__session_motdepasse'])) {
                // Compte valide
                $_SESSION['login'] = $_POST['__session_login'];
                // regarder les droits du groupe, voir surtout s'il a le droit d'accès à l'application
		$this->user = $this->ad->search()->users()->find($_SESSION['login']);
                
                $this->getDroits();

                return true;
            } else {
                $this->message = "Identification incorrecte";
                return false;
            }
        } else {
            $this->error = true;
            $this->message = "Identification tr&egrave;s incorrecte";
            return false;
        }
    }

    function isLogon() : bool{
        return isset($_SESSION['login']);
    }
    
    /*
     * Vérifie si l'utilisateur est connecté sinon le redirige vers la page de 
     * login
     * Vérifie ensuite que l'utilisateur dispose de la bonne habilitation
     */
    public function isLoggedOrHasRight (string $right, bool $isDisplay=true) : bool{
    global $url_query;
        If (! $this->isLogon()){
            header("Location: ?session/login/". (is_array($url_query) ? implode("/", $url_query) : ''));
        }
        if (! $this->droits[$right]) {
            if ($isDisplay) {
                _echo(HTML_error("Vous n'avez pas l'habilitation suffisante."));
            }
            return false;
        }
        return true;
    }

} // end class
