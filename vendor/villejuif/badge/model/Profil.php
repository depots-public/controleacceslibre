<?php
/*
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2021 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */
namespace Badge\Model;
require_once('Horaire.php');
require_once('Lecteur.php');
require_once('Site.php');
require_once('Badge.php');

/*
 * Gestion des profils de droits
 * Les profils ne sont pas visibles en tant que tels à l'utilisateurs
 * mais sont indispensables car c'est comme cela qu'ils sont stockés
 * dans la base SQL
 */

class Profil {
    public 
            $id = null,          # id du profil dans la base SQL
            $nom = null,         # nom automatique du profil
            $statut = "nouveau", # nouveau (temporaire), visible ou interdit ; TODO : vérifier dans la base SQL
            $error   = false, 
            $message = "",
            /*
             * $droits = array of ($lecteur_id => horaire_id);
             * en effet, le lecteur ne peut apparaître qu'une fois
             * alors que l'horaire plusieurs fois
             */
            $droits = array();

    private
            $dbs,
            $objects;
    
    public function __construct(array $objects)
    {
        $this->objects = $objects;
        $this->dbs = $objects['dbs'];
    }

    function setDroit($lecteur_id, $horaire_id) : void
    {
        $this->droits[$lecteur_id] = $horaire_id;
    }

    function unsetDroit($lecteur_id) : void
    {
        unset($this->droits[$lecteur_id]);
    }

    function get() : bool
    {
        $query = "SELECT * ".
            "FROM {$this->dbs['mssql']->getTableName('profil')} ".
            "WHERE groupeId={$this->id}";
        $this->dbs['mssql']->query ($query, [$this->id]);
        if (! ($record = $this->dbs['mssql']->fetchObject()))
        {
            $this->message = "Aucun profil de droits avec l'id ". $this->id;
            //echo HTML_debug($this->message);
            $this->error = true;
            $this->droits    = array();
        } else {
          $this->nom         = $record->libelle;
          $this->statut      = $record->estInterdit==0 ? "visible" : "interdit";
          //$this->datemaj     = $record->datemaj;
          $this->dbs['mssql']->freeResult();
          
          // get droits
          $this->droits    = array();
          $query = "SELECT * ".
              "FROM {$this->dbs['mssql']->getTableName('droit')} ".
              "WHERE groupeId=? ".
              "ORDER BY lecteurId";
          $this->dbs['mssql']->query ($query, [$this->id]);
          while ($record = $this->dbs['mssql']->fetchObject()) {
            $this->setDroit($record->lecteurId, (int)$record->indexPlage);
          }
          $this->dbs['mssql']->freeResult();

          $this->message = "";
          $this->error = false;
        }
        return ! $this->error;
    }

    function save() : bool
    {
        $this->error = false;
        $params = ['libelle'=>$this->nom, 'statut'=>($this->statut != "interdit" ? 0 : 1)];
        if ($this->id > 0) {
            $sql = "UPDATE ". $this->dbs['mssql']->getTableName('profil').
                " SET libelle=:libelle, estInterdit=:statut ".
                " WHERE groupeId=:groupeId";
            $params['groupeId'] = $this->id;
        } else {
            // TODO : la structure de la table SQL n'a pas d'autoincrement
            // sur le champ groupeId de la table Groupe.
            // Il faut donc chercher le premier numéro libre avant d'insérer.
            // Quelle misère...
            // trouver la max indexPlage
            $sql = "SELECT groupeId FROM ". $this->dbs['mssql']->getTableName('profil').
                    " ORDER BY groupeId";
            $ret = $this->dbs['mssql']->query($sql);
            if ($ret === false) {
                $this->error = true;
                $this->message .= HTML_error("Impossible de trouver l'index des profils"). HTML_debug($sql);
                $itemIndex = 0;
            } else {
                $index = array();
                while ($record = $this->dbs['mssql']->fetchObject()) {
                    $index[] = $record->groupeId;
                }
                for ($itemIndex=1; in_array($itemIndex, $index); $itemIndex++) ;
                $params['groupeId'] = $this->id = $itemIndex;
            }
            $sql = "INSERT INTO ". $this->dbs['mssql']->getTableName('profil').
                " (groupeId, libelle, estInterdit, testerDelaiTransit, compteur, maxCompteur, societeId)".
                " VALUES (:groupeId, :libelle, :statut, 1, 0, 0, 1)";
            // dernier nombre = societeId
            // 11 = HdV
            // 1 = _VILLEJUIF
        }
        $ret = $this->dbs['mssql']->query($sql, $params);
//_echo(HTML_debug($sql));
        if ($ret === false) {
            $this->error=true;
            $this->message .= "Impossible d'enregistrer le profil de badge". HTML_debug($sql);
            _echo(HTML_error($this->message));
        } else {
            /*if ($this->id==0) {
              $this->id = $this->dbs['mssql']->insertid();
            } else {*/
                
              // erase droits
              $sql = "DELETE FROM {$this->dbs['mssql']->getTableName('droit')} ".
                  "WHERE groupeId=?";
              $ret = $this->dbs['mssql']->query($sql, [$this->id]);
//_echo(HTML_debug($sql));
              if ($ret === false) {
                  $this->error=true;
                  $this->message .= HTML_error("Impossible de supprimer les droits du profil de badge");
                  _echo($this->message);
              }
            //}

            if (! $this->error) {
                // save droits
                $query = "INSERT INTO ". $this->dbs['mssql']->getTableName('droit').
                    " (groupeId, lecteurId, indexPlage) VALUES";
                $sep = " ";
                foreach($this->droits as $lecteur_id=>$horaire_id) {
                  $query .= $sep. "({$this->id}, {$lecteur_id}, {$horaire_id})";
                  $sep = " ,";
                }
                $ret = $this->dbs['mssql']->query($query);
//_echo(HTML_debug($query));
                if ($ret === false) {
                    $this->error=true;
                    $this->message .= HTML_error("Impossible d'enregistrer les droits du profil de badge");
                    _echo($this->message);
                } else {
                    $this->commit();
                }
                
            } else {
                _echo(HTML_debug($this->message));
            }

            // enregistrer dans les logs
            $log = (new log($this->objects))->add('save', 'profil', $this->id, $this);
            unset($log);    
        }
        
        return ! $this->error;
    }

        /*
     * commande la mise à jour du badge sur les centrales
     *
    private function commit() : bool
    {
        $sql = "INSERT INTO ". $this->dbs['mssql']->getTableName('badgeC').
                " (zoneModifiee, numeroBadge, action)".
                " VALUES (0, ?, 2)";
        $ret = $this->dbs['mssql']->query($sql, [$this->numero]);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de commande la mise à jour sur les centrales");
        }
        return ! $this->error;
    }/**/
    
    // supprime le profil qui en principe ne sert plus.
    public function delete() : bool
    {
        if ($this->is_used()) {
          $this->error = true;
          $this->message .= HTML_error("Impossible de supprimer le profil, il sert encore");
            _echo($this->message);
          return false;
        }

        // erase droits
        $sql = "DELETE ".
            "FROM {$this->dbs['mssql']->getTableName('droit')} ".
            "WHERE groupeId=?";
        $ret = $this->dbs['mssql']->query($sql, [$this->id]);
        if ($ret === false) {
            $this->error=true;
            $this->message .= HTML_error("Impossible de supprimer les droits du profil de badge");
            _echo($this->message);
        }

        // supprimer le profil
        $sql = "DELETE FROM {$this->dbs['mssql']->getTableName('profil')}";
        $sql .= " WHERE groupeId=?";
        $ret = $this->dbs['mssql']->query($sql, [$this->id]);
        if ($ret === false) {
            $this->error=true;
            $this->message .= HTML_error("Impossible de supprimer le profil de badge");
            _echo($this->message);
        }

        // enregistrer dans les logs
        $log = (new log($this->objects))->add('delete', 'profil', $this->id, $this);
        unset($log);

        return ! $this->error;
    }

    /* le profil est-il utilisé ? */
    function is_used()
    {
        $badge = new badge($this->objects);
        $badge->profil_id = $this->id;
        return $badge->is_profil_used();
    }

    // retourne l'id du profil egal aux droits
    // ou false si aucun profil n'existe
    public function exists()
    {
        $query = "SELECT groupeId FROM {$this->dbs['mssql']->getTableName('profil')}";
        $ret = $this->dbs['mssql']->query ($query);
        if ($ret === false) {
            $this->error=true;
            $this->message .= HTML_error("Impossible de récupérer les profils de badge");
            _echo($this->message);
        }
        $arrayProfilId = array();
        while ($record = $this->dbs['mssql']->fetchObject()) {
          $arrayProfilId[] = $record->groupeId;
        }
        // une deuxième boucle parce qu'on ne peut pas imbriquer avec PDO.
        $droits = $this->droits;
        $profil = new profil($this->objects);
        foreach ($arrayProfilId as $profil_id) {
            $profil->id = $profil_id;
            $profil->get();
            if ($profil->is_equal($droits)) {
                return (int)$profil->id;
            }
        }
        unset($profil, $arrayProfilId);
        return false;
    }

    /*
     *  crée un nom de profil à partir des droits
     * suite de HL séparés par des +
     *  Hh:L+H:L où h = numéro de l'horaire
     * où L est une liste de lecteurs
     *  ex: 1,3 ou 5-9,11
     *
     * ex: H2:1,3+H51:5-9,11
     *
     * on suppose que les droits ont été chargés avant avec un get();
     */
    public function creeNom() : string
    {
        $H = array();
        foreach($this->droits as $lecteur_id=>$horaire_id) {
          if (!isset($H[$horaire_id])) {
            $H[$horaire_id] = array();
          }
          $H[$horaire_id][] = $lecteur_id;
        }
        $nom = "";
        foreach($H as $horaire_id=>$lecteurs) {
          $nom .= ($nom > "" ? "+" : ""). "H". $horaire_id. ":";
          sort($lecteurs);
          $prec_id = $prem_id = null;
          foreach($lecteurs as $lecteur_id) {
            if ($prec_id + 1 != $lecteur_id) {
              if ($prem_id !== null & $prem_id != $prec_id) $nom .= "$prem_id-";
              $nom .= ($prec_id == null ? "" : $prec_id.",");
              $prem_id = $prec_id = $lecteur_id;
            } else {
              $prec_id = $lecteur_id;
            }
          }
          if ($prem_id !== null & $prem_id != $prec_id) $nom .= "$prem_id-";
          $nom .= $prec_id;
        }
        unset($H);
        $this->nom = $nom;
        return $nom;
    }

    /*
     * Fonction inverse de creeNom
     * enregistre les droits à partir d'un nom de profil
     */
    public function creeDepuisNom() : bool {
        $this->droits = array();
        
        // découpage par horaire
        if (! strpos($this->nom, "+")) {
            $horaires = [$this->nom];
        } else {
            $horaires = explode("+", $nom);
        }

        foreach($horaires as $horaire) {
            // extrait du numéro d'horaire
            $horaire_id = substr($horaire, 1, strpos($horaire, ":")-1);
            $horaire = substr($horaire, strpos($horaire, ":")+1);

            // découpage en lecteurs
            if (! strpos($horaire, ",")) {
                $lecteurs = [$horaire];
            } else {
                $lecteurs = explode(",", $horaire);
            }
            
            foreach($lecteurs as $lecteur) {            
                if (strpos($lecteur, "-")) {
                    // intervalle
                    list($de, $a) = explode("-", $lecteur);
                    $lecteur_ids = range($de, $a);
                } else {
                    // extrait du numéro de lecteur
                    $lecteur_ids = [$lecteur];
                }
                // droits correspondants
                foreach($lecteur_ids as $lecteur_id) {
                    $this->setDroit($lecteur_id, $horaire_id);
                }
            }
        }    
        return ! $this->error;
    }
    
    
    // compare les droits avec ceux du profil en cours
    function is_equal(array $droits) : bool
    {
        return count(
            array_merge(
              array_diff_assoc($this->droits, $droits),
              array_diff_assoc($droits, $this->droits)
            )
          )==0;
    }

    /*
     * Trouve l'intersection entre tous les profils
     * en limitant aux éventuels lecteurs de sites (array of site_id)
     *
     * Retourne un tableau des droits ($horaire_id => lecteur_id);
     */
    function communDroits($profils_id, $sites_id_parents=array(0)) : array
    {
        // Retrouver la liste de tous les sites (et leurs enfants)
        $site = new site($this->objects);
        $sites_id = $sites_id_parents;
        foreach($sites_id_parents as $site_id) {
            foreach($site->array_flatListOfChildren(true, $site_id) as $site_id) {
                $sites_id[$site_id] = $site_id;
            }
        }
        // Retrouver la liste de tous les lecteurs de tous les sites
        $lecteur = new lecteur($this->objects);
        $lecteurs_id = array();
        foreach($sites_id as $site_id) {
            foreach($lecteur->array_listInSite($site_id, true) as $array_lecteur) {
                $lecteurs_id[$array_lecteur] = $array_lecteur;
            }
        }

        // par défaut tous les lecteurs interdits
        $lecteursCommunsInterdits = $lecteurs_id;

        // on récupère les profils et on ne retient que les lecteurs concernés
        $first = true;
        foreach($profils_id as $profil_id) {
            $droitsProfil = array();
            $this->id = $profil_id;
            $this->get();
            foreach($this->droits as $lecteur_id=>$horaire_id) {
                if (! isset($droitsProfil[$horaire_id])) {
                    $droitsProfil[$horaire_id] = array();
                }
                if (in_array($lecteur_id, $lecteurs_id)) {
                    if (! in_array($lecteur_id, $droitsProfil[$horaire_id])) {
                        $droitsProfil[$horaire_id][] = $lecteur_id;
                        // on retire des droits communs interdits tous les lecteurs
                        // présents au moins une fois dans un profil
                        unset($lecteursCommunsInterdits[$lecteur_id]);
                    }
                }
            }
            if ($first) {
                $droitsCommun = $droitsProfil;
                $first=false;
                continue;
            } else {
                // on regarde ce qui est en commun
                $droitsCommun = array_intersect_assoc($droitsCommun, $droitsProfil);
            }
            // plus rien n'est en commun
            if (count($droitsCommun)==0) break;
        }

        // aucun droit (jamais) sur les lecteurs
        $droitsCommun[-1] = array();
        foreach($lecteursCommunsInterdits as $val) {
            $droitsCommun[-1][] = (int)$val;
        }
        
        return $droitsCommun;
    }

    /*
     * Affiche dans le div affectation les informations sur le profil commun aux badges :
     * - les horaires
     *   - sites
     *     - lecteurs
     */
    // TODO : comprendre pourquoi ça prend un temps très important.
    public function HTML_view() : string
    {
        $html = "<fieldset><legend>Droits accordés au badge (groupe n°{$this->id} {$this->nom})</legend>";

        // tous les horaires
        $horaires_id = array_unique($this->droits);
        sort($horaires_id);
        $horaire = new horaire($this->objects);
        foreach($horaires_id as $horaire_id) {
          $horaire->id = $horaire_id;
          $horaire->get();
          $html .= "<h5 class='horaire'>". I_HORAIRE. "{$horaire->nom}</h5>";
          // tous les lecteurs de l'horaire
          $lecteurs_id = array_keys($this->droits, $horaire_id);
          $html .= $this->_HTML_listeSitesLecteurs($lecteurs_id);
        }
        $html .= "</fieldset>\n";
        return $html;
    }

    /*
     * Retourne code HTML
     * $lecteurs_id = array des lecteurs à afficher
     * $site_id = site des lecteurs
     */
    private function _HTML_listeSitesLecteurs($lecteurs_id, $site_id=0) : string
    {
        $html = "";

        $site = new site($this->objects);
        $site->id = $site_id;
        $site->get();

        $lecteur = new lecteur($this->objects);
        // les lecteurs du site
        foreach ($lecteurs_id as $lecteur_id) {
            $lecteur->id = $lecteur_id;
            $lecteur->get();
            if ($lecteur->site_id == $site_id) {
                $html .= "<li class='lecteur'>". I_LECTEUR. $lecteur->libelle. " (". $lecteur->id. ")</li>";
            }
        } // end foreach
        unset($lecteur);

        // les sites de ce site
        foreach($site->array_listOfChildren(false, $site_id) as list($site_enfant_id, $level)) {
            // on ne veut que le 1er niveau, les enfants, mais pas les petits-enfants
            // ces derniers apparaîtrons dans le bon site avec la récursivité)
            if ($level == 0) {
                $html .= $this->_HTML_listeSitesLecteurs($lecteurs_id, $site_enfant_id);
            }
        }

        // il y a au moins un site ou un lecteur, on affiche
        if ($html != "") {
          $html = "<li class='site'>". I_SITE. $site->libelle. "<ul>". $html;
          $html .= "</ul></li>";
        }
        unset($site);

        return $html;
    }

    public function listeProfils_Lecteur($lecteur_id)
    {
        $array = array();
        $query = "SELECT groupeId ".
            "FROM {$this->dbs['mssql']->getTableName('droit')} ".
            "WHERE lecteurId={$lecteur_id}";
        $ret = $this->dbs['mssql']->query ($query);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Problème dans la requête ". $query);
            return ! $this->error;
        }

        while ($record = $this->dbs['mssql']->fetchObject()) {
          $array[$record->groupeId] = $record->groupeId;
        }
        $this->dbs['mssql']->freeResult();

        $this->message = "";
        $this->error = false;
        return $array;
    }
    
    /*
     * retourne un tableau (horaire=> array(badges))
     */
    public function listeBadgesHoraires_Lecteur(int $lecteurId)
    {
        $this->message = "";
        $this->error = false;
        
        $array = array();
        $badge = new Badge($this->objects);
        if ($badge->error) {
            $this->message .= $badge->message;
            $this->error = true;
        }
        
        $query = "SELECT DISTINCT groupeId ".
            "FROM {$this->dbs['mssql']->getTableName('droit')} ".
            "WHERE lecteurId={$lecteurId}";
        $ret = $this->dbs['mssql']->query ($query);
        if ($ret == false) {
            $this->error = true;
            $this->message .= HTML_error("Problème dans la requête ". $query);
            return ! $this->error;
        }
        // on stocke les réponses
        $aProfils = array();
        while ($record = $this->dbs['mssql']->fetchObject()) {
            $aProfils[$record->groupeId] = $record->groupeId;
        }
        $this->dbs['mssql']->freeResult();

        // on balaye tous les profils
        foreach ($aProfils as $groupeId) {
            $this->id = $groupeId;
            $this->get();
            // récup de l'horaire (il n'y en a qu'un pour un lecteur)
            $horaire_id = $this->droits[$lecteurId];

            // on récupère les badges
            $aBadges = $badge->array_withProfil($groupeId);
            if ($badge->error) {
                $this->message .= $badge->message;
                $this->error = true;
            }
            
            $array[$horaire_id] = isset($array[$horaire_id]) ? 
                        array_merge($array[$horaire_id], $aBadges) :
                        $aBadges;
        }

        return $array;
    }

    /*
     * Commande la mise à jour des centrales
     */
    private function commit() : bool
    {
        // commit avec le numéro badge
        $sql = "INSERT INTO ". $this->dbs['mssql']->getTableName('commit').
                " (zoneModifiee, numeroBadge, action)".
                " VALUES (128, 0, 0)";
        $ret = $this->dbs['mssql']->query($sql);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de commande la mise à jour sur les centrales");
        }
        return ! $this->error;
    }
  
}
