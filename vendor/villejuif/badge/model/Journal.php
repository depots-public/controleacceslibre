<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace Badge\Model;
require_once('Badge.php');
require_once('Lecteur.php');
require_once('Site.php');

/* Journal est la consultation des accès acceptés ou refusées sur les
 * lecteurs par des badges.
 *
 * À ne pas confondre avec les logs qui sont ceux de cette application.
 */


class Journal {
    public 
            $id,
            $date,              # datetime
            $badge_numero,      # quel badge
            $porteurNom, $porteurPrenom, # porteur du badge a la date de l'événement
            $lecteur_id,        # quel lecteur
            $nature,            # réponse du lecteur / événement
            $lecteur,
            $badge,
            $error   = false, 
            $message = "",
            $filter; 
    
    private
            $dbs,
            $objects;    
    
    /* array
    * id est l'id de l'enregistrement dans le journal
    * avant, apres sont des dates
    * lecteur_id
    * site_id
    * nature est le message retourné par les centrales suite au badgeage
    */

    public function __construct(array $objects)
    {
        $this->objects = $objects;
        $this->dbs = $objects['dbs'];
        
        $this->filter = array(
        'id'=>FILTER_SANITIZE_NUMBER_INT,
        'avant'=>FILTER_SANITIZE_STRING,
        'apres'=>FILTER_SANITIZE_STRING,
        'badge'=>FILTER_SANITIZE_STRING,
        'site_lecteur'=>FILTER_SANITIZE_STRING,
        'nature'=>FILTER_SANITIZE_NUMBER_INT);
      
    }

    function get()
    {
      $query = "SELECT j.*, jt.libelle AS nature ".
          "FROM {$this->dbs['mssql']->getTableName('journal')} AS j ".
          "LEFT JOIN ". $this->dbs['mssql']->getTableName('journalT'). " AS jt ON j.typeEvenementId=jt.typeEvenementId ".
          "WHERE evenementId=?";
      $result = $this->dbs['mssql']->query ($query, [$this->id]);
      if (! ($record = $this->dbs['mssql']->fetchObject()))
      {
          $this->message = "Aucune trace avec l'id ". $this->id;
          $this->error = true;
      } else {
        $this->date         = date('d/m/Y H:i:s', strtotime($record->dateEvt));
        $this->badge_numero = $record->numeroRessource;
        $this->lecteur_id   = $record->lecteurId;
        $this->nature       = $record->nature;

        $this->lecteur = new Lecteur($this->objects);
        $this->lecteur->id = $this->lecteur_id;
        $this->lecteur->get();

        $this->badge = new Badge($this->objects);
        $this->badge->numero = $this->badge_numero;
        $this->badge->get();
        // Information du porteur du badge à la date de l'événément et pas l'actuel
        // porteur de badge...
        $this->porteurNom    = $record->porteurNom;
        $this->porteurPrenom = $record->porteurPrenom;

        $this->dbs['mssql']->freeResult($result);
        $this->message = "";
        $this->error = false;
      }
      return ! $this->error;
    }

    function HTML_view()
    {
      if (! $this->objects['session']->is_allowed('journal', $this->lecteur->site_id)) {
        $this->message = "Vous n'avez pas l'habilitation suffisante.";
        return false;
      }
      $html = "<fieldset><legend>Trace - n° {$this->id}</legend>";

      $html .= "<p><label class='inline' for='__date'>Quand :</label> ".
           "<strong>{$this->date}</strong></p>";
      $html .= "<p><label class='inline' for='__badge_numero'>Quel badge :</label> ".
           "<strong>{$this->badge->nom} {$this->badge->prenom} ({$this->badge->numero})". ($this->badge->statut != 'En service' ? $this->badge->statut : ''). "</strong></p>";
      $html .= "<p><label class='inline' for='__lecteur'>Quel lecteur :</label> ".
           "<strong>{$this->lecteur->nom} ({$this->lecteur->id})". ($this->lecteur->statut != 'visible' ? $this->lecteur->statut : ''). "</strong></p>";
      $html .= "<p><label class='inline' for='__nature'>Réponse :</label> ".
           "<strong>{$this->nature}</strong></p>";

      $html .= "</fieldset>\n";

      return $html;
    }

    private function _valueList($champ) : array
    {
      $rep = array();

      switch ($champ) {
        case 'nature' : 
            $query = "SELECT j.typeEvenementId AS value, jt.libelle AS display ".
              " FROM {$this->dbs['mssql']->getTableName('journal')} AS j".
              " LEFT JOIN {$this->dbs['mssql']->getTableName('journalT')} AS jt ON j.typeEvenementId=jt.typeEvenementId". 
              " GROUP BY j.typeEvenementId, jt.libelle ORDER BY jt.libelle";
            break;
        default :
            $this->error = true;
            $this->message .= HTML_error("Type de champ non préparé.");
            break;
      }
      $ret = $this->dbs['mssql']->query ($query);
      if ($ret === false) {
          $this->error = true;
          $this->message .= HTML_error("Impossible de lire les traces du journal");
      } else {
        while ($record = $this->dbs['mssql']->fetchObject($ret)) {
          $rep[$record->value] = $record->display;
        }
        $this->dbs['mssql']->freeResult($ret);
      }
      return ($this->error ? array() : $rep);
    }

    private function _optionList($champ, $defaultValue) : string
    {
      $rep = $this->_valueList($champ);
      $html = "<option></option>"; // Choisir $champ
      foreach($rep as $val=>$display) $html .= "<option value={$val}". ($defaultValue!='' && $val==$defaultValue ? " selected='selected'" : ""). ">$display</option>";
      return $html;
    }

    function HTML_searchForm() : string
    {
      $html = "<fieldset><legend>Filtres sur le journal des accès</legend>";
      $html .= "<form method='post' action='index.php?journal/list/'>".
        "<label for='__id'>Id de la trace</label><input id='__id' type='text' name='_filter[id]' value='".
        $this->filter['id']. "'> ".
        "<label for='__apres'>Entre le</label><input id='__apres' type='datetime-local' name='_filter[apres]' value='".
        $this->filter['apres']. "'>".
        "<label for='__avant' class='inline'>et le</label><input id='__avant' type='datetime-local' name='_filter[avant]' value='".
        $this->filter['avant']. "'><br/>";

      $html .= "<label for='__badge' class='inline'>Badge</label><input id='__badge' name='_filter[badge]' value='".
        $this->filter['badge']. "'/></select>";

      $html .= " <label for='__site_lecteur'>Site/lecteur</label> <select id='__site_lecteur' name='_filter[site_lecteur]'>";
      $listeLecteurs = new Lecteur($this->objects);
      // TODO : ne présenter que les sites pour lesquels l'utilisateur a le droit de voir le journal
      $array = array();
      foreach($listeLecteurs->array_list_rights('journal') as $key=>$value) {
        // C'est soit un site, soit un lecteur
        if (substr($value[1],0, strlen(I_SITE)) == I_SITE || $value[1]=='Racine') {
          $value[0] = "S". $value[0];
          $value[1] = str_replace(I_SITE, '', $value[1]);
        } else {
          $value[1] = str_replace(I_LECTEUR, '', $value[1]);
        }
        $array[$key] = $value;
      }
      $html .= HTML_array2options($array, $this->filter['site_lecteur'], false);
      unset($listeLecteurs, $array);
      $html .= "</select>";
      $html .= "<label for='__nature'>Réponse</label><select id='__nature' name='_filter[nature]'>".
        $this->_optionList('nature', $this->filter['nature']). "</select>";

      $html .= "<p><input type='submit' value='Mettre à jour la liste des résultats'/> ".
        "<a class='button' href='?{$this->objects['module']}/list'>Retour à la liste</a></p>";
      $html .= "</form></fieldset>";
      return $html;
    }

    function HTML_searchResult($title = "Résultats")
    {
      if (! $this->objects['session']->droits['journal']) {
        $this->message = "Vous n'avez pas l'habilitation suffisante.";
        return false;
      }
      $html = "<div id='journal_details'></div>";
      $html .= "<fieldset id='journal_results'><legend>{$title}</legend>";
      $listeSitesAutorises = $this->objects['session']->mesSitesAutorises('journal');
      if (! in_array(0, $listeSitesAutorises)) {
        $html .= HTML_message("Les réponses affichées dépendent de votre habilitation.");
      }
      if ($this->filter) {
        if ($res = $this->_searchQuery()) {
          $list_id = $this->_searchList($res);
          $html .= HTML_array2table($list_id);
          if (count($list_id) == 100) {
            $html .= HTML_message("Il y a peut-être d'autres réponses, mais seules 100 sont affichées.");
          }
        } else {
          if ($this->message>"") {
            $html .= $this->message;
          } else {
            $html .= HTML_message("Aucune trace avec ces filtres.");
          }
        }
      }
      $html .= "</fieldset>";
      return $html;
    }

    /*
     * execute une requête en fonction des filtres
     * Les filtres s'ajoutent les uns aux autres et sont tous optionnels.
     * Le résultat se limite aux 100 derniers logs.
     */
    private function _searchQuery()
    {
        $query = "SELECT DISTINCT j.evenementId as id, dateEvt FROM {$this->dbs['mssql']->getTableName('journal')} AS j";

        // selon les habilitations
        $listeSitesAutorises = $this->objects['session']->mesSitesAutorises('journal');
        $params = [];
        /*if (! in_array(0, $listeSitesAutorises) || (isset($this->filter['site_id']) && $this->filter['site_id'] != '')) {
          $query .= " LEFT JOIN ". $this->dbs['mssql']->getTableName('lecteur'). " AS l ON j.lecteurId=l.lecteurId";
        }*/
        if (! in_array(0, $listeSitesAutorises)) {
            $this->lecteur = new Lecteur($this->objects);
            $lecteursAutorises = array();
            foreach($listeSitesAutorises as $site_id) {
                $lecteursAutorises = array_merge($lecteursAutorises, $this->lecteur->array_listInSite($site_id));
            }
            $query .= " AND j.lecteurId IN (". implode(',', $lecteursAutorises). ")";
            unset($lecteursAutorises);
      } elseif (array_key_exists('badge', $this->filter) && $this->filter['badge'] != '' && ! is_numeric($this->filter['badge'])) {
        $query .= " LEFT JOIN ". $this->dbs['mssql']->getTableName('badge'). " AS b ON numeroRessource=b.numeroBadge".
          " WHERE (0=0)";
      } else {
        $query .= " WHERE (0=0)";
      }
      // selon les filtres
      foreach(
        array('id'=>'evenementId', 'date'=>'dateEvt', 'lecteur_id'=>'j.lecteurId', 'nature'=>'typeEvenementId') 
        as $field=>$tableField
      ) {
          if (array_key_exists($field, $this->filter) && $this->filter[$field] != '') {
          //$query  .= " AND {$tableField}= ". $this->dbs['mssql']->quoteSmart($this->filter[$field]);
          $query  .= " AND {$tableField}=:{$field}";
          $params[$field] = $this->filter[$field];
        }
      }
      // badge - particulier, on peut chercher le nom ou le numéro
      if (array_key_exists('badge', $this->filter) && $this->filter['badge'] != '') {
        if (is_numeric($this->filter['badge'])) {
          $query .= " AND numeroRessource=:badge";
        } else {
          $query .= " AND (CONCAT(porteurNom, porteurPrenom) LIKE :badge".
          //" OR porteurPrenom LIKE :badge".
          ")";
          $this->filter['badge'] = "%". $this->filter['badge']. "%";
        }
        $params['badge'] = $this->filter['badge'];
      }
      // dates
      if (isset($this->filter['apres']) && $this->filter['apres'] != '') {
        $query .= " AND dateEvt >= CONVERT(datetime, :apres, 126)";
        $params['apres'] = $this->filter['apres'];
      }
      if (isset($this->filter['avant']) && $this->filter['avant'] != '') {
        $query .= " AND dateEvt <= CONVERT(datetime, :avant, 126)";
        $params['avant'] = $this->filter['avant'];
      }
        // site/lecteur
        if (isset($this->filter['site_id']) && $this->filter['site_id'] != '' && $this->filter['site_id'] != 0) {
            $this->lecteur = new Lecteur($this->objects);
            $lecteursFiltres = $this->lecteur->array_listInSite($this->filter['site_id'], true);
            $query .= " AND lecteurId IN (". implode(',', $lecteursFiltres). ")";
        }

        // limite aux 100 premiers résultats
        $query .= " ORDER BY dateEvt DESC OFFSET 0 ROWS FETCH NEXT 100 ROW ONLY";
        //print_r($query);
      
        $ret = $this->dbs['mssql']->query ($query, count($params) > 0 ? $params : null);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Erreur dans la requête "). HTML_debug($query);
            var_dump($params);
        } else {
            $rep = array();
            while ($record = $this->dbs['mssql']->fetchObject($ret)) {
                $rep[] = $record->id;
            }
            $this->dbs['mssql']->freeResult($ret);
        }
        return ($this->error ? false : $rep);
    }

    /*
     * retourne un tableau complet des données
     * à partir d'un autre tableau des id seulement
     */
    private function _searchList($list)
    {
      $array = array();
      foreach ($list as $id) {
        $this->id = $id;
        $this->get();
        $nom = $this->porteurNom. " ". $this->porteurPrenom;
        if ($this->badge->nom. " ". $this->badge->prenom != $nom) {
            $nom = "ancien porteur ". $nom;
        }
        $array[] = array(
          'id'      => $this->id,
          'Date'    => $this->date,
          'Badge'   => $nom. " (<a href='?badge/view/{$this->badge_numero}'><i class='fas fa-eye'></i>". $this->badge_numero. "</a>)",
          //'Site'    => $this->lecteur->site->libelle. " (". $this->lecteur->site_id. ")",
          'Lecteur' => $this->lecteur->libelle. " (". $this->lecteur->id. ")",
          'Réponse' => $this->nature
        );
      }
      return $array;
    }

    function fromSearchForm()
    {
      if (!isset($_REQUEST['_filter']) || !is_array($_REQUEST['_filter'])) return false;

      foreach(array(
        'id'=>FILTER_SANITIZE_NUMBER_INT,
        'badge'=>FILTER_SANITIZE_STRING,
        'site_lecteur'=>FILTER_SANITIZE_STRING,
        'nature'=>FILTER_SANITIZE_NUMBER_INT
        ) as $field=>$sanitize) {
        $this->filter[$field] = isset($_REQUEST['_filter'][$field]) && $_REQUEST['_filter'][$field]>"" ? filter_var($_REQUEST['_filter'][$field], $sanitize) : "";
      }
      foreach(array(
        'avant'=>FILTER_SANITIZE_STRING,
        'apres'=>FILTER_SANITIZE_STRING,
        ) as $field=>$sanitize) {
        $this->filter[$field] = isset($_REQUEST['_filter'][$field]) && $_REQUEST['_filter'][$field]>"" ? filter_var($_REQUEST['_filter'][$field].':00', $sanitize) : "";
      }
      // soit un site, soit un lecteur
      if (is_numeric($this->filter['site_lecteur'])) {
        $this->filter['lecteur_id'] = $this->filter['site_lecteur'];
        $this->filter['site_id'] = "";
      } else {
        $this->filter['site_id'] = filter_var(substr($this->filter['site_lecteur'], 1), FILTER_SANITIZE_NUMBER_INT);
        $this->filter['lecteur_id'] = "";
      }
    }
  }

