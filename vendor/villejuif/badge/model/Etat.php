<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace Badge\Model;
require_once('Lecteur.php');
require_once('Diff.php');

class Etat {
    public 
            $id = null,
            $date,
            $lecteur_id,
            $html_droits,
            $error   = false,
            $message = "";
    private 
            $dbs,
            $objects;

    public function __construct(array $objects)
    {
        $this->objects = $objects;
        $this->dbs = $objects['dbs'];
        $this->site = new Site($this->objects);
    }

    function get() : bool
    {
        $query = "SELECT * ".
            "FROM {$this->dbs['mariadb']->getTableName('etat')} AS e ";
        if ($this->id > 0) {
            $query .= "WHERE id=?";
            $values = array($this->id);
        } elseif ($this->date > "" && $this->lecteur_id > 0) {
            $query .= "WHERE date=? AND lecteur_id=?";
            $values = array($this->date, $this->lecteur_id);
        } else {
            $this->message .= "Aucun critère de recherche d'un état";
            $this->error = true;
            return false;
        }
        $result = $this->dbs['mariadb']->query ($query, $values);
        if (! ($record = $this->dbs['mariadb']->fetchObject()))
        {
            $this->message .= "Aucun état trouvé";
            //$this->error = true;
            return false;
        }

        $this->id           = $record->id;
        $this->date         = $record->date;
        $this->lecteur_id   = $record->lecteur_id;
        $this->html_droits  = $record->html_droits;

        $this->dbs['mariadb']->freeResult();
        
        return ! $this->error;
    }
    
    public function save() : bool
    {
        // uniquement des insertions, pas de modifications
        $sql = "REPLACE INTO ". $this->dbs['mariadb']->getTableName('etat').
            " (date, lecteur_id, html_droits) VALUES (?, ?, ?)";
        $params=[$this->date, $this->lecteur_id, $this->html_droits];

        $ret = $this->dbs['mariadb']->query($sql, $params);
        if ($ret === false) {
            $this->error=true;
            $this->message .= HTML_error("Impossible d'enregistret l'état"). HTML_debug($sql);
        }
        $this->id = $this->dbs['mariadb']->insertid();

        return ! $this->error;
    }
        
    /* 
     * Enregistre pour la date du jour l'état des droits
     * Un enregistrement par lecteur
     */
    public function saveAllLecteurs() : string
    {
        $html = "";
        
        // liste des lecteurs d'un site
        $lecteur = new \Badge\Model\Lecteur($this->objects);
        $aLecteurs = $lecteur->array_listInSite(null, true);
      
        // balayage de chaque lecteur
        $n=0;
        foreach ($aLecteurs as $lecteur_id) {

            $lecteur->id        = $lecteur_id;
            $lecteur->get();
            
            // est-ce déjà fait ?
            $this->id           = null;
            $this->date         = date('Y-m-d');    
            $this->lecteur_id   = $lecteur_id;
            $this->get();
            if ($this->error) {
                $html .= HTML_error($this->message);
            }
            if ($this->id !== null) {
                //$html .= "<li>{$lecteur->libelle} déjà enregistré ce jour (". $this->id. ")</li>";
                continue;
            }
           
            // on met à jour
            $this->lecteur_id   = $lecteur_id;
            $this->html_droits  = $lecteur->text_who('horaire');
            $this->save();

            $html .= "{$lecteur->libelle} ($this->id)\n";

            if (++$n >= 20) break;
        }
        if ($n==0) {
            $html .= "Aucun lecteur à ajouter à l'état ce jour.\n";
        }
        
        return $html;
    }
    
    public function diffUnLecteur() : string
    {
        
        $html = "";

        $lecteur = new Lecteur($this->objects);
        $lecteur->id        = $this->lecteur_id;
        $lecteur->get();
        $this->error &= $lecteur->error;
        $this->message .= $lecteur->message;
        
        // ce jour
        $this->id           = null;
        $this->date         = date('Y-m-d');    
        $this->get();

        // hier
        $hier = new Etat($this->objects);
        $hier->date         = date('Y-m-d', strtotime("yesterday"));    
        $hier->lecteur_id   = $this->lecteur_id;
        $hier->get();
        $this->error &= $hier->error;
        $this->message .= $hier->message;

        $html_lecteur = "<h3>{$lecteur->libelle} ({$lecteur->id})</h3>";

        $aDiff = Diff::compare($hier->html_droits, $this->html_droits);
        
        $horaire = "";
        $diff = array();
        $html_horaire = "";
        foreach ($aDiff as $aLine) {
            if ($aLine[0] === "") {
                continue;
            }
            if ($aLine[0][0] == "*") {
                if (count($diff)>0) {
                    $html_horaire .= "<h4>{$horaire}</h4><ul><li>". implode("</li><li>", $diff). "</li></ul>";
                    $diff = array();
                }
                $horaire = $aLine[0];
            } elseif ($aLine[1] != Diff::UNMODIFIED) {
                $diff[] = ($aLine[1] == Diff::INSERTED ? "+" : "-"). $aLine[0];
            }
        }
        if (count($diff) > 0) {
            $html_horaire .= "<h4>{$horaire}</h4><ul><li>". implode("</li><li>", $diff). "</li></ul>";
        }

        if ($html_horaire > "") {
            $html .= $html_lecteur. $html_horaire;
        }
        
        return $html;
    }

    public function diffLecteurs() : string
    {
        // liste des lecteurs d'un site
        $lecteur = new Lecteur($this->objects);
        $aLecteurs = $lecteur->array_listInSite(null, true);
        $this->error &= $lecteur->error;
        $this->message .= $lecteur->message;
      
        // balayage de chaque lecteur
        $aDiff = array();
        foreach ($aLecteurs as $lecteur_id) {
            $lecteur->id = $lecteur_id;
            $lecteur->get();
            $this->error &= $lecteur->error;
            $this->message .= $lecteur->message;
            foreach ($lecteur->site->responsables() as $responsable) {        
                $this->lecteur_id = $lecteur_id;
                if (($temp = $this->diffUnLecteur()) > "") {
                    if (!isset($aDiff[$responsable])) {
                        $aDiff[$responsable] = "";
                    }
                    $aDiff[$responsable] .= $temp;
                    unset($temp);
                }
            }
        }
        unset($lecteur, $aLecteurs, $responsable, $lecteur_id);

        if (count($aDiff)>0) {
            // balayage de tous les responsables
            $html_date = date('d/m/Y', strtotime('yesterday'));
            $html_before = "<html>\r\n<body>\r\n".
                    "<p>Ci-dessous les évolutions dans les droits d'accès\r\n".
                    "par lecteur de badge durant la journée du {$html_date}.</p>\r\n";
            $html_end = "</body></html>\r\n";
            foreach ($aDiff as $responsable=>$html_diff) {
                if (! $this->mailHtml(
                        $responsable, "évolution le ". $html_date, 
                        $html_before. $html_diff. $html_end
                )) {
                    $this->message .= "Erreur lors de l'envoi d'un courriel.\n";
                    $this->error = true;
                    break;
                };
            }
            $html = "Courriel envoyé à : ";
            $html .= implode(", ", array_keys($aDiff));
            $html .= ".\n";
        } else {
            $html = "Aucune évolution, aucun courriel envoyé.\n";
        }
        
        return $html;
    }
    
    private function mailHtml (string $to, string $title, string $html) : bool
    {
        $title = '=?UTF-8?B?'.base64_encode("[BADGES] " . $title).'?=';

        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=UTF-8';
        $headers[] = "X-Mailer: PHP/". phpversion(). "-badges";
        $headers[] = "Content-Transfert-Encoding: 8bit";
        $headers[] = 'From: Badges-ne-pas-repondre <ne-pas-repondre@villejuif.fr>';
     
        return mail($to, $title, $html, implode("\r\n", $headers));
    }

    public function archivage() : string
    {
        $query = "DELETE FROM {$this->dbs['mariadb']->getTableName('etat')} ".
            "WHERE date < ?";
        $values = array(date('Y-m-d', strtotime("-3 month")));
        
        $result = $this->dbs['mariadb']->query ($query, $values);
        
        $n = $this->dbs['mariadb']->numRows();

        if ($result === false) {
            $this->message .= "Problème dans la requête d'archivage\n";
            $this->error = true;
            return "Erreur.";
        } else {
            return "Nombre d'enregistrements supprimés : ". $n. "\n";
        }
    }
    
} // end class