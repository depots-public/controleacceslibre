<?php
/*
 * ControleAccesLibre
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2023 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace Badge\Model;

require_once('Societe.php');

// =====================================================================

class Horaire {
    public 
        $id = null,              # indexPlage (sert de lien pour tout sauf avec les plages et sociétés)
        $plageHoraireId,         # plageHoraireId (ne sert de lien que pour les plages et les sociétés)
        $nom,                    # nom de l'horaire qui n'est pas automatique
        $horaires = array(),     # array of array(jour, heure debut, heure fin)
        $type,                   # type d'enregistrement des plages horaires
        $statut = 'nouveau',     # nouveau (temporaire), visible ou cache
        $datemaj,
        $error   = false, 
        $message = "";
    private
        $dbs,
        $objects;
            
    private static
        $nombreTranchesMaxi = 3,
        $jourSemaine = array(
            1=>'lundi',        2=>'mardi',  3=>'mercredi', 4=>'jeudi', 
            5=>'vendredi',     6=>'samedi', 7=>'dimanche', 
            8=>'jours fériés', 9=>'tous les jours (lundi-vendredi)', 10=>'week-end'),
        $typeCreneau = array(0=>'détaillé',
            1=>'quotidien / tous les jours',
            2=>'semaine / week-end'),
        $jourType = [
            [1,2,3,4,5,6,7,8],    // 0 = 'détaillé'
            [9,8],                // 1 = 'quotidien / tous les jours'
            [9,10,8]              // 2 = 'semaine / week-end'
          ];
    
    public function __construct(array $objects)
    {
        $this->objects = $objects;
        $this->dbs = $objects['dbs'];
    }

    private function addHoraire($jour, $heure_debut, $heure_fin)
    {
        $this->horaires[] = array($jour, $heure_debut, $heure_fin);
    }

    /*
     * charge l'objet des valeurs récupérées dans la base à partir de l'id
     * retourne true/false selon le succès
     */
    public function get() : bool
    {
        // cas particuliers
        // qui n'existe pas
        if ($this->id === -1) {
          $this->nom         = 'Jamais';
          $this->statut      = 'visible';
          return true;
        }
        // permanent
        if ($this->id === 0) {
          $this->nom         = 'Permanent';
          $this->statut      = 'visible';
          return true;
        }

        $query = "SELECT * ".
            "FROM {$this->dbs['mssql']->getTableName('horaire')} ".
            "WHERE indexPlage=?";
        $result = $this->dbs['mssql']->query ($query, [$this->id]);
        if (! ($record = $this->dbs['mssql']->fetchObject()))
        {
            $this->message = "Aucun profil horaire avec l'id ". $this->id;
            $this->error = true;
        } else {
          $this->plageHoraireId = $record->plageHoraireId;
          $this->nom         = $record->libelle;
          $this->statut      = 'visible'; //$record->status;
          //$this->datemaj     = $record->updatedate;
          $this->type        = $record->type;
          
          // recupération des créneaux
          $this->horaires = array();
          $query = "SELECT * FROM ". $this->dbs['mssql']->getTableName('horaireC').
              " WHERE plageHoraireId=?".
              " ORDER BY JourId, debut";
          $result = $this->dbs['mssql']->query ($query, [$this->plageHoraireId]);
          while ($record = $this->dbs['mssql']->fetchObject())
          {
              $this->addHoraire($record->JourId, $record->debut, $record->fin);
              //$this->horaires[] = []$record->jourheure=='' ? array() : json_decode($record->jourheure, true));
          }
          //$this->dbs['mssql']->freeResult();

          $this->message = "";
          $this->error = false;
        }
        return ! $this->error;
    }

    private function getPlageHoraireId() : bool
    {
        $horaire = new Horaire($this->objects);
        $horaire->id = $this->id;
        $ret = $horaire->get();
        $this->plageHoraireId = $horaire->plageHoraireId;
        unset($horaire);
        return $ret;
    }
    
    /*
     * met à jour l'horaire dans la base
     * retourne true/false selon succès
     */
    function save()
    {
        if ($this->id > 0) {
            $sql = "UPDATE ". $this->dbs['mssql']->getTableName('horaire').
                " SET libelle=?, type=?".
                " WHERE indexPlage=?";
            $params = [$this->nom, $this->type, $this->id];
            $this->getPlageHoraireId(); // renseigne $this->plageHoraireId
        } else {
            // trouver la max indexPlage
            $sql = "SELECT indexPlage FROM ". $this->dbs['mssql']->getTableName('horaire').
                    " ORDER BY indexPlage";
            $ret = $this->dbs['mssql']->query($sql);
            if ($ret === false) {
                $this->error = true;
                $this->message .= HTML_error("Impossible de trouver l'index des plages horaire"). 
                        HTML_debug($sql);
                $itemIndex = 0;
            } else {
                $index = array();
                while ($record = $this->dbs['mssql']->fetchObject())
                {
                    $index[] = $record->indexPlage;
                }
                for ($itemIndex=1; in_array($itemIndex, $index); $itemIndex++) ;
            }
            // ajout de l'horaire
            $sql = "INSERT INTO ". $this->dbs['mssql']->getTableName('horaire').
                " (libelle, type, indexPlage)".
                " VALUES (?, ?, ?)";
            $params = [$this->nom, $this->type, $itemIndex];
        }
        $ret = $this->dbs['mssql']->query($sql, $params);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible d'enregistrer le profil horaire"). 
                    HTML_debug($sql);
        } else {
            if ($this->id==0) {
              $this->plageHoraireId = $this->dbs['mssql']->insertid();
            }
            // enregistrer dans les logs
            $log = new Log($this->objects);
            $log->add('save', 'horaire', $this->id, $this);
            //var_dump($log->sur_valeur);
            unset($log);
            //die();
        }
        
        // ajouter les créneaux
        $query = "DELETE FROM {$this->dbs['mssql']->getTableName('horaireC')}".
            " WHERE plageHoraireId=?";
        $ret = $this->dbs['mssql']->query ($query, [$this->plageHoraireId]);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de supprimer les créneaux de l'horaire"). 
                    HTML_debug($query);
        }
        $query = "INSERT INTO ". $this->dbs['mssql']->getTableName('horaireC').
                " (plageHoraireId, jourId, debut, fin) VALUES (?, ?, ?, ?)";
        foreach($this->horaires as $horaire)
        {
            $ret = $this->dbs['mssql']->query($query, [$this->plageHoraireId, $horaire[0], $horaire[1], $horaire[2]]);
            if ($ret === false) {
                $this->error = true;
                $this->message .= HTML_error("Impossible d'enregistrer les créneaux de l'horaire"). 
                        HTML_debug($query);
                print_r(array([$this->plageHoraireId, $horaire[0], $horaire[1], $horaire[2]]));
                echo "<br/>\n";
            }
        }
        
        // ajouter tous les sites/sociétés
        $societe = new Societe($this->objects);
        $aSocietes = $societe->array_list();

        $query = "DELETE FROM {$this->dbs['mssql']->getTableName('horaireS')}".
            " WHERE plageHoraireId=?";
        $ret = $this->dbs['mssql']->query ($query, [$this->plageHoraireId]);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de supprimer les sites de l'horaire"). 
                    HTML_debug($query);
        }
        $query = "INSERT INTO {$this->dbs['mssql']->getTableName('horaireS')} ".
                " (plageHoraireId, societeId) VALUES (?, ?)";
        foreach($aSocietes as $aSociete)
        {
            $ret = $this->dbs['mssql']->query($query, [$this->plageHoraireId, $aSociete['id']]);
            if ($ret === false) {
                $this->error = true;
                $this->message .= HTML_error("Impossible d'enregistrer les sites de l'horaire"). 
                        HTML_debug($query). HTML_debug($this->plageHoraireId). HTML_debug($aSociete['id']);
            }
        }
        
        if (!$this->error) {
            $this->commit();
        }
        
        return ! $this->error;
    }

    /*
     * met à jour l'horaire dans la base
     * retourne true/false selon succès
     */
    public function delete() : bool
    {
        if ($this->id == 0) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de trouver l'index des plages horaire"); 
        }
        if ($this->is_used()) {
            $this->error = true;
            $this->message .= HTML_error("L'horaire est utilisé, impossible de le supprimer"); 
        }
        if ($this->delete !== "delete") {
            $this->error = true;
            $this->message .= HTML_error("Suppression non confirmée"); 
        }
        if ($this->error) {
            return false;
        }
        
        // supprimer les créneaux
        $this->getPlageHoraireId();
        if (! is_numeric($this->plageHoraireId)) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de retrouver l'index des plages horaire");
            return false;
        }
        $query = "DELETE FROM {$this->dbs['mssql']->getTableName('horaireC')}".
            " WHERE plageHoraireId=?";
        $ret = $this->dbs['mssql']->query ($query, [$this->plageHoraireId]);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de supprimer les créneaux de l'horaire"). 
                    HTML_debug($query);
            return false;
        }
        
        // supprimer les sites/sociétés de l'horaire
        $societe = new Societe($this->objects);
        $aSocietes = $societe->array_list();
        $query = "DELETE FROM {$this->dbs['mssql']->getTableName('horaireS')}".
            " WHERE plageHoraireId=?";
        $ret = $this->dbs['mssql']->query ($query, [$this->plageHoraireId]);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de supprimer les sites de l'horaire"). 
                    HTML_debug($query);
            return false;
        }
        
        // supprimer l'horaire
        $sql = "DELETE FROM ". $this->dbs['mssql']->getTableName('horaire').
                " WHERE indexPlage=?";
        $params = [$this->id];
        $ret = $this->dbs['mssql']->query($sql, $params);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de supprimer le profil horaire"). 
                    HTML_debug($sql);
            return false;
        }
        
        // enregistrer dans les logs
        $log = new Log($this->objects);
        $log->add('delete', 'horaire', $this->id, $this);
        unset($log);
                
        if (! $this->error) {
            $this->commit();
        }
        
        return ! $this->error;
    }

    // retourne true si l'horaire est utilisé dans un profil
    public function is_used() : bool
    {
        $query = "SELECT groupeId FROM ". $this->dbs['mssql']->getTableName('droit').
          " WHERE indexPlage=?";
        $ret = $this->dbs['mssql']->query($query, [$this->id]);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de récupérer le nombre d'utilisations de l'horaire"). 
                    HTML_debug($query). HTML_debug($this->id);
            echo $this->message;
        }
        return $this->dbs['mssql']->fetchObject() ?  true : false;
    }

    function sortHoraires()
    {
        sort($this->horaires);
    }

    // fond les tranches horaires si elle se jouxtent ou se chevauchent
    function optimizeHoraires()
    {
        $this->sortHoraires();
        foreach ($this->horaires as $index=>$tranche) {
            if (isset($precindex) && $this->horaires[$precindex][0]==$tranche[0]) {
                if ($tranche[1]<=$this->horaires[$precindex][2]) {
                    $fin = max($tranche[2], $this->horaires[$precindex][2]);
                    $this->horaires[$precindex][2] = $fin;
                    unset($this->horaires[$index]);
                    $index = $precindex;
                }
            }
            $precindex = $index;
        }
    }

    // formulaire de modification des horaires
    function HTML_form()
    {
        $html  = "<form name='profilHoraires' method='post' action='?horaire/fromForm/{$this->id}'>";
        $html .= "<fieldset><legend>Identité du profil horaire";
        if ($this->id) $html .= " - n° {$this->id}";
        $html.= "</legend>";
        if ($this->id) $html .= "<input type='hidden' id='__pid' name='__pid' value='{$this->id}' />".
         "<input type='hidden' id='__pnominit' name='__pnominit' value='{$this->nom}' />";
        $html.= " <label for='__pnom' class='inline'>Nom<em>*</em></label> ".
         "<input type='text' id='__pnom' name='__pnom' value=\"{$this->nom}\" required='required' /><br/>";
        $html.= " <label for='__pstatut' class='inline'>Statut<em>*</em></label> <select id='__pstatut' name='__pstatut' disabled>";
        $html.= "   <option value='visible' ". ($this->statut!='cache' ? " selected" : ""). ">Visible</option>";
        $html.= "   <option value='cache' ". ($this->statut=='cache' ? " selected" : ""). ">Caché</option>";
        $html.= " </select><br/>\n";
        $html .= "</fieldset>\n";

        $html .= "<fieldset><legend>Tranches horaires</legend>";
        $html .=  HTML_message("Attention, la modification d'un profil horaire aura des répercutions sur les droits".
                " de tous les utilisateurs utilisant ce profil horaire. Il peut être plus prudent".
                " d'ajouter un nouvel horaire et d'affecter les bons utilisateurs ensuite.");
        $html .=  HTML_message("Pour un créneau à cheval sur minuit, faire deux créneaux."
                . " Par exemple de 22h à 4h du matin, créer 00:00 à 04:00 et 22:00 à 23:59.");
        $html.= " <label for='__ptype' class='inline'>Type d'horaire<em>*</em></label>".
                " <select id='__ptype' name='__ptype' onchange='changeType();'>";
        foreach (self::$typeCreneau as $type=>$texte) {
            $html.= "   <option value='{$type}' ". ($this->type==$type ? " selected" : ""). ">{$texte}</option>";
        }
        $html.= " </select><br/>\n";
        $html.= " <table>\n";
        $index_horaire=0;
        $classTranche='odd';
        for ($jour=1; $jour<=count(self::$jourSemaine); $jour++) {
          $html.= "  <tr id='jour{$jour}'><th>". self::$jourSemaine[$jour]. "</th>";
          for ($tranche=0; $tranche<self::$nombreTranchesMaxi; $tranche++) {
            while (!isset($this->horaires[$index_horaire]) & $index_horaire<count($this->horaires)) $index_horaire++;
            if ($index_horaire<count($this->horaires) && $this->horaires[$index_horaire][0] == $jour) {
              $hd = " value='". $this->_decodeHeure($this->horaires[$index_horaire][1]). "'";
              $hf = " value='". $this->_decodeHeure($this->horaires[$index_horaire][2]). "'";
              $index_horaire++;
            } else {
              $hd = $hf = "";
            }
            $html.= "<td class='$classTranche'> de <input name='__pde[{$jour}][{$tranche}]' id='__pde{$jour}{$tranche}' type='time'{$hd} /> à <input name='__pa[{$jour}][{$tranche}]' id='__pa{$jour}{$tranche}' type='time'{$hf} /></td>";
            if ($tranche<self::$nombreTranchesMaxi-1) $html.= "<th>&nbsp;+&nbsp;</th>";
            $classTranche = ($classTranche=='odd' ? 'even' : 'odd');
          }
          $html.= "<td> <a href='#' onclick='javascript:vider({$jour}); return false;' title='vider la ligne pour ce jour'>vider</a>";
          if ($jour==1) $html.= " <a href='#' onclick='javascript:recopier(); return false;' title='recopier les tranches horaires du lundi sur les autres jours'>recopier</a>";
          $html.= "</td>";
          $html.= "</tr>\n";
        }
        $html .= " </table>\n";
        $html .= "</fieldset>\n";
        $html .= " <input type='button' id='enregistrer' onclick='javascript:valid();' value='Enregistrer' ". (! $this->id ? "disabled " : "/>").
         " <input type='button' id='nouveau' onclick='javascript:validnew();' value='Enregistrer en tant que nouveau' />".
         " <a class='button' href='?{$this->objects['module']}/list'>Retour à la liste</a>\n";
        $html .= "</form>\n";
        // on envoie au javascript les valeurs des constantes PHP
        $html .= "<script>var ntranches=". self::$nombreTranchesMaxi. ";</script>\n".
            "<script>var njours=". count(self::$jourSemaine). ";</script>\n".
            "<script>var jourType=". JSON_encode(self::$jourType). ";</script>\n".
            "<script type='text/javascript' src='vendor/villejuif/badge/js/horaire.js'></script>".
            "<script>changeType();</script>";
        return $html;
    }

    private function _decodeHeure($minutes) : string
    {
        $h = sprintf("%'.02d", floor($minutes/60));
        $m = sprintf("%'.02d", $minutes - $h*60);
        return $h.":".$m;
    }
    
    private function _encodeHeure($heure) : int
    {
        list($h, $m) = explode(':', $heure);
        return 60*$h + 1*$m;
    }
    
    public function HTML_view($is_actionLinks = true) : string
    {
        $html = "<fieldset><legend>Horaire - n° {$this->id}</legend>";
        $html .= "<label class='inline' for='__nom'>Nom de l'horaire :</label> ".
         "<strong>{$this->nom}</strong><br/>";
        $html.= " <label class='inline' for='__statut'>Statut :</label> <strong>{$this->statut}</strong>";
        $html .= "</fieldset>\n";

        $html .= "<fieldset><legend>Tranches horaires</legend>";
        $html.= " <label class='inline' for='__type'>Type d'horaire :</label> <strong>". 
                self::$typeCreneau[$this->type]. "</strong>";
        $html.= " <table>\n";
        $classTranche='odd';
        $jour_prec = -1;
        foreach ($this->horaires as $key=>$horaire) {
          list($jour, $debut, $fin) = $horaire;
          if ($jour!=$jour_prec) {
            if ($jour_prec!=-1) $html.="</td></tr>";
            $html.= "  <tr id='jour{$jour}'><th>". self::$jourSemaine[$jour]. "</th><td class='$classTranche'>";
            $classTranche = ($classTranche=='odd' ? 'even' : 'odd');
            $jour_prec = $jour;
            $sep = "";
          }
          $html.= $sep. "de ". $this->_decodeHeure($debut). " à ". $this->_decodeHeure($fin);
          $sep = " et ";
        }
        $html .= " </table>\n";
        $html .= "</fieldset>\n";
        
        if ($is_actionLinks) {
            $html .= "<p><a href='?horaire/edit/{$this->id}' title='modifier'><i class='fas fa-edit'></i>Modifier</a>".
               ($this->is_used() ? '' : " <a href='?horaire/delform/{$this->id}' title='supprimer'><i class='far fa-trash-alt'></i>Supprimer</a>").
               "</p>";
        }
        return $html;
    }
    
    public function HTML_delform() : string
    {
        $html  = $this->HTML_view(false);
        $html .= "<form name='delHoraire' method='post' action='?horaire/fromDelForm/{$this->id}'>";
        $html .= "<input type='hidden' id='__pid' name='__pid' value='{$this->id}' />";

        $html .= "<fieldset><legend>Suppression de l'horaire</legend>";
        $html .= "<label for='__pdelete'>Je confirme la suppression irréversible</label>"
                . "<input type='checkbox' id='__pdelete' name='__pdelete' value='delete' />";
        $html .= "</fieldset>\n";
        $html .= " <input type='submit' id='enregistrer' value='Enregistrer la suppression' />"
                . "<a class='button' href='?{$this->objects['module']}/list'>Retour à la liste</a>\n";
        $html .= "</form>\n";
        return $html;
    }
    /*
     * affichage des tranches horaires d'un horaire par survol du nom
     * de l'horaire dans la liste
     */
    private function HTML_divView()
    {
        $html = "";
        $jour_prec = -1;
        foreach ($this->horaires as $key=>$horaire) {
          list($jour, $debut, $fin) = $horaire;
          if ($jour!=$jour_prec) {
            if ($jour_prec!=-1) $html.="<br/>";
            $html.= self::$jourSemaine[$jour];
            $jour_prec = $jour;
            $sep = " ";
          }
          $html.= $sep. "de ". $this->_decodeHeure($debut). " à ". $this->_decodeHeure($fin);
          $sep = " et ";
        }
        return $html;
    }

    private function HTML_div() : string
    {
        $html = "";
        $query = "SELECT indexPlage as id FROM {$this->dbs['mssql']->getTableName('horaire')} ORDER BY libelle";
        $this->dbs['mssql']->query($query);
        // obligé de faire en deux temps pour ne pas imbriquer des 
        // requêtes.
        // premier temps, on enregistre tous les horaire_id
        $resultats = array();
        while ($record = $this->dbs['mssql']->fetchObject()) {
            $resultats[] = $record->id;
        }
        $this->dbs['mssql']->freeResult();
        // second temps, on affiche les div
        foreach($resultats as $horaire_id) {
            $this->id = $horaire_id;
            $this->get();
            $html .= "<div id='h{$this->id}' class='horaire'>". $this->HTML_divView(). "</div>\n";
        }
        
        return $html;
    }

    /*
     * appelé après l'envoi d'un formulaire
     * charge l'objet avec les valeurs des champs
     * retourne le résutat du contrôle sur les valeurs
     */
    function fromForm()
    {
        if (isset($_POST['__pid'])) $this->id = $_POST['__pid'];
        $this->nom = filter_var($_POST['__pnom'], FILTER_SANITIZE_STRING);
        $this->statut = 'visible'; //($_POST['__pstatut'] == 'visible' ? 'visible' : 'cache');
        $this->type = filter_var($_POST['__ptype'], FILTER_SANITIZE_NUMBER_INT);
        for ($jour=1; $jour<count(self::$jourSemaine); $jour++) {
            for ($tranche=0; $tranche<self::$nombreTranchesMaxi; $tranche++) {
                if ($_POST['__pde'][$jour][$tranche]>'' && $_POST['__pa'][$jour][$tranche]>'') {
                    if ($_POST['__pde'][$jour][$tranche] >= $_POST['__pa'][$jour][$tranche]) {
                        $this->error = true;
                        $this->message = "Une tranche horaire est incorrecte,"
                                . " l'heure de début est égale ou plus tardive que l'heure de fin.";
                    } else {
                        $this->addHoraire(
                          $jour,
                          $this->_encodeHeure($_POST['__pde'][$jour][$tranche]),
                          $this->_encodeHeure($_POST['__pa'][$jour][$tranche])
                        );
                    }
                }
            }
        }
        return ! $this->error;
    }

    /*
     * appelé après l'envoi du formulaire de suppression
     * charge l'objet avec les valeurs des champs
     * retourne le résutat du contrôle sur les valeurs
     */
    function fromDelForm()
    {
      if (isset($_POST['__pid'])) $this->id = $_POST['__pid'];
      $this->delete = filter_var($_POST['__pdelete'], FILTER_SANITIZE_STRING);
      return true;
    }

    /*
     * retourne un tableau des profil horaires
     * (id, label, 1, liens-actions, option html)
     */
    function array_list($cache)
    {
      $array = array();
      $array[] = array(0, I_HORAIRE. "Permanent", 1, 
          "<i class='gauche fas fa-edit hidden'></i><i class='gauche far fa-trash-alt hidden'></i>", 
          'class="nodrop"');
      $query = "SELECT indexPlage AS id FROM {$this->dbs['mssql']->getTableName('horaire')} ".
          //(! $cache ? "WHERE status='visible'" : "").
          " ORDER BY libelle";
      $result = $this->dbs['mssql']->query($query);
      // ressource dbs non permanent. On est obligé de faire en deux temps
      // collecter les id, puis faire une boucle avec les get
      while ($record = $this->dbs['mssql']->fetchObject()) {
          $listeHoraires[] = $record->id;
      }
      foreach ($listeHoraires as $id) 
      {
        //$horaire = new Horaire($this->objects);
        $this->id = $id;
        $this->get();
        $span = "<a class='".
         ($this->statut!='visible' ? 'barre' : '').
         ($this->is_used() ? '' : ' unused').
         "' href='?horaire/view/{$this->id}' onmouseover='divDisplay(this, {$this->id})' onmouseout='divHide(this, {$this->id})'>";
        $array[] = array($this->id,
          I_HORAIRE. $span. $this->nom. '</a>',
         1,
         " <a href='?horaire/edit/{$this->id}' title='modifier'><i class='gauche fas fa-edit'></i></a>".
         ($this->is_used() ?
          "<i class='gauche far fa-trash-alt hidden'></i>" :
          " <a href='?horaire/delform/{$this->id}' title='supprimer'><i class='gauche far fa-trash-alt'></i></a>"),
         'class="nodrop"');
      }
      $this->dbs['mssql']->freeResult();
      return $array;
    }

    function HTML_array2options($array, $default, $empty=true)
    {
      $html = ($empty==true ? "<option". ($default===null ? " selected" : ""). "></option>" : "");
      foreach($array as $key=>$value)
      {
        $html .= "<option value='$key'".
          ($key==$default ? " selected" : "").
          ">". $value. "</option>";
      }
      return $html;
    }

    function HTML_list()
    {
      $html = "<script type='text/javascript' src='js/horaire.js'></script>";
      $html .= HTML_array2list($this->array_list(true));
      $html .= $this->HTML_div();
      return $html;
    }

    // arrayFrom resultat de array_list
    function JSON_array($arrayFrom)
    {
      $array = array();
      foreach($arrayFrom as $item) {
        $this->id = $item[0];
        $this->get();
        $array[/*$this->id*/] = array(
          'id'=>$this->id,
          'value'=>$this->nom
        );
      }
      return json_encode($array);
    }

    /*
     * retourne un tableau de (id, label, value) de horaire
     * en fonction d'une chaine recherchée dans
     * name
     */
    public function array_search($queryString) : array
    {
      $array = array();
      $sql = "SELECT indexPlage AS id, libelle AS name".
                " FROM {$this->dbs['mssql']->getTableName('horaire')}".
                " WHERE libelle LIKE ?".
                " ORDER BY libelle".
                " OFFSET 0 ROWS FETCH NEXT 20 ROW ONLY";

      $result = $this->dbs['mssql']->query ($sql, ['%'.$queryString.'%']);
        if (! $result) {
            $this->message = "Problème avec la requête de recherche ". HTML_debug($sql);
            $this->error = true;
            echo $this->message;
            return [false];
        }    

        // cas de Permanent
        if (stripos("Permanent", $queryString) !== false) {
            $array[] = array(
              'id'    => 0,
              'value' => "0 Permanent",
              'label' => "0 Permanent"
            );
        }

        while ($record = $this->dbs['mssql']->fetchObject()) {
            $array[] = array(
              'id'    => $record->id,
              'value' => $record->id. " ". $record->name,
              'label' => $record->id. " ". $record->name
            );
        }
      $this->dbs['mssql']->freeResult($result);
      return $array;
    }

    function HTML_lastlogs()
    {
        $log = new log($this->objects);
        $log->filter['sur_objet'] = "horaire";
        $log->filter['sur_id'] = $this->id;
        return $log->HTML_searchResult("Historique des logs");
    }

        /*
     * Commande la mise à jour des centrales
     */
    private function commit() : bool
    {
        // commit avec le numéro badge
        $sql = "INSERT INTO ". $this->dbs['mssql']->getTableName('commit').
                " (zoneModifiee, numeroBadge, action)".
                " VALUES (32, 0, 0)";
        $ret = $this->dbs['mssql']->query($sql);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de commande la mise à jour sur les centrales");
        }
        return ! $this->error;
    }

}
