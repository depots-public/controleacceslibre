<?php
/*
 * ControleAccesLibre
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2023 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace Badge\Model;
require_once('Site.php');
require_once('Profil.php');
require_once('Horaire.php');

class Lecteur {
    public 
            $id = null,
            $libelle,           # nom du lecteur
            $site_id,           # id du site de rattachement
            $site,              # objet site
            $error   = false,
            $message = "";
    private 
            $dbs,
            $objects;

    public function __construct(array $objects)
    {
        $this->objects = $objects;
        $this->dbs = $objects['dbs'];
        $this->site = new Site($this->objects);
    }

    function get($record = null) : bool
    {
	// Particularité des lecteurs:
        // ils sont enregistrés dans SQL (Senator), mais reliés à un site dans MariaDb
        // Comme on ne peut pas mélanger les requêtes, cela impose de
        // tout faire en deux temps
        
        If($record == null){
            $query = "SELECT l.libelle as libelle ".
                "FROM {$this->dbs['mssql']->getTableName('lecteur')} AS l ".
                "WHERE lecteurId=?";
            $result = $this->dbs['mssql']->query ($query, [$this->id]);
            if (! ($record = $this->dbs['mssql']->fetchObject()))
            {
                $this->message .= "Aucun lecteur avec l'id ". $this->id;
                $this->error = true;
                return false;
            }
        }
        $this->libelle = $record->libelle;
        $this->dbs['mssql']->freeResult();
        
        // on récupère la liste des sites
        $query = "SELECT site_id FROM {$this->dbs['mariadb']->getTableName('lecteurS')}".
                " WHERE lecteurId=?";
        $result = $this->dbs['mariadb']->query ($query, [$this->id]);
        if (! $result) {
            $this->message .= "Erreur dans la requête $query pour récupérer le site d'un lecteur.";
            $this->error = true;
            return false;
        }
        if ($record = $this->dbs['mariadb']->fetchObject()) {
            $this->site_id = $record->site_id;
        } else {
            $this->message = "Bizarerie pour retrouver le site d'un lecteur.";
            $this->error = true;
            //$this->site_id = \Badge\Model\Site::$racineId;
        }
        $this->dbs['mariadb']->freeResult();
        
        $this->site->id = $this->site_id;
        $this->site->get();
        $this->message .= $this->site->message;
        $this->error &= $this->site->error;

        return ! $this->error;
    }

    public function save() : bool
    {
        if ($this->id>0) {
            $sql = "UPDATE ". $this->dbs['mssql']->getTableName('lecteur').
                " SET libelle=? WHERE lecteurId=?";
            $params=[$this->libelle, $this->id];
        } else {
            $sql = "INSERT INTO ". $this->dbs['mssql']->getTableName('lecteur').
                " (libelle) VALUES ('?')";
            $params=[$this->libelle];
        }        
        $ret = $this->dbs['mssql']->query($sql, $params);
        if ($ret === false) {
            $this->error=true;
            $this->message .= HTML_error("Impossible d'enregistrer le lecteur"). HTML_debug($sql);
        } else {

            $this->commit();

            // enregistrer dans les logs
            $log = (new Log($this->objects))->add('save', 'lecteur', $this->id, $this);
            unset($log);
        }
        if ($this->id==0) {
            $this->id = $this->dbs['mssql']->insertid();
        }

        // enregistrement du site
        $query = "REPLACE INTO {$this->dbs['mariadb']->getTableName('lecteurS')} ".
                " (lecteurId, site_id) VALUES (?, ?)";
        $ret = $this->dbs['mariadb']->query($query, [$this->id, $this->site_id]);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible d'enregistrer le site du lecteur"). HTML_debug($query);
        }
        
        return ! $this->error;
    }

    function HTML_form() : string
    {
        $html = "<form name='site' method='post' action='?lecteur/fromForm/{$this->id}'>";
        $html .= "<fieldset><legend>Lecteur";
        if ($this->id) { $html .= " - n° {$this->id}<input type='hidden' name='__id' value='{$this->id}' />"; }
        $html.= "</legend>";
        $html.= " <label for='__nom'>Nom<em>*</em></label> <input type='text' id='__nom' name='__nom' value=\"{$this->libelle}\" required minlength='3' /><br/>";
        $html.= " <label for='__site_id'>Site(s)</label><select name='__site_id' id='__site_id'>";
        $list = new Site($this->objects);
        $html.= HTML_array2options($list->array_list(), $this->site_id, '__site_id');
        unset($list);
        $html.= "</select><br/>";
        $html .= "</fieldset>\n";

        $html .= " <input type='submit' value='Enregistrer' /> <a class='button' href='?{$this->objects['module']}/list'>Retour à la liste</a>\n";
        $html.= "</form>\n";
        return $html;
    }

    function HTML_view($actions = true) : string
    {
        $html  = "<fieldset><legend>Lecteur n°{$this->id}</legend>";
        $html .= " <p><label for='__nom'>Nom</label> {$this->libelle}</p>";
        $html .= " <p><label for='__site_id'>Site</label> {$this->site->libelle} ({$this->site_id})</p>";
        $html .= "</fieldset>\n";
        if ($actions) {
            $html .= "<p><a class='button' href='?lecteur/edit/{$this->id}' title='modifier'><i class='fas fa-edit'></i>Modifier</a>".
               ($this->is_used() ? '' : " <a class='button' href='?lecteur/delform/{$this->id}' title='supprimer'><i class='far fa-trash-alt'></i>Supprimer</a>");
            if ($this->objects['session']->is_allowed('lireBadges', $this->site_id === null ? 0 : $this->site_id)) {
                $html .= " <a class='button' href='?lecteur/who/{$this->id}'><i class='fa fa-users'></i>Qui a accès</a>";
            }
            $html .= "</p>";
        }
        return $html;
    }

    /*
     * appelé après l'envoi d'un formulaire
     * charge l'objet avec les valeurs des champs
     * retourne le résutat du contrôle sur les valeurs
     */
    function fromForm()
    {
      if (isset($_POST['__id'])) $this->id = $_POST['__id'];
      // libelle
      $this->libelle = filter_var($_POST['__nom'], FILTER_SANITIZE_STRING);
      // parent
      $this->site_id = filter_var($_POST['__site_id'], FILTER_SANITIZE_NUMBER_INT);
      $this->site->id = $this->site_id;
      $this->site->get();

      //$this->statut = ($_POST['__statut'] == 'visible' ? 'visible' : 'cache');
      return ! $this->error;
    }

    function is_allowed_lire()
    {
      return $this->objects['session']->is_allowed('lireBadges', 
              (is_int($this->site_id) ? $this->site_id : 0));
    }

    function is_allowed_edit()
    {
      return $this->objects['session']->is_allowed('editBadges', 
              (is_int($this->site_id) ? $this->site_id : 0));
    }

    /*
     * renvoie un tableau avec la liste des (lecteur_id)
     * si $site_id vaut null, renvoie tous les lecteurs
     */
    public function array_listInSite($site_id, bool $andChildren) : array
    {
        // On récupère les lecteurs du site
        $arrayLecteursSite = $arrayLecteursTous = array();
        $query = "SELECT lecteurId, site_id FROM ". $this->dbs['mariadb']->getTableName('lecteurS');
        $sites_id = array($site_id);
        if ($site_id !== null) {
            if ($andChildren) {
                $sites_id = $this->site->array_flatListOfChildren(false, $site_id);
                $sites_id[] = $site_id; // on ajoute le parent aux enfants
            }
        }
        $result = $this->dbs['mariadb']->query($query);
        if ($result === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible d'extraire une liste de lecteurs du site"). HTML_debug($query);
            _echo($this->message);
        } else {
            while ($record = $this->dbs['mariadb']->fetchObject()) {
                if (in_array($record->site_id, $sites_id)){
                    $arrayLecteursSite[$record->lecteurId] = $record->site_id;
                }
                $arrayLecteursTous[$record->lecteurId] = $record->site_id;
            }
            $this->dbs['mariadb']->freeResult();
        }

        // On prend la liste de tous les lecteurs
        $array = array();
        $query = "SELECT lecteurId FROM {$this->dbs['mssql']->getTableName('lecteur')}"
              . " ORDER BY libelle";
        $result = $this->dbs['mssql']->query($query);
        if ($result === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible d'extraire une liste de lecteurs"). HTML_debug($query);
        } else {
            while ($record = $this->dbs['mssql']->fetchObject()) {
                // le lecteur est-il dans le site recherché ?
                if (
                    // soit il n'est pas affecté, on le fait apparaître dans la racine
                    ($site_id===null) || 
                    // il est inconnu de Badge mais présent dans SenatorFX
                    ($site_id==0 && !isset($arrayLecteursTous[$record->lecteurId])) || 
                    // soit il est affecté et dans le bon site
                    (isset($arrayLecteursSite[$record->lecteurId]) && in_array($arrayLecteursSite[$record->lecteurId], $sites_id))
                ) {
                    $array[$record->lecteurId] = $record->lecteurId;
                }
            }
            $this->dbs['mssql']->freeResult();
        }
        
        unset($arrayLecteursSite);

        return $array;
    }

    /*
     * retourne un tableau pas beau TODO
     * (id, HTML_nom, level, HTML_action, HTML_attribut)
     */
    public function HTML_listInSite(int $site_id, int $level) : array
    {
        $array = array();
        $arraySite = $this->array_listInSite($site_id, false);
        $isSitesAutorise = $this->objects['session']->is_allowed('lireBadges', $site_id);
        $isDroitLecteur = $this->objects['session']->isLoggedOrHasRight('lecteur', false);
        foreach($arraySite as $lecteur_id) {
            //echo HTML_debug("record->id: ". $record->id);
            $this->id = $lecteur_id;
            $this->get();
            $array[] = array(
              $this->id,
                //I_LECTEUR. ($this->statut!='visible' ? '<span class="barre">' : ''). $this->libelle. ($this->statut!='visible' ? '</span>' : ''),
                I_LECTEUR. $this->libelle,
              $level+1,
              ($isDroitLecteur && $this->is_allowed_lire() ? " <a href='?lecteur/view/{$this->id}' title='voir'><i class='fas fa-eye'></i></a>" : "").
              ($isDroitLecteur && $this->is_allowed_edit() ? " <a href='?lecteur/edit/{$this->id}' title='modifier'><i class='fas fa-edit'></i></a>" : "").
              ($isSitesAutorise ? " <a href='?lecteur/who/{$this->id}' title='qui a accès'><i class='fa fa-users'></i></a>" : ""),
              'class="nodrop"'
            );
        }
        return $array;
    }

    public function JSON_array(array $arrayFrom) : string
    {
      $array = array();
      foreach($arrayFrom as $lecteur_id) {
        $this->id = $lecteur_id;
        $this->get();
        $site_id = $this->site_id;
        $array[] = array(
          'id'=>(int)$this->id,
          'libelle'=>$this->libelle,
          'site_id'=>$site_id,
          'statut' =>'visible'//$this->statut
        );
      }
      return json_encode($array);
    }

    /*
     * tableau des lecteurs par ordre hiérarchique et alphabétique
     * array (id, libelle, level, link, option)
     * TODO : le paramètre $id_site ne sert à rien
     */
    private function array_list(int $id_site=0) : array
    {
      //$site = new site($this->objects);
      $arraySite = array_merge(array(0=>array(0, "Racine", 0, '', 'class="nomove"')), 
              $this->site->HTML_listOfChildren(true));
      $arrayLecteurs = array();
      // on ajoute à chaque site ses lecteurs rattachés
      foreach($arraySite as $key=>$record) {
        $arrayLecteur = $record;
        $arrayLecteur[3] = ''; // on retire le lien html sur le site
        $arrayLecteur[4] = "class='nomove'"; // on ajoute que le site n'est pas triable
        $arrayLecteurs[] = $arrayLecteur;
        $arrayLecteurs = array_merge($arrayLecteurs, $this->HTML_listInSite($record[0], $record[2]));
        unset($arrayLecteur);
      }
      return $arrayLecteurs;
    }

    /*
     * tableau des lecteurs par ordre hiérarchique et alphabétique
     * array (id, nom, level, link)
     * en tenant compte des droits de l'utilisateur
     */
    function array_list_rights($typeDeDroit=null) : array
    {
      // on récupère les habilitations
      $listeSitesAutorises = $this->objects['session']->mesSitesAutorises($typeDeDroit);
      $site = new site($this->objects);
      // quels sont tous les parents des sites
      $listeParentsSitesAutorises = array();
      foreach ($listeSitesAutorises as $value) {
        $listeParentsSitesAutorises = array_merge($listeParentsSitesAutorises, $site->array_listOfParents($value[0]));
      }

      $arraySite = //$site->HTML_listOfChildren(true); 
          array_merge(array(0=>array(0, "Racine", 0, '', 'class="nomove"')), $site->HTML_listOfChildren(false));
      $arrayLecteurs = array();
      // on ajoute à chaque site ses lecteurs rattachés
      foreach($arraySite as $key=>$record) {
        if (in_array($record[0], $listeParentsSitesAutorises) || in_array($record[0], $listeSitesAutorises)) {
          $arrayLecteur = $record;
          $arrayLecteur[3] = ''; // on retire le lien html sur le site
          $arrayLecteur[4] = "class='nomove'"; // on ajoute que le site n'est pas triable
          $arrayLecteurs[] = $arrayLecteur;
          if (in_array($record[0], $listeSitesAutorises)) {
            $arrayLecteurs = array_merge($arrayLecteurs, $this->HTML_listInSite($record[0], $record[2]));
          }
          unset($arrayLecteur);
        }
      }

      unset($site);
      return $arrayLecteurs;
    }

    /*
     * retourne le code html de la liste des lecteurs avec
     * possibilité de faire du drag and drop
     */
    function HTML_interactiveList() : string
    {
        $html = ($this->objects['session']->isLoggedOrHasRight('lecteur', false) ?
            "<script type='text/javascript' src='js/lecteur.js'></script>" : "");
        $html .= HTML_array2list($this->array_list(true));
        return $html;
    }

    /*
     * retourne un tableau de (id, label, value) de lecteur
     * en fonction d'une chaine recherchée dans
     * name
     */
    function array_search($queryString) : array
    {
        $array = array();
        $sql = "SELECT lecteurId, libelle".
            " FROM {$this->dbs['mssql']->getTableName('lecteur')}".
            " WHERE libelle LIKE ?".
            " ORDER BY libelle".
            " OFFSET 0 ROWS FETCH NEXT 20 ROW ONLY";

        $result = $this->dbs['mssql']->query ($sql, ['%'.$queryString.'%']);
        if (! $result) {
            $this->message = "Problème avec la requête de recherche ". HTML_debug($sql);
            $this->error = true;
            echo $this->message;
            return false;
        }    
        
        while ($record = $this->dbs['mssql']->fetchObject()) {
          $array[] = array(
            'id'    => $record->lecteurId,
            'value' => $record->lecteurId. " ". $record->libelle,
            'label' => $record->lecteurId. " ". $record->libelle //. ($record->status!="visible" ? " ({$record->status})" : "")
          );
        }
        $this->dbs['mssql']->freeResult($result);
        return $array;
    }

    function HTML_lastlogs()
    {
        $log = new Log($this->objects);
        $log->filter['sur_objet'] = "lecteur";
        $log->filter['sur_id'] = $this->id;
        return $log->HTML_searchResult("Historique des logs");
    }

    // permet la suppression du lecteur.
    // Pas géré dans cette application, donc on force en indiquant que
    // c'est toujours utilisé
    function is_used() : bool
    {
        return true;
    }

    public function estDansSite($site_id) : bool
    {
        return $site_id == $this->site_id; //in_array($site_id, $this->sites_id);
    }
    
    // renvoie un tableau de tableaux (numero, horaires, badges, statuts)
    private function who() : array
    {
        // chercher les profils ayant le lecteur
        $profil = new Profil($this->objects);
        $aHoraireBadge = $profil->listeBadgesHoraires_Lecteur($this->id);
        
        $TexteNumero = $TexteHoraire = $TexteBadge = $TexteStatut = array();

        $horaire = new horaire($this->objects);
        $badge = new badge($this->objects);
        
        foreach($aHoraireBadge as $horaire_id=>$aBadges_id) {
            $horaire->id = $horaire_id;
            $horaire->get();
            if ($horaire->error) {
                $this->message .= $horaire->message;
                $this->error = true;
            }
            
            $aBadges_id = array_unique($aBadges_id);
                   
            foreach (array_unique($aBadges_id) as $badge_id) {
                $badge->numero = $badge_id;
                $badge->get();
                if ($badge->error) {
                    $this->message .= $badge->message;
                    $this->error = true;
                }
                $TexteNumero[] = "{$badge->numero}";
                $TexteHoraire[] = "{$horaire->nom}";
                $TexteBadge[] = "{$badge->nom} {$badge->prenom}";
                $TexteStatut[] = ($badge->statut == "En service" ? "" : " ". $badge->statut);
            }
        }

        return array($TexteNumero, $TexteHoraire, $TexteBadge, $TexteStatut);
    }
    
    // renvoie une liste de personnes triées par horaires
    // $order peut être horaire ou badge
    public function HTML_who($order = 'horaire', $displayNumero = true) : string
    {
        $html = "";
        
        $array = $this->who();

        array_multisort(
            $array[$order=='horaire' ? 1 : 2], SORT_ASC, SORT_STRING,
            $array[$order=='horaire' ? 2 : 1], SORT_ASC, SORT_STRING, 
            $array[0],
            $array[3]
        );

        $html .= "<ul>";
        $h0=""; $first = true;
        foreach($array[1] as $key=>$h) {
            if ($order=='horaire' && $h0 != $h) {
                if (! $first) {
                    $html .= "</ul></li>";
                }
                $html .= "<li><span style='font-weight: bold;'>{$h}</span><ul>";
            }
            // badge
            $b = ($array[2][$key] > "" ? $array[2][$key] : "pas nommmé").
                    ($displayNumero ?
                        " (<a href='?badge/view/{$array[0][$key]}' title='voir le badge no {$array[0][$key]}'>{$array[0][$key]}</a>)"
                        : "");
            // statut
            $s = $array[3][$key]!="" ? " <span style='color:grey;' class='unused'>{$array[3][$key]}</span>" : "";
            if ($order == 'horaire') {
                $html .= "<li>{$b}{$s}</li>";
            } else {
                $html .= "<li>{$b}{$s} : <i>{$h}</i></li>";
            }
            if ($h0 != $h) {
                $h0 = $h;
            }
            $first = false;
        }
        if ($order=='horaire' && !$first) {
            $html .= "</ul></li>";
        }
        $html .= "</ul>";

        return $html;
    }
       
    // renvoie une liste de personnes triées par horaires
    // $order peut être horaire ou badge
    public function text_who($order = 'horaire') : string
    {
        $txt = "";
        
        $array = $this->who();

        array_multisort(
            $array[$order=='horaire' ? 1 : 2], SORT_ASC, SORT_STRING,
            $array[$order=='horaire' ? 2 : 1], SORT_ASC, SORT_STRING, 
            $array[0],
            $array[3]
        );

        $txt .= "\n";
        $h0=""; $first = true;
        foreach($array[1] as $key=>$h) {
            if ($order=='horaire' && $h0 != $h) {
                if (! $first) {
                    $txt .= "\n";
                }
                $txt .= "* {$h}\n";
            }
            // badge
            $b = ($array[2][$key] > "" ? $array[2][$key] : "pas nommmé");
            // statut
            $s = $array[3][$key]!="" ? " ({$array[3][$key]})" : "";
            if ($order == 'horaire') {
                $txt .= "  {$b}{$s}\n";
            } else {
                $txt .= "  {$b}{$s} : {$h}\n";
            }
            if ($h0 != $h) {
                $h0 = $h;
            }
            $first = false;
        }
        $txt .= "\n";

        return $txt;
    }

    /*
     * Commande la mise à jour des centrales
     */
    private function commit() : bool
    {
        // commit avec le numéro badge
        $sql = "INSERT INTO ". $this->dbs['mssql']->getTableName('commit').
                " (zoneModifiee, numeroBadge, action)".
                " VALUES (4, 0, 0)";
        $ret = $this->dbs['mssql']->query($sql);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible de commande la mise à jour sur les centrales");
        }
        return ! $this->error;
    }

}
