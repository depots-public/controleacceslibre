<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2023 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace Badge\Model;
require_once('Badge.php');

/*
 * Programmation des droits des badges
 *
 */

class Prog {

    public
            $id, // le numéro sert d'identifiant unique dans la table
            $numeroBadge, // numéro du badge

            $depuis, $jusque, // au format datetime

            $profil_origine_nom, // nom profil initial de droits du badge , avant programmation
                                 // et après s'il y a une date "jusque"
            $profil_origine, // profil initial de droits du badge
            $profil_destination_nom, // nom profil programmé de droits du badge pendant la programmation
                                     // ou destination si pas de "jusque"
            $profil_destination, // profil programmé de droits du badge

            $statut = "aucun", // statut parmi avant, pendant, apres, annule

            $error = false,
            $message = "";
    private
            $badge,
            $dbs,
            $objects;

    public function __construct(array $objects) {
        $this->objects = $objects;
        $this->dbs = $objects['dbs'];
        $this->badge = new \Badge\Model\Badge($objects);
        $this->profil_origine = new \Badge\Model\Profil($objects);
        $this->profil_destination = new \Badge\Model\Profil($objects);
    }

    /*
     * charge l'objet des valeurs récupérées dans la base à partir de l'id
     * retourne true/false selon le succès
     */

    public function get(): bool {
        $query = "SELECT p.* " .
                "FROM {$this->dbs['mariadb']->getTableName('prog')} AS p " .
                //"LEFT JOIN {$this->dbs['mariadb']->getTableName('prog')} AS bs ON p.statutId=bs.statutBadgeId ".
                "WHERE id=?";
        $this->dbs['mariadb']->query($query, [$this->id]);
        if (!($record = $this->dbs['mariadb']->fetchObject())) {
            $this->message .= HTML_error("Aucune programmation avec l'id " . $this->id);
            $this->error = true;
            return false;
        }
        $this->numeroBadge = $record->numeroBadge;

        //$this->profil_origine = new Profil($this->objects);
        $this->profil_origine_nom = $record->profil_origine_nom;
        //$this->profil_destination = new Profil($this->objects);
        $this->profil_destination_nom = $record->profil_destination_nom;

        $this->statut = $record->statut;
        $this->depuis = $record->depuis != null ? date('Y-m-d\TH:i:s', strtotime($record->depuis)) : null;
        $this->jusque = $record->jusque != null ? date('Y-m-d\TH:i:s', strtotime($record->jusque)) : null;
        $this->datemaj = date('d/m/Y H:i:s', strtotime($record->datemaj));

        $this->dbs['mariadb']->freeResult();

        $this->message .= "";
        $this->error = false;
        return true;
    }

    
    /*
     * charge l'objet des valeurs récupérées dans la base à partir du numeroBadge
     * retourne true/false selon le succès
     */

    public function getBadge(): bool {
        $query = "SELECT id " .
                "FROM {$this->dbs['mariadb']->getTableName('prog')} AS p " .
                "WHERE numeroBadge=? AND statut IN ('avant', 'pendant')";
        $this->dbs['mariadb']->query($query, [$this->numeroBadge]);
        if (!($record = $this->dbs['mariadb']->fetchObject())) {
            $this->message .= HTML_error("Aucune programmation avec le badge " . $this->numeroBadge);
            $this->error = true;
            return false;
        }
        $this->id = $record->id;
        $this->dbs['mariadb']->freeResult();
        return $this->get();
    }
    
    /*
     * met à jour le statut et le profil du badge dans la base
     * retourne true/false selon succès
     */

    private function save(): bool {
        $sql = ($this->id > 0 ? "UPDATE " : "INSERT INTO ");
        $sql .= "{$this->dbs['mariadb']->getTableName('prog')} SET " .
                "numeroBadge=?," .
                "profil_origine_nom=?," .
                "profil_destination_nom=?," .
                "statut=?," .
                "depuis=?," .
                "jusque=?";
        $params = [
            $this->numeroBadge,
            $this->profil_origine_nom,
            $this->profil_destination_nom,
            $this->statut,
            $this->depuis,
            $this->jusque,
        ];
        if (!$this->id > 0) {
            $sql .= ", id=NULL";
        }
        if ($this->id > 0) {
            $sql .= " WHERE id=?";
            $params[] = $this->id;
        }
        $ret = $this->dbs['mariadb']->query($sql, $params);
        if ($ret === false) {
            $this->error = true;
            $this->message .= HTML_error("Impossible d'enregistrer la programmation. ");
        }
        if ($this->id == 0) {
            $this->id = $this->dbs['mariadb']->insertid();
        }

        // enregistrer dans les logs
        $log = (new log($this->objects))->add('save', 'prog', $this->id, $this);
        unset($log);    

        return !$this->error;
    }

    /*
     * Statut de programmation d'un badge
     * 
     * Entrée : $this->numeroBadge
     * 
     * Renvoie le statut de programmation d'un badge
     * "aucun" si aucune programmation n'est prévue ou en cours
     * sinon, "avant" ou "pendant"
     */
    public function statutBadge() : string
    {
        $this->getBadge();
        return $this->statut;
    }
    
    /*
     * Nouvelle affectation programmée à enregistrer
     * 
     * Entrées :
     * $this->numeroBadge
     *      ->depuis
     *      ->jusque
     *      ->profil_origine
     *      ->profil_destination
     */
    public function nouveau()
    {
        $prog0 = new Prog($this->objects);
        $prog0->numeroBadge = $this->numeroBadge;
        if ($prog0->statutBadge() != "aucun") {
            $this->message .= HTML_error("Ce badge est déjà programmé. ");
        }
        unset($prog0);
        if ($this->depuis === null && $this->jusque === null) {
            $this->message .= HTML_error("Aucune date de début ni de fin. ");
        }
        if ($this->depuis !== null && $this->depuis < date('Y-m-d\TH:i:s')) {
            $this->message .= HTML_error("La date de début {$this->depuis} est dans le passé. ");
        }
        if ($this->depuis !== null && $this->jusque !== null && $this->depuis > $this->jusque) {
            $this->message .= HTML_error("La date de fin est avant le début. ");
        }
        if ($this->jusque !== null && $this->jusque < date('Y-m-d\TH:i:s')) {
            $this->message .= HTML_error("La date de fin est dans le passé. ");
        }
        if ($this->message>"") {
            $this->error = true;
        } else {
            $this->statut = "avant";
            $this->profil_destination->creeNom();
            $this->save();
        }
        if (! $this->error) {
            $this->badge->numero = $this->numeroBadge;
            $this->badge->get();
            _echo(HTML_debug("Programmation du badge {$this->badge->prenom} {$this->badge->nom} ({$this->numeroBadge})"
                . " avec le profil {$this->profil_destination->nom} "
                . ($this->depuis !== null ?" à partir du ". $this->depuis : " maintenant ")
                . ($this->jusque !== null ?" avec le profil ". $this->profil_origine->nom. " jusque ". $this->jusque : " pour toujours")
            ));
            // enregistrer dans les logs
            $log = (new log($this->objects))->add('save', 'profil', $this->id, $this);
            unset($log);    
        }
        return ! $this->error;
    }

    // via cli
    public function check() : bool
    {
        $anciensProfils = array();
        // recherche des programmations qui sont arrivées à échéance
        $query = "SELECT id " .
                "FROM {$this->dbs['mariadb']->getTableName('prog')} AS p " .
                "WHERE statut IN ('avant', 'pendant')";
        $this->dbs['mariadb']->query($query, []);
        // stock les résultats
        $prog_ids = array();
        while ($record = $this->dbs['mariadb']->fetchObject()) {
            $prog_ids[] = $record->id;
        }
        // exécute les 50 prochaines affectations
        $n = 50;
        foreach ($prog_ids as $id) {
            if ($n < 0) {
                break;
            }
            $this->id = $id;
            $this->get();
            $this->badge->numero = $this->numeroBadge;
            $this->badge->get();
            // avant et (depuis==null ou depuis passé)
            if ($this->statut == "avant" && ($this->depuis === null || $this->depuis <= date('Y-m-d\TH:i:s'))) {
                $this->message .= HTML_message("{$this->badge->nom} avant atteint. ");
                $this->profil_destination->nom = $this->profil_destination_nom;
                // on enregistre le profil actuel
                $this->profil_origine->nom = $this->profil_origine_nom;
                $this->profil_origine->creeDepuisNom();
                $this->profil_origine->id = $this->profil_origine->exists();
                $anciensProfils[$this->profil_origine->id] = $this->profil_origine->id;
                // on affecte
                $this->doDestination();
                $n--;
            }
            // jusque atteint
            if ($this->statut == "pendant" && $this->jusque <= date('Y-m-d\TH:i:s')) {
                $this->message .= HTML_message("{$this->badge->nom} jusque atteint. ");
                $this->profil_origine->nom = $this->profil_origine_nom;
                // on enregistre le profil actuel
                $this->profil_destination->nom = $this->profil_destination_nom;
                $this->profil_destination->creeDepuisNom();
                $this->profil_destination->id = $this->profil_destination->exists();
                $anciensProfils[$this->profil_destination->id] = $this->profil_destination->id;
                // on affecte
                $this->doOrigine();
                $n--;
            }
        }
        $this->dbs['mariadb']->freeResult();
        
        // nettoyage des profils peut-être plus utilisés
        $this->badge->message = "";
        $this->badge->doCleanProfiles($anciensProfils);
        $this->message .= $this->badge->message;
        
        return ! $this->error;
    }
    
    /*
     * Exécute une programmation en statut avant
     */
    public function doDestination() : bool {
        $this->profil_destination->creeDepuisNom();
        if ($this->profil_destination->error) {
            $this->error &= $this->profil_destination->error;
            $this->message .= $this->profil_destination->message;
        } else {
            $this->badge->doChangeProfil($this->profil_destination);
            if ($this->badge->error) {
                $this->error &= $this->badge->error;
            }
            $this->message .= $this->badge->message;
        }
        if (! $this->error) {
            $log = (new Log($this->objects))->add('cancel', 'prog', $this->id, $this);
            unset($log);
            $this->statut = $this->jusque !== null ? 'pendant' : 'apres';
            $this->save();
        }
        return ! $this->error;
    }
    
    /*
     * Exécute une programmation en statut pendant
     */
    public function doOrigine() : bool {
        $this->profil_origine->creeDepuisNom();
        if ($this->profil_origine->error) {
            $this->error &= $this->profil_origine->error;
            $this->message .= $this->profil_origine->message;
        } else {
            $this->badge->doChangeProfil($this->profil_origine);
            if ($this->badge->error) {
                $this->error &= $this->badge->error;
            }
            $this->message .= $this->badge->message;
        }
        if (! $this->error) {
            $log = (new Log($this->objects))->add('cancel', 'prog', $this->id, $this);
            unset($log);
            $this->statut = 'apres';
            $this->save();
        }
        return ! $this->error;
    }
    
    /*
     * Annule une programmation en statut avant
     */
    public function cancel() : bool {
        if ($this->statut == "avant") {
            $this->statut = 'annule';
            $this->save();
            if (! $this->error) {
                $log = (new Log($this->objects))->add('cancel', 'prog', $this->id, $this);
                unset($log);
                $this->message .= HTML_message("Programmation du badge {$this->numeroBadge} annulée. ");
            }
        } else {
            $this->error = true;
            $this->message .= HTML_error("Impossible d'annuler la programmation qui a débuté ou est déjà annulée. ");
        }
        return ! $this->error;        
    }
    
    /*
     * Annule une programmation en statut avant
     */
    public function forestall() : bool {
        if ($this->statut == "pendant") {
            // on récupère les infos sur le badge
            $this->badge->numero = $this->numeroBadge;
            $this->badge->get();
            //$this->message .= HTML_message("{$this->badge->nom} jusque atteint. ");
            $this->profil_origine->nom = $this->profil_origine_nom;
            // on enregistre le profil actuel
            $this->profil_destination->nom = $this->profil_destination_nom;
            $this->profil_destination->creeDepuisNom();
            $this->profil_destination->id = $this->profil_destination->exists();
            // on affecte
            $this->doOrigine();

            if (! $this->error) {
                $log = (new Log($this->objects))->add('forestall', 'prog', $this->id, $this);
                unset($log);
                $this->message .= HTML_message("La fin de la programmation du badge {$this->badge->nom} {$this->badge->prenom} ({$this->numeroBadge}) a été avancée. ");

                // nettoyage des profils peut-être plus utilisés
                $this->badge->message = "";
                $this->badge->doCleanProfiles([$this->profil_destination->id]);
                $this->message .= $this->badge->message;
            }
        } else {
            $this->error = true;
            $this->message .= HTML_error("Impossible d'annuler la programmation qui n'a pas débuté, est terminée ou a été annulée. ");
        }
        return ! $this->error;        
    }    
    
} 

// end class
