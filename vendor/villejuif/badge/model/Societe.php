<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace Badge\Model;
/*
 * Gestion des sociétés
 *
 * C'est géré dans Senator, on ne fait que consulter
 */

class Societe {
    public
            $id = null,
            $libelle,
            $error = false,
            $message = "";
    
    private
            $dbs,
            $objects;
    
    public function __construct(array $objects)
    {
        $this->objects = $objects;
        $this->dbs = $objects['dbs'];
    }

    function get($record = null)
    {
        If($record == null){
            $query = "SELECT * ".
                "FROM {$this->dbs['mssql']->getTableName('societe')} ".
                "WHERE societeId=?";
            $this->dbs['mssql']->query ($query, [$this->id]);
            if (! ($record = $this->dbs['mssql']->fetchObject()))
            {
                $this->message = "Aucune societe avec l'id ". $this->id;
                $this->error = true;
                return false;
            }
            $this->dbs['mssql']->freeResult();
        }
        $this->libelle     = $record->libelle;

        $this->message = "";
        $this->error = false;
        return true;
    }


    /*
     * tableau des sites
     */
    function array_list($cache=false)
    {
        $array = array();
        $sql = "SELECT societeId, libelle".
                " FROM {$this->dbs['mssql']->getTableName('societe')}".
                " ORDER BY libelle";
        $this->dbs['mssql']->query ($sql);
        while ($record = $this->dbs['mssql']->fetchObject()) {
            $array[] = array(
                'id'    => $record->societeId,
                'value' => $record->libelle
            );
        }
        $this->dbs['mssql']->freeResult();
        return $array;
    }

}