<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/*
 * Gestion des habilitations par groupe d'utilisateurs LDAP
 */
namespace Badge\Model;
require_once('Site.php');

class Groupe{

    public 
            $id = null,
            $nom,       // nom du groupe qui est le nom du gropue AD
            $droits = array(),
            $statut, 
            $datemaj,
            $error   = false, 
            $message = "";
    private
            $dbs,
            $objects;

    public static
            $groupeDroitsVf = array(
                'session'=>"Accès à l'application",
                'horaire'=>"Modification des profils horaires",
                'site'=>"Modification des sites ou groupes de lecteurs",
                'lecteur'=>"Modification des lecteurs",
                'groupe'=>"Modification des habilitations",
                'log'=>"Accès aux logs (journal des actions) de l'application"
            ), // Liste des habilitations possibles pour un groupe LDAP - VF = vrai/faux
            $groupeDroitsSite = array(
                'lireBadges'=>"Lecture des droits des badges sur les lecteurs dans les sites...",
                'editBadges'=>"Modification des droits des badges pour les lecteurs dans les sites...",
                'journal'=>"Accès à l'historique des accès sur les lecteurs dans les sites..."
            ); // SITE = liste de sites sur lesquels porte l'habilitation

    public function __construct(array $objects)
    {
        $this->objects = $objects;
        $this->dbs = $objects['dbs'];
        // initialisation des habilitations
        foreach(self::$groupeDroitsVf as $droit=>$comment){ $this->droits[$droit] = false; }
        foreach(self::$groupeDroitsSite as $droit=>$comment){ $this->droits[$droit] = array(); }
    }

    /*
     * charge l'objet des valeurs récupérées dans la base à partir de l'id
     * retourne true/false selon le succès
     */
    function get($record = null) :bool
    {
        If($record == null){
            $query = "SELECT * ".
                "FROM {$this->dbs['mariadb']->getTableName('groupe')} ".
                "WHERE id=?";
            $result = $this->dbs['mariadb']->query ($query, [$this->id]);
            if (! ($record = $this->dbs['mariadb']->fetchObject()))
            {
                $this->message = "Aucun groupe avec l'id ". $this->id;
                $this->error = true;
                return false;
            }
            $this->dbs['mariadb']->freeResult($result);
        }
        $this->nom         = $record->nom;
        $this->statut      = $record->statut;
        $this->datemaj     = $record->datemaj;
        $this->droits      = json_decode($record->droits, true);

        $this->message = "";
        $this->error = false;
        return true;
    }

    /*
     * retourne la liste des noms de groupes
     */
    function getAllGroups() :array
    {
        $query = "SELECT id, nom ".
            "FROM {$this->dbs['mariadb']->getTableName('groupe')} ".
            "WHERE statut=?";
        $result = $this->dbs['mariadb']->query ($query, ["actif"]);
        if (! ($records = $this->dbs['mariadb']->fetchAllAsArray()))
        {
            $this->message = "Aucun groupe avec le statut 'actif'";
            $this->error = true;
            return [];
        }
        $this->dbs['mariadb']->freeResult($result);
        $this->message = "";
        $this->error = false;
        $return = [];
        foreach($records as $record)
        {
            $return[$record['id']] = $record['nom'];
        }
        return $return;
    }

    /*
     * met à jour le groupe et les habilitations associées dans la base
     * retourne true/false selon succès
     */
    function save()
    {
      $sql = ($this->id>0 ? "UPDATE " : "INSERT INTO ");
      $sql .= "{$this->dbs['mariadb']->getTableName('groupe')} SET " .
        "nom=? , ".
        "statut=? , ".
        "droits=?";
      $params = [$this->nom, $this->statut, json_encode($this->droits)];
      if ($this->id>0) {
        $sql .= " WHERE id=?";
        $params[] = $this->id;
      }
      $result = $this->dbs['mariadb']->query($sql, $params);
      if ($result === false) {
          $this->error=true;
          $this->message .= HTML_error("Impossible d'enregistrer le groupe d'habilitations"). HTML_debug($sql);
          var_dump($params);
      } else {
        // enregistrer dans les logs
        $log = (new log($this->objects))->add('save', 'groupe', $this->id, $this);
        unset($log);
      }
      if ($this->id==0) {
        $this->id = $this->dbs['mariadb']->insertid();
      }
      return ! $this->error;
    }

    /*
     * recherche si $this->$champ est unique dans la base en dehors de
     * $this->id même s'il est inactif
     */
    function isunique($champ='nom')
    {
      $query = "SELECT id FROM {$this->dbs['mariadb']->getTableName('groupe')} ".
          "WHERE {$champ}=:champ";
      $params = ['champ' => $this->$champ];
      if ($this->id != null) {
        $query .= " AND id<>:id";
        $params['id'] = $this->id;
      }
      $result = $this->dbs['mariadb']->query ($query, $params);
      $return =($this->dbs['mariadb']->numRows($result) == 0);
      $this->dbs['mariadb']->freeResult($result);
      return $return;
    }

    /*
     * appelé après l'envoi d'un formulaire
     * charge l'objet avec les valeurs des champs
     * retourne le résutat du contrôle sur les valeurs
     */
    function fromForm()
    {
      if (isset($_POST['__id'])) $this->id = $_POST['__id'];
      $this->nom = filter_var($_POST['__nom'], FILTER_SANITIZE_STRING);
      if (strlen($this->nom)<3) {
          $this->error = true;
          $this->message .= HTML_error("Le nom du groupe est absent ou trop court.");
      }
      // TODO : est-ce que le nom est unique ?
      if (! $this->isunique('nom')) {
          $this->error = true;
          $this->message .= HTML_error("Le nom du groupe existe déjà.");
      }
      $this->statut = ($_POST['__statut'] == 'actif' ? 'actif' : 'inactif');

      $base = '__droits'; // base du nom des champs

      // champs accès/interdit
      foreach (self::$groupeDroitsVf as $fin=>$comment) {
        if (!isset($_POST[$base.ucfirst($fin)])) {
          $this->error = true;
          $this->message .= HTML_error("La saisie pour {$fin} est obligatoire.");
          $this->droits[$fin] = null;
          continue;
        }
        $this->droits[$fin] = ($_POST[$base.ucfirst($fin)] == 'true');
      }

      // champs s'appliquant à des sites
      foreach (self::$groupeDroitsSite as $fin=>$comment) {
        $this->droits[$fin] = array();
        if (!isset($_POST[$base.ucfirst($fin)])) {
          continue;
        }
        foreach($_POST[$base.ucfirst($fin)] as $site_id) {
          $val = filter_var($site_id, FILTER_SANITIZE_NUMBER_INT);
          // mauvaise injection si pas integer
          if ($val==0 && $site_id!=='0') continue;
          // si existe déjà, on passe au suivant
          if (in_array($val, $this->droits[$fin])) continue;
          // TODO : vérifier que les sites existent
          $this->droits[$fin][] = $val;
        }
        sort($this->droits[$fin]);
      }
      return ! $this->error;
    }

    /*
     * partie du formulaire de choix des sites
     */
    private function HTML_choixSites($champ, $valeurs) : string
    {
      $html = "<ul id='{$champ}'>";
      $site = new site($this->objects);
      foreach($valeurs as $key=>$val) {
        $site->id = $val;
        $site->get();
        $html .= "<li id='{$champ}{$key}'>{$site->libelle}<input type='hidden' name='{$champ}[{$key}]' value='{$val}'>".
         ' <a href="#" onclick="javascript:_delete(\''. $champ. '\', '. $key. '); return false;">'.
         "<i class='far fa-minus-square'></i>&nbsp;retirer</a></li>";
      }
      $html .= "</ul>".
          "<select name='{$champ}_site_id' id='{$champ}_site_id'>". HTML_array2options($site->array_list(false), null, false).
          "</select><a id='{$champ}_add' href='#' onclick='javascript:add(\"{$champ}\", ".
          count($valeurs).
          "); return false;'><i class='far fa-plus-square'></i>&nbsp;ajouter</a>";
      unset($site);
      return $html;
    }

    /*
     * partie du formulaire pour choix vrai/faux
     */
    private function HTML_choixVF($champ, $val) : string
    {
      $html = "<label class='inline' for='{$champ}True'>accès</label><input id='{$champ}True' name='{$champ}' type='radio' value='true'".
        ($val==true ? ' checked' : ''). " required> ".
        "<label class='inline' for='{$champ}False'>interdit</label><input id='{$champ}False' name='{$champ}' type='radio' value='false'".
        ($val==false ? ' checked' : ''). ">";
      return $html;
    }

    /*
     * formulaire de modification du groupe et des habilitations
     */
    function HTML_form() : string
    {
      $html = "<form name='groupeForm' method='post' action='?groupe/fromForm/{$this->id}'>";
      $html .= "<fieldset><legend>Groupe LDAP";
      if ($this->id) $html .= " - n° {$this->id}";
      $html.= "</legend>";
      if ($this->id) $html .= "<input type='hidden' id='__id' name='__id' value='{$this->id}' />".
       "<input type='hidden' id='__nominit' name='__nominit' value='{$this->id}' />";
      // TODO : vérifier ou ne prendre que les noms des groupes existants dans LDAP
      $html.= "<label class='inline' for='__nom'>Nom LDAP du groupe<em>*</em> ".
       "<input type='text' id='__nom' name='__nom' value=\"{$this->nom}\" required='required' /></label><br/>";
      $html.= " <label class='inline' for='__statut'>Statut<em>*</em><select id='__statut' name='__statut'>";
      $html.= "   <option value='actif' ". ($this->statut!='inactif' ? " checked" : ""). " required>Actif</option>";
      $html.= "   <option value='inactif' ". ($this->statut=='inactif' ? " selected" : ""). ">Inactif</option>";
      $html.= " </select></label>";
      $html .= "</fieldset>\n";
      $html .= "<fieldset><legend>Autorisations</legend>";
      $html.= " <table class='form'>\n";

      $even = true;
      foreach(self::$groupeDroitsVf as $droit=>$comment) {
        $even = !$even;
        $html.= "  <tr class='". ($even ? "even" : "odd"). "'><th>". wordwrap($comment, 40, "<br />"). "<em>*</em></th>".
          "<td>". $this->HTML_choixVF('__droits'. ucfirst($droit), $this->droits[$droit]).
          "</td></tr>";
        break; // on ne montre que le première entrée : accès à l'application
      }
      foreach(self::$groupeDroitsSite as $droit=>$comment) {
        $even = !$even;
        $html.= "  <tr class='". ($even ? "even" : "odd"). "'><th>". wordwrap($comment, 40, "<br />"). "</th>".
          "<td>".
          $this->HTML_choixSites('__droits'. ucfirst($droit), $this->droits[$droit]);
          "</td></tr>";
      }
      foreach(self::$groupeDroitsVf as $droit=>$comment) {
        if ($droit=='session') continue; // on passe la première entrée
        $even = !$even;
        $html.= "  <tr class='". ($even ? "even" : "odd"). "'><th>". wordwrap($comment, 40, "<br />"). "<em>*</em></th>".
          "<td>". $this->HTML_choixVF('__droits'. ucfirst($droit), $this->droits[$droit]).
          "</td></tr>";
      }

      $html .= " </table>\n";
      $html .= "</fieldset>\n";
      $html .= " <input type='button' id='enregistrer' onclick='javascript:valid();' value='Enregistrer' ". (! $this->id ? "disabled='disabled' />" : "/>").
       " <input type='button' id='nouveau' onclick='javascript:validnew();' value='Enregistrer en tant que nouveau' />".
       " <a class='button' href='?{$this->objects['module']}/list'>Retour à la liste</a>\n";
      $html .= "</form>\n";
      $html .= "<script type='text/javascript' src='js/groupe.js'></script>";
      return $html;
    }

    private function HTML_viewVF($champ, $val) : string
    {
      return ($val === true ? ' autorisé' : 'interdit');
    }

    private function HTML_viewSites($champ, $valeurs) : string
    {
      $html = "";
      $site = new Site($this->objects);
      foreach($valeurs as $key=>$val) {
        $site->id = $val;
        $site->get();
        $html .= "<li id='{$champ}{$key}'>{$site->libelle}</li>";
      }
      unset($site);
      if ($html>"") {
        $html .= "<ul id='{$champ}'></ul>";
      } else {
        $html .= "Aucun";
      }
      return $html;
    }

    function HTML_view() : string
    {
      $html = "<fieldset><legend>Groupe LDAP - n° {$this->id}</legend>";
      $html .= "<label class='inline' for='__nom'>Nom du groupe LDAP :</label> ".
       "<strong>{$this->nom}</strong><br/>";
      $html.= " <label class='inline' for='__statut'>Statut :</label> <strong>{$this->statut}</strong>";
      $html .= "</fieldset>\n";

      $html .= "<fieldset><legend>Autorisations</legend>";
      $even = true;
      $html .= "<table>";
      foreach(self::$groupeDroitsVf as $droit=>$comment) {
        $even = !$even;
        $html.= "  <tr class='". ($even ? "even" : "odd"). "'><th>". wordwrap($comment, 40, "<br />"). "</th>".
          "<td>". $this->HTML_viewVF('__droits'. ucfirst($droit), $this->droits[$droit]).
          "</td></tr>";
        break; // on ne montre que le première entrée : accès à l'application
      }

      foreach(self::$groupeDroitsSite as $droit=>$comment) {
        $even = !$even;
        $html.= "  <tr class='". ($even ? "even" : "odd"). "'><th>". wordwrap($comment, 40, "<br />"). "</th>".
          "<td>".
          $this->HTML_viewSites('__droits'. ucfirst($droit), $this->droits[$droit]).
          "</td></tr>";
      }

      foreach(self::$groupeDroitsVf as $droit=>$comment) {
        if ($droit=='session') continue; // on passe la première entrée, qu'on a déjà affiché
        $even = !$even;
        $html.= "  <tr class='". ($even ? "even" : "odd"). "'><th>". wordwrap($comment, 40, "<br />"). "</th>".
          "<td>". $this->HTML_viewVF('__droits'. ucfirst($droit), $this->droits[$droit]).
          "</td></tr>";
      }

      $html .= " </table>\n";
      $html .= "</fieldset>\n";
      $html .= "<p><a href='?groupe/edit/{$this->id}' title='modifier'><i class='fas fa-edit'></i>Modifier</a>".
         ($this->is_used() ? '' : " <a href='?groupe/delform/{$this->id}' title='supprimer'><i class='far fa-trash-alt'></i>Supprimer</a>").
         "</p>";
      return $html;
    }

    function is_used() : bool
    {
      // TODO : regarder si le groupe LDAP existe
      // pour le moment, on renvoie false pour pouvoir le supprimer
      return false;
    }

    /*
     * cache = false pour ne pas retourner les groupes inactifs
     *
     * retourne un tableau compose de
     * (id, html_nom, niveau_hierarchique, liens, class)
     */
    function array_list($cache) : array
    {
      $array = array();
      $query = "SELECT * FROM {$this->dbs['mariadb']->getTableName('groupe')} ".
          (! $cache ? "WHERE statut='inactif'" : "").
          " ORDER BY nom";
      $result = $this->dbs['mariadb']->query ($query);
      while ($record = $this->dbs['mariadb']->fetchObject()) {
        $this->id = $record->id;
        $this->get($record);
        $span = "<a href='?groupe/view/{$this->id}'";
        if ($this->statut!='actif' || !$this->is_used()) {
          $span .= "class='".
           ($this->statut!='actif' ? 'barre' : '').
           ($this->statut!='actif' && ! $this->is_used() ? ' ' : '').
           ($this->is_used() ? '' : 'unused').
           "' title='".
           ($this->statut!='actif' ? 'désactivé' : '').
           ($this->statut!='actif' && ! $this->is_used() ? ' ' : '').
           ($this->is_used() ? '' : 'inutilisé').
           "'>";
        } else $span="";
        $array[] = array($this->id,
          I_GROUPE. $span. $this->nom. '</a>',
         1,
         " <a href='?groupe/edit/{$this->id}' title='modifier'><i class='gauche fas fa-edit'></i></a>".
         ($this->is_used() ? '' : " <a href='?groupe/delform/{$this->id}' title='supprimer'><i class='gauche far fa-trash-alt'></i></a>"),
         'class="nodrop"');
      } // end while $record
      $this->dbs['mariadb']->freeResult($result);
      return $array;
    }

    function HTML_list() : string
    {
        $html = HTML_array2list($this->array_list(true));
        return $html;
    }

    public function HTML_lastlogs()
    {
        $log = new log($this->objects);
        $log->filter['sur_objet'] = "groupe";
        $log->filter['sur_id'] = $this->id;
        return $log->HTML_searchResult("Historique des logs");
    }

}
