<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

namespace Badge\Model;
require_once('Lecteur.php');

/*
 * Gestion des sites et des groupes de lecteurs
 *
 * Cette table est en plus des Societe dans Senator.
 * Il n'y a pas de rapport avec les centrales ou la base Senator,
 * ces informations ne servent que pour l'application Badges.
 */

class Site {
    public
            $id = null,
            $libelle,
            $parent_id = null,
            $responsable = null,      # courriel du responsable du site
            $statut = "nouveau",      # nouveau (temporaire), visible, cache.
            $datemaj,
            $error = false,
            $message = "";
    
    /*public static   // TODO verser l'enregistrement racine dans la table
            $racineId = 0;
            $racineResponsable = "L-DAYOT@villejuif.fr"; // pour dev*/

    private
            $dbs,
            $objects;
    
    public function __construct(array $objects)
    {
        $this->objects = $objects;
        $this->dbs = $objects['dbs'];
    }

    function get($record = null)
    {
        /*if ($this->id == self::$racineId) {
            $this->libelle = "Racine";
            $this->parent_id = null;
            $this->responsable = null;
            $this->statut = "visible";
            return true;
        }*/
        If($record == null){
            $query = "SELECT * ".
                "FROM {$this->dbs['mariadb']->getTableName('site')} ".
                "WHERE id=?";
            $this->dbs['mariadb']->query ($query, [$this->id]);
            if (! ($record = $this->dbs['mariadb']->fetchObject()))
            {
                $this->message = "Aucun site avec l'id ". $this->id;
                $this->error = true;
                return false;
            }
            $this->dbs['mariadb']->freeResult();
        }
        $this->libelle     = $record->libelle;
        $this->parent_id   = $record->parent_id;
        $this->responsable = $record->responsable;
        $this->statut      = $record->status;
        $this->datemaj     = $record->updatedate;

        $this->message = "";
        $this->error = false;
        return true;
    }

  function save()
  {
    $params = [$this->libelle, 
        ($this->id > 0 ? $this->parent_id : NULL), 
        $this->responsable, $this->statut];
    if ($this->id !== null) {
        $sql  = "UPDATE ". $this->dbs['mariadb']->getTableName('site');
        $sql .= " SET libelle=?, parent_id=?, responsable=?, status=?";
        $sql .= " WHERE id=?";
        $params[] = $this->id;
    } else {
        $sql  = "INSERT INTO ". $this->dbs['mariadb']->getTableName('site');
        $sql .= " (libelle, parent_id, responsable, status)";
        $sql .= " VALUES (?, ?, ?, ?)";
    }
    $ret = $this->dbs['mariadb']->query($sql, $params);
    if ($ret === false) {
        $this->error=true;
        $this->message .= "Impossible d'enregistrer le site". HTML_debug($sql);
        var_dump($params);
    } else {
      // enregistrer dans les logs
      $log = (new log($this->objects))->add('save', 'site', $this->id, $this);
      unset($log);
    }
    if ($this->id==0) {
      $this->id = $this->dbs['mariadb']->insertid();
    }
    return ! $this->error;
  }

  /*
   * appelé après l'envoi d'un formulaire
   * charge l'objet avec les valeurs des champs
   * retourne le résutat du contrôle sur les valeurs
   */
  function HTML_form()
  {
    $html = "<form name='site' method='post' action='?site/fromForm/{$this->id}'>";
    $html .= "<fieldset><legend>Nom du site ou groupe de lecteurs";
    if ($this->id) {
        $html .= " - n° {$this->id}<input type='hidden' name='__id' value='{$this->id}' />";
    }
    $html.= "</legend>";
    $html.= " <label for='__nom'>Nom<em>*</em></label> ";
    if ($this->id === 0 || $this->id === "0") {
        $html .= "<input type='hidden' name='__nom' value='{$this->libelle}' />{$this->libelle}<br/>";
    } else {
        $html .= "<input type='text' id='__nom' name='__nom' value=\"{$this->libelle}\" required minlength='3' /><br/>";
    }

    $html.= " <label for='__parent_id'>Parent</label> ";
    if ($this->id === 0 || $this->id === "0") {
        $html .= "<input type='hidden' name='__parent_id' value='NULL' /><i>aucun, c'est la racine</i><br/>";
    } else {
        $html .= "<select name='__parent_id' id='__parent_id'>";
        $list = new site($this->objects);
        $html.= HTML_array2options($list->array_list($cache=false), $this->parent_id, true);
        unset($list);
        $html.= "</select><br/>";
    }

    $html.= " <label for='__responsable'>Courriel responsable</label> ".
            "<input type='mail' id='__responsable' name='__responsable' value=\"{$this->responsable}\" minlength='6' /><br/>";
            
    $html.= " <label for='__statut'>Statut<em>*</em></label> ";
    if ($this->id === 0 || $this->id === "0") {
        $html .= "<input type='hidden' name='__statut' value='visible'>visible<br/>";
    } else {
        $html .="<select id='__statut' name='__statut'>";
        $html.= "   <option value='visible' ". ($this->statut!='cache' ? " selected" : ""). ">Visible</option>";
        $html.= "   <option value='cache' ". ($this->statut=='cache' ? " selected" : ""). ">Caché</option>";
        $html.= " </select><br/>\n";
    }
    $html .= "</fieldset>\n";

    $html .= " <input type='submit' value='Enregistrer' /> ".
            "<a class='button' href='?{$this->objects['module']}/list'>Retour à la liste</a>\n";
    $html.= "</form>\n";
    return $html;
  }

  function HTML_view()
  {
    $html = "<fieldset><legend>Nom du site ou groupe de lecteurs".
            " - n° {$this->id}</legend>";
    $html.= " <label for='__nom'>Nom</label> {$this->libelle}<br/>";
    if ($this->id>0) {
        $parent = new site($this->objects);
        $parent->id = $this->parent_id;
        $parent->get();
        $html.= " <label for='__parent_id'>Parent</label> <a href='?site/view/{$parent->id}'>{$parent->libelle} ({$parent->id})</a><br/>";
        unset($parent);
    }
    /*$html.= " <label for='__statut'>Statut</label> ".
      ($this->statut!='cache' ? "Visible" : "Caché"). "<br/>\n";*/
    $html.= " <label for='__responsable'>Courriel responsable</label> ".
            $this->HTML_responsable(). "<br/>";
    
    $html .= "</fieldset>\n";
    $html .= "<p><a class='button' href='?site/edit/{$this->id}' title='modifier'>Modifier<i class='fas fa-edit'></i></a></p>";
    return $html;
  }

  function fromForm()
  {
    // nom
    if (isset($_POST['__id'])) $this->id = $_POST['__id'];
    $this->libelle = filter_var($_POST['__nom'], FILTER_SANITIZE_STRING);
    // parent
    $this->parent_id = filter_var($_POST['__parent_id'], FILTER_SANITIZE_NUMBER_INT);
    //if ($this->parent_id == null) $this->parent_id=self::$racineId;
    // test référence circulaire
    if ($this->parent_id==$this->id)
    {
      $this->message = "Référence circulaire, le parent choisi est le site lui-même.";
      $this->error = true;
    }
    $parent = new site($this->objects);
    if ($parent->is_enfantde($this->parent_id, $this->id))
    {
      $this->message = "Référence circulaire, le parent choisi est descendant du site.";
      $this->error = true;
    }
    unset($parent);

    // responsable
    if (isset($_POST['__responsable']) && $_POST['__responsable']>"") {
        $this->responsable = filter_var($_POST['__responsable'], FILTER_SANITIZE_EMAIL);
        if (! filter_var($this->responsable, FILTER_VALIDATE_EMAIL))
        {
            $this->message = "L'adresse de courriel du/de la responsable n'est pas correcte.";
            $this->error = true;
        }
    }

    $this->statut = ($_POST['__statut'] == 'visible' ? 'visible' : 'cache');
    return ! $this->error;
  }

  function is_enfantde($enfant_id, $parent_id)
  {
    $this->id = $enfant_id;
    $this->get();
    if ($this->parent_id != null) {
      if ($this->parent_id == $parent_id) return true;
      return $this->is_enfantde($this->parent_id, $parent_id);
    }
    return false;
  }

  function JSON_array($arrayFrom)
  {
    $array = array();
    foreach($arrayFrom as $site) { //(id, HTML_nom, niveau hiérarchique, HTML_action, HTML_attribut)
      $this->id = $site[0];
      $this->get();
      $array[] = array(
        'id'=>(int)$this->id,
        'nom'=>$this->libelle,
        'parent_id'=>($this->id == 0 ? null : ($this->parent_id==null ? 0 : (int)$this->parent_id)),
        'level'=>(int)$site[2]
      );
    }
    return json_encode($array);
  }

  /*
   * renvoie un tableau avec la liste des (site_id, level)
   */
  public function array_listOfChildren(bool $cache, $parent_id=0, $level=0) : array
  {
    $array = array();
    // a cause de la récursivité, obligé de cloner l'objet, sinon, PDO mélange tout
    $db = clone $this->dbs['mariadb'];
    $query = "SELECT id FROM {$db->getTableName('site')} ".
        " WHERE ". ($parent_id==0 ? "(ISNULL(parent_id) OR parent_id=:parent_id)" : "parent_id=:parent_id").
        " AND id<>:parent_id".
        (! $cache ? " AND status='visible'" : "").
        " ORDER BY libelle";
    $result = $db->query($query, ["parent_id" => $parent_id]);
    if (!$result) {
        $this->error = true;
        $this->message .= "Impossible de trouver les sites enfants de {$parent_id}". HTML_debug($query);
        _echo(HTML_error($this->message));
    }
    while ($record = $db->fetchObject()) {
      $array[] = array($record->id, $level);
      $array = array_merge($array, $this->array_listOfChildren($cache, $record->id, $level+1));
    }
    $db->freeResult($result);
    return $array;
  }

    /*
     * renvoie un tableau avec la liste des sites_id
     */
    public function array_flatListOfChildren(bool $cache, int $parent_id) : array
    {
        $array1 = array();
        foreach($this->array_listOfChildren($cache, $parent_id) as list($site_id, $level)) {
            $array1[$site_id] = $site_id;
        }
        return $array1;
    }
  
  /*
   * Renvoie un tableau avec la liste des parents d'un site
   */
  function array_listOfParents($site_id)
  {
    if ($site_id == 0 || $site_id == NULL) return array();
    $this->id = $site_id;
    $this->get();
    $parent = new site($this->objects);
    $r = $parent->array_listOfParents($this->parent_id);
    $r[] = (int)$this->parent_id;
    return $r;
  }

  /*
   * renvoie un tableau
   * (id, HTML_nom, niveau hiérarchique, HTML_action, HTML_attribut)
   */
  function HTML_listOfChildren($cache, $parent_id=0)
  {
    $array = array();
    $sites = $this->array_listOfChildren($cache, $parent_id);
    foreach($sites as list($site_id, $level)) {
      $this->id = $site_id;
      $this->get();
      $array[] = array(
        $this->id,
        I_SITE. ($this->statut!='visible' ? '<span class="barre">' : ''). $this->libelle. ($this->statut!='visible' ? '</span>' : ''),
       $level+1,
       " <a href='?site/view/{$this->id}' title='voir'><i class='fas fa-eye'></i></a>".
       " <a href='?site/edit/{$this->id}' title='modifier'><i class='fas fa-edit'></i></a>",
       ''
      );
    }
    return $array;
  }

    /*
     * tableau des sites par ordre hiérarchique et alphabétique
     */
    function array_list($cache=false)
    {
        $this->id = 0;
        $this->get();
        return array_merge(array(0=>array(0, $this->libelle, 0,
                " <a href='?site/view/{$this->id}' title='voir'><i class='fas fa-eye'></i></a>".
                " <a href='?site/edit/{$this->id}' title='modifier'><i class='fas fa-edit'></i></a>",
                'class="nomove"')),
                $this->HTML_listOfChildren($cache));
    }

    function HTML_interactiveList()
    {
        $html = "<script type='text/javascript' src='js/site.js'></script>";
        $html .= HTML_array2list($this->array_list(true));
        return $html;
    }

  /*
   * retourne un tableau de (id, label, value) de site
   * en fonction d'une chaine recherchée dans
   * nom
   */
  function array_search($queryString)
  {
    $array = array();
    $sql = "SELECT id, libelle, status".
        " FROM {$this->dbs['mariadb']->getTableName('site')}".
        " WHERE libelle LIKE ?".
        " ORDER BY libelle LIMIT 0, 20";

    $this->dbs['mariadb']->query ($sql, ['%'.$queryString.'%']);
    while ($record = $this->dbs['mariadb']->fetchObject()) {
      $array[] = array(
        'id'    => $record->id,
        'value' => $record->id. " ". $record->libelle,
        'label' => $record->id. " ". $record->libelle. 
            ($record->status!="visible" ? " ({$record->status})" : "")
      );
    }
    $this->dbs['mariadb']->freeResult();
    return $array;
  }

  function HTML_lastlogs()
  {
    if (! $this->objects['session']->droits['log']) {
      $this->message = "Vous n'avez pas l'habilitation suffisante.";
      return false;
    }
    $log = new log($this->objects);
    $log->filter['sur_objet'] = "site";
    $log->filter['sur_id'] = $this->id;
    return $log->HTML_searchResult("Historique des logs");
  }

  /*
   * recherche du courriel du responsable du site
   * ou celui du site supérieur
   */
  private function HTML_responsable() : string
  {
      $site_id = $this->id;
      $siteTemp = new Site($this->objects);
      $siteTemp->id = $this->id;
      $responsable = $siteTemp->_responsable();
      return $responsable == "" ? 
              "aucun" : 
              "<a href='mailto:{$responsable}'>{$responsable}</a>".
                      ($this->id != $siteTemp->id ? " (hérité de <a href='?site/view/{$siteTemp->id}'>{$siteTemp->libelle}</a>)" : "");
  }
  
  /*
   * retourne le courriel du responsable du site
   */
  public function responsable() : string
  {
      $site_id = $this->id;
      $siteTemp = new Site($this->objects);
      $siteTemp->id = $this->id;
      return $responsable = $siteTemp->_responsable();
  }

  /*
   * retour un tableau des responsables du site et des ses paerents
   */
  public function responsables() : array
  {
      return array_unique($this->_responsables());
  }
  
  private function _responsable() : string
  {
      $this->get();
      if ($this->responsable > "") return $this->responsable;
      if ($this->id == 0) return "";
      $this->id = $this->parent_id;
      return $this->_responsable();
  }  
  
    private function _responsables() : array
    {
        $this->get();
        $array = array();

        if ($this->responsable > "") {
            $array[] =$this->responsable;
        }
        if ($this->id == 0) return $array;
            
        $this->id = $this->parent_id;
        return array_merge($array, $this->_responsables());
    }  
  
}