-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : mer. 24 juin 2020 à 12:18
-- Version du serveur :  10.3.22-MariaDB-0+deb10u1
-- Version de PHP : 7.3.14-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `badges_test`
--


-- --------------------------------------------------------

--
-- Structure de la table `b_groupe`
--

CREATE TABLE `b_groupe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `droits` text DEFAULT NULL,
  `statut` varchar(20) NOT NULL DEFAULT 'visible',
  `datemaj` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `b_groupe`
--

INSERT INTO `b_groupe` (`id`, `nom`, `droits`, `statut`, `datemaj`) VALUES
(1, 'APP_BADGES_TOTAL', '{\"session\":true,\"horaire\":true,\"site\":true,\"lecteur\":true,\"groupe\":true,\"log\":true,\"lireBadges\":[\"0\"],\"editBadges\":[\"0\"],\"journal\":[\"0\"]}', 'actif', '2020-06-24 08:07:50'),
(2, 'APP_BADGES_VISU', '{\"session\":true,\"horaire\":false,\"site\":false,\"lecteur\":false,\"groupe\":false,\"log\":false,\"lireBadges\":[\"0\"],\"editBadges\":[],\"journal\":[]}', 'actif', '2020-06-24 08:08:44'),
(3, 'Defaut', '{\"session\":false,\"horaire\":false,\"site\":false,\"lecteur\":false,\"groupe\":false,\"log\":false,\"lireBadges\":[],\"editBadges\":[],\"journal\":[]}', 'actif', '2020-05-09 16:36:12');


-- --------------------------------------------------------

--
-- Structure de la table `b_log`
--

CREATE TABLE `b_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `par` varchar(64) NOT NULL,
  `action` varchar(32) NOT NULL,
  `sur_objet` varchar(32) NOT NULL,
  `sur_id` varchar(32) DEFAULT NULL,
  `sur_valeur` text NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;


-- --------------------------------------------------------

--
-- Structure de la table `b_lecteur`
--

CREATE TABLE `b_lecteur` (
  `lecteurId` int(11) NOT NULL,
  `site_id` int(11) DEFAULT 0,
  `updatedate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  KEY `lecteurId` (`lecteurId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


--
-- Index pour les tables déchargées
--

--
-- Index pour la table `b_groupe`
--
ALTER TABLE `b_groupe`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`nom`),
  ADD KEY `status` (`statut`);


--
-- Index pour la table `b_log`
--
ALTER TABLE `b_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `datetime` (`datetime`),
  ADD KEY `sur_objet` (`sur_objet`,`sur_id`),
  ADD KEY `par` (`par`);


-- --------------------------------------------------------

--
-- Structure de la table `b_site`
--

CREATE TABLE `b_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `responsable` varchar(127) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'valide',
  `updatedate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  UNIQUE KEY `id` (`id`),
  KEY `name` (`libelle`),
  KEY `parent_id` (`parent_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `b_site` (`id`, `libelle`, `status`) VALUES
  (0, 'Racine', 'valide');


-- --------------------------------------------------------

--
-- Structure de la table `b_etat`
--

CREATE TABLE `b_etat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date,
  `lecteur_id` int(11),
  `html_droits` text DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `date` (`date`),
  KEY `lecteur_id` (`lecteur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Structure de la table `b_prog`
--

CREATE TABLE `b_prog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numeroBadge` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profil_origine_nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profil_destination_nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `depuis` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `jusque` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `statut` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datemaj` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `numeroBadge` (`numeroBadge`),
  KEY `depuis_jusque` (`depuis`,`jusque`),
  KEY `statut` (`statut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- --------------------------------------------------------
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
