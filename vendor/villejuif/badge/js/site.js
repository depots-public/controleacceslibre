/*
 * GestionDeBadges
 * 
 * Copyright 2017-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net>
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/*
 * Sortable list
 */

function from3(str) {
  return str.substring(3);
}
$(function() {
    $( ".sortable" ).sortable({
      placeholder: "ui-state-highlight",
      start: function(event, ui) {
          if (ui.item.attr("class")=="nomove") {
            ui.item.cancel = true;
          } else {
            ui.item.cancel = false;
          }
          ui.item.startPos = from3(ui.item[0].parentElement.parentElement.id);
      },
      stop: function(event, ui) {
          if (ui.item.cancel == true) {
            $(this).sortable("cancel");
          }
          id = from3(ui.item[0].id);
          console.log("Id: " + id);
          oldparentid = ui.item.startPos;
          newparentid = from3(ui.item[0].parentElement.parentElement.id);
          console.log("Nouveau parent: " + newparentid);
          $.ajax({
              type:'GET',
              url:'index.php',
              data:'site/move/'+id+'/'+newparentid,
              dataType:"json",
              async:true,
              cache:true,
              success:function(e){
                  console.log(e.message);
              }
          });
      }
    });
    $(".sortable").sortable({
        cancel: "li.nosort",         // pas triable
        items: "li:not(.nosort)",      // ni triable, ni cible
        connectWith: ".sortable"
    });
    $(".sortable li").disableSelection();
} );
