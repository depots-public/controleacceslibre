/*
 * GestionDeBadges
 * 
 * Copyright 2017-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net>
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/*
 * Log display
 */

// par ailleurs, un tableau logList est chargé contenant les id
// des résultats affichés dans le cadre Resultats.

function detail(id) {
  getDetails(id);
  $('#log_details').attr('style','display:block;');
}

/*
 * initialise la partie affectation avec le résultat
 * des droits de la séléction
 */
function getDetails(id) {
  $.ajax({
      type:'GET',
      url:'index.php',
      data:'log/apiGet/'+id,
      dataType:"html",
      async:true,
      cache:true,
      success:function(e){
        $('#log_details').empty();
        $('#log_details').append(e);
      }
  });
  return false;
}
