/*
 * GestionDeBadges
 * 
 * Copyright 2017-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net>
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/*
 * Formulaire profil horaire
 */

function vider(j) {
  s = "__p";
  for (var h=0;h<ntranches;h++) {
    document.getElementById(s.concat("de", j, h)).value = '';
    document.getElementById(s.concat("a",  j, h)).value = '';
  }
  return false;
}

function recopier() {
  s = "__p";
  for (var j=2; j<6; j++) {
    for (var h=0;h<ntranches;h++) {
      document.getElementById(s.concat("de", j, h)).value = document.getElementById(s.concat("de", 1, h)).value;
      document.getElementById(s.concat("a",  j, h)).value = document.getElementById(s.concat("a",  1, h)).value;
    }
  }
  return false;
}

function valid() {
  s = "__p";
  if (document.getElementById(s.concat("nom")).value == '') {
    alert("Le nom de la tranche horaire est obligatoire");
    return false;
  }
  for (var j=1; j<=njours; j++) {
    for (var h=0;h<ntranches;h++) {
      if ((document.getElementById(s.concat("de", j, h)).value=='' &&
           document.getElementById(s.concat("a",  j, h)).value>'') ||
          (document.getElementById(s.concat("de", j, h)).value>'' &&
           document.getElementById(s.concat("a",  j, h)).value=='') ) {
        alert('Tranche horaire incomplète le jour ' + (j+1));
        return false;
      }
    }
  }
  document.profilHoraires.submit();
}

function validnew() {
  s = "__p";
  if (document.getElementById(s.concat("nom")).value == document.getElementById(s.concat("nominit")).value) {
    alert("Le nom doit être différent de l'original");
    return false;
  }
  document.getElementById(s.concat("id")).value = '';
  document.getElementById("enregistrer").disabled = true;
  return valid();
}

function divDisplay(a, id) {
  a.style.background = 'black';
  with(document.getElementById('h'+id)) {
    style.visibility = 'visible';
    style.top = a.offsetTop+'px';
    style.left = (window.event.clientX+100) +'px';
    style.height = '30em;';
  }
}

function divHide(a, id) {
  a.style.background = 'white';
  document.getElementById('h'+id).style.visibility = 'hidden';
}

// changement de type d'horaire dans le formulaire Horaire::HTML_form()
// les numéros des "jours" sont indiqués en début de classe Horaire
/*var jourType = [
  [1,2,3,4,5,6,7,8],    // 0 = 'détaillé'
  [9,8],                // 1 = 'quotidien / tous les jours'
  [9,10,8]              // 2 = 'semaine / week-end'
];*/
function changeType() {
    var t = document.getElementById("__ptype").value;
    var s = "jour";
    for(var njour=1; njour<=njours; njour++) {
        with(document.getElementById(s.concat(njour))) {
            if(jourType[t].indexOf(njour) >= 0) {
                style.display = 'table-row';
            } else {
                style.display = 'none';
            }
        }
    }
}