/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> agenda-du-libre-php
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/*
 * Badge select form
 */

/* Inspired from Benjamin Marquant 2016
 * https://www.bxnxg.com/minituto-01-jquery-autocomplete-ajax-cache-astuce-listing-suggestion-javascript-autocompletion/
 */
var autocomplete = false;
var autocompleteValeurChoisie = false;

function selectAutocomplete() {
  var champ = document.getElementById('_champ').value;
  if (champ === '') {
    if (autocomplete !== false) {
      disableAutocomplete();
      autocomplete = false;
    }
    return false;
  }
  var operateur = document.getElementById('_operateur').value;

  // cas où la valeur est à rechercher dans une liste
  if (operateur==='égal' | operateur==='différent') {
    if (autocomplete !== champ) {
      if (champ !== 'statut') {
        enableAutocomplete(champ);
        //console.log(champ);
      } else {
        enableAutocompleteStatut();
      }
      autocomplete = champ;
    }
  } else {
    if (autocomplete !== false) {
      disableAutocomplete();
      autocomplete = false;
    }
  }
}

function selectChamp(o) {
  with (document.getElementById('_champ')) {
      if (['statut', 'site'].indexOf(value)!==-1) {
          selectOperateurActive(['', 'égal', 'différent']);
      } else {
          selectOperateurActive([]);
      }
  }
  selectAutocomplete();
  selectOk();
}

function selectOperateurActive(operateursActifs) {
    for(var item of document.getElementById('_operateur').options) {
        if ((operateursActifs.length!==0) & (operateursActifs.indexOf(item.value)===-1)){
            item.disabled='disabled';
        } else {
            item.disabled='';
        }
    };
}

function selectOperateur(o) {
  //console.log(o.value);
  with(document.getElementById('_valeur')) {
    // cas où il n'y a pas besoin de valeur
    if (o.value=='est vide' | o.value=='n‘est pas vide') {
      value = o.value;
      disabled=true;
    } else {
      if (value=='est vide' |
        value=='n‘est pas vide') {
        value = "";
      }
      disabled=false;
    }
  } // end with
  selectAutocomplete();
  selectOk();
}

function selectValeur(o) {
  selectOk();
  if(window.event.which == 13) {
    window.event.preventDefault();
    window.event.stopPropagation();
    selectAdd();
    return false;
  }
    $('#_copy input').attr('disabled', '');
    /*$('#_valeur').attr('disabled', '');*/
    $('#_add').removeAttr('disabled');
  return true;
}

function selectOk() {
  if (document.getElementById('_champ').value>'' &
    document.getElementById('_operateur').value>'' &
    (document.getElementById('_valeur').disabled
      | document.getElementById('_valeur').value>'') &
      ((! autocomplete) | autocompleteValeurChoisie)
    ) {
     document.getElementById('_add').style.visibility='visible';
   } else {
     document.getElementById('_add').style.visibility='hidden';
   }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/*
 * Sort a JSON object
 * https://stackoverflow.com/questions/17684921/sort-json-object-in-javascript
 */
function isObject(v) {
    return '[object Object]' === Object.prototype.toString.call(v);
};
JSON.sort = function(o) {
if (Array.isArray(o)) {
        return o.sort().map(JSON.sort);
    } else if (isObject(o)) {
        return Object
            .keys(o)
        .sort()
            .reduce(function(a, k) {
                a[k] = JSON.sort(o[k]);

                return a;
            }, {});
    }

    return o;
}
// fin Sort a JSON object

/*
 * valeur par défaut :
 *   { badge: { 'égal': [ 'En service' ] } };
 * cf class.badge.inc.php en ligne 318
 */

function select2text() {
  var q = JSON.parse(document.getElementById('_q').value);
  JSON.sort(q);
  $('#_qtext').empty();
  var html = "";

  // boucle sur le champ
  for (var champ in q) {
    html = "<div id='"+champ+"' class='condition condition"+
      capitalizeFirstLetter(champ)+ "'>"+ champ;

    var premOperateur = true;
    // boucle sur l'opérateur
    for(var operateur in q[champ]) {
      if (! premOperateur) {
        html += " et ";
      }
      html += listeOperateurs[operateur].court;
      var premValeur = true;
      // boucle sur les valeurs
      for(var valeurKey in q[champ][operateur]) {
        var valeur = q[champ][operateur][valeurKey];
        if (! premValeur) {
          if (operateur=='différent' | operateur=='ne contient pas') {
            html += " et ";
          } else {
            html += " ou ";
          }
        }
        if (operateur!='est vide' & operateur!='n‘est pas vide') {
          html += valeur;
        }
        html += " <a href='#' onClick=\"javascript:selectDelete('"+ champ+ "', '"+ operateur+ "', '"+ valeur.replace(/'/g, '&quot;')+ "'"+
          "); return false;\" title='retirer'><i class=\'far fa-minus-square\'></i>"+ "</a>";
        premValeur = false;
      } // fin boucle valeurs
      premOperateur = false;
    } // fin boucle operateur
    html += "</div>";
    if (html > ""){
      if ($('#_qtext')[0].childElementCount > 0) {
        $('#_qtext').append(" et ");
      }
      $('#_qtext').append(html);
    }
    champPrec = champ;
  } // fin boucle champ
}

function selectAdd() {
  champ = document.getElementById('_champ').value;
  operateur = document.getElementById('_operateur').value
  valeur = document.getElementById('_valeur').value;
  if (valeur=='') {
    selectOk();
    return false;
  }
  var q = JSON.parse(document.getElementById('_q').value);
  if (champ in q) {
    if (operateur in q[champ]) {
      //console.log(q[champ][key]);
      for (valeurKey in q[champ][operateur]) {
        if (q[champ][operateur][valeurKey] == valeur) {
          //console.log('doublon');
          return false;
        }
      }
      // nouvelle valeur
      q[champ][operateur].push(valeur);
    } else { // nouvel operateur
      q[champ][operateur] = [ ];
      q[champ][operateur].push(valeur);
    }
  } else { // nouveau champ
    q[champ] = { } ;
    q[champ][operateur] = [ ];
    q[champ][operateur].push(valeur);
  }
  JSON.sort(q);
  document.getElementById('_q').value = JSON.stringify(q);
  select2text();
  return false;
}

function selectDelete(champ, operateur, valeur) {
  var q = JSON.parse(document.getElementById('_q').value);
  q[champ][operateur].splice(q[champ][operateur].indexOf(valeur), 1);
  if (q[champ][operateur].length==0) {
    delete q[champ][operateur];
  }
  if (Object.getOwnPropertyNames(q[champ]).length == 0) {
    delete q[champ];
  }
  document.getElementById('_q').value = JSON.stringify(q);
  select2text();
  return false;
}


/*
 * Recherche pour autocompletion
 */

function disableAutocomplete() {
  $('#_valeur').autocomplete("option", "disabled", true);
}

/* initialisation paramètres globaux : */
var cache = {}; /* tableau cache de tous les termes */
var cacheAll = {};
var term = null; /* terme renseigné dans le champ input */

function enableAutocomplete(base, field='#_valeur') {
    /* field autocomplete */
    $(field).autocomplete({
        disabled:false,
        minLength:2, /* nombre de caractères minimaux pour lancer une recherche */
        delay:200, /* delais après la dernière touche appuyée avant de lancer une recherche */
        scrollHeight:320,
        appendTo:'#_valeur-container', /* div ou afficher la liste des résultats, si null, ce sera une div en position fixe avant la fin de </body> */

        /* dès qu'une recherche se lance, source est executé, il peut contenir soit un tableau JSON de termes, soit une fonctions qui retournera un résultat */
        source:function(e,t) {
            term = e.term; /* récupération du terme renseigné dans l'input */
            term = term.toLowerCase();
            autocompleteValeurChoisie = false;
            if(term in cache) { /* on vérifie que la clé "term" existe dans le tableau "cache", si oui alors on affiche le résultat */
                t(cache[term]);
            } else { /* sinon on fait une requête ajax vers index.php pour rechercher "term" */
                $('#_loading').attr('style','');
                $.ajax({
                    type:'GET',
                    url:'index.php',
                    data:base+'/search/&queryString='+term,
                    dataType:"json",
                    async:true,
                    cache:true,
                    success:function(e){
                        cache[term] = e; /* vide ou non, on stocke la liste des résultats avec en clé "term" */
                        if(!e.length){ /* si aucun résultats, on renvoi un tableau vide pour informer qu'on a rien trouvé */
                            var result = [{
                                label: 'Aucun '+base+' trouvé(e)...',
                                value: null,
                                id: null,
                            }];
                            t(result); /* envoi du résultat à source */
                        }else{ /* sinon on renvoie toute la liste */
                            t($.map(e, function (item){
                                return{
                                    label: item.label,
                                    value: item.value,
                                    id: item.id,
                                }
                            }));  /* envoi du résultat à source avec map() de jQuery (permet d'appliquer une fonction pour tous les éléments d'un tableau */
                        }
                        $('#_loading').attr('style','display:none;');
                    }
                });
            }
        },

        /* sélection depuis la liste des résultats (flèches ou clic) > ajout du résultat automatique et callback */
        select: function(event, ui) {
            $('form input[name="_vid"]').val(ui.item ? ui.item.id : ''); /* on récupère juste l'id qu'on stocke dans l'autre input */
            /*document.getElementById('#_vid').value = (ui.item ? ui.item.id : '');*/
            $('#_loading').attr('style','display:none;');
            autocompleteValeurChoisie = true;
            selectOk();
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        },
    });
}

function enableAutocompleteStatut() {
    /* field autocomplete */
    $('#_valeur').autocomplete({
      disabled:false,
      minLength:1,
      delay:100,
      source: [ 'En service', 'Suspend', 'Volé' ]
    });
}

function selectSubmit(page=1) {
  var query = btoa(unescape(encodeURIComponent(document.getElementById('_q').value)));
  document.getElementById('_resultQuery').value = query;
  query += '&page='+page
  if(query in cache) { /* on vérifie que la clé "term" existe dans le tableau "cache", si oui alors on affiche le résultat */
    $('#_result').empty();
    $(cache[query]).appendTo('#_result');
  } else { /* sinon on fait une requête ajax vers index.php pour rechercher "term" */
    $('#_resultLoading').attr('style','');
    var code = $.ajax({
        type:'GET',
        url:'index.php',
        data:'badge/results/&queryString='+query,
        dataType:"html",
        async:true,
        cache:true,
        success:function(e){
          cache[query] = e; /* vide ou non, on stocke la liste des résultats avec en clé "term" */
          if(!e.length){ /* si aucun résultats, on renvoi un tableau vide pour informer qu'on a rien trouvé */
            var result = '<p>Aucun badge trouvé...</p>'
            $(result).appendTo('#_result');
          }else{ /* sinon on renvoie toute la liste */
            $('#_result').empty();
            $('#_result').append(e);
          }
          $('#_resultLoading').attr('style','display:none;');
        }
    });
  }
}

function resultRefresh(page) {
  selectSubmit(page);
}

function resultInverse() {
  var allcheckboxes = $('#_result').find("input").map(function(){
    this.checked = ! (this.checked);
  });
  return false;
}

function resultVide() {
  var allcheckboxes = $('#_result').find("input").map(function(){
    if (this.checked) { this.checked=false; }
  });
  return false;
}

function resultall2selection(query) {
  var query = document.getElementById('_resultQuery').value;
  if(query in cacheAll) { /* on vérifie que la clé "query" existe dans le tableau "cache", si oui alors on affiche le résultat */
    $.map(cacheAll[query], function (item){
      selectionAdd(item.id, item.value);
    });  /* envoi du résultat à source avec map() de jQuery (permet d'appliquer une fonction pour tous les éléments d'un tableau */
    selection2list();
  } else { /* sinon on fait une requête ajax vers index.php pour rechercher "term" */
    $('#_selectionLoading').attr('style','');
    var code = $.ajax({
        type:'GET',
        url:'index.php',
        data:'badge/resultsAll/&queryString='+query,
        dataType:"json",
        async:true,
        cache:true,
        success:function(e){
          cacheAll[query] = e; /* vide ou non, on stocke la liste des résultats avec en clé "term" */
          if(!e.length){ /* si aucun résultat, on renvoie un tableau vide pour informer qu'on n'a rien trouvé */
            var result = [{
              value: null,
              id: null,
            }];
            // on ne fait rien
          }else{ /* sinon on renvoie toute la liste */
            $.map(e, function (item){
              selectionAdd(item.id, item.value);
            });  /* envoi du résultat à source avec map() de jQuery (permet d'appliquer une fonction pour tous les éléments d'un tableau */
            selection2list();
          }
          $('#_selectionLoading').attr('style','display:none;');
        }
    });
  }
  return false;
}

function result2selection() {
  var allcheckboxes = $('#_result').find("input").map(function(){
    if (this.checked) {
      selectionAdd(this.name, this.value);
    }
  });
  selection2list();
  return false;
}

/*
 * variable contenant la liste des badges sélectionnés
 */
var selectionF = [];

function selectionAdd(numero, nomprenom) {
  if (! selectionF.find(record => record[0]==numero)) {
    selectionF.push( [numero, nomprenom] );
  }
}

function selection2list() {
  selectionF.sort(function(a, b){
    return a[1]>b[1];
  });
  $('#_selection').empty();
  if (selectionF.length>0) {
    $('#_selection').append("<a href='#' id='_commuteaction' onclick='javascript:selectionConfirme(); "+
      "return false;' title='Confirmer la sélection pour appliquer un droit'>"+
      "<i id='_commute' class='droite grand fas fa-arrow-circle-right'></i></a>");
  } else {
    selectionRetour();
  }
  if (selectionF.length == 1) {
    $('#_selection').append(selectionF.length+ " badge sélectionné.");
  } else if (selectionF.length > 1) {
    $('#_selection').append(selectionF.length+ " badges sélectionnés."+
        " <a href='#' onclick='selectionRemoveAll(); return false;' title='retirer tout'><i class='fas fa-eraser'></i></a>");
  }
  $('#_selection').append("<br/><br/>");
  
  $('#_selection').append("<ul id='_selectionul'>");
  selectionF.forEach(function(item, key) {
    //console.log(item);
    $('#_selectionul').append("<li>"+ item[1]+ " ("+ item[0]+ ") <a href='#' onclick='selectionRemove("+ key+ "); return false;' title='retirer'><i class='gauche fas fa-eraser'></i></a></li>");
  });
  return false;
}

function selectionRemove(id) {
  selectionF.splice(id, 1);
  selection2list();
  return false;
}

function selectionRemoveAll() {
  selectionF.splice(0);
  selection2list();
  return false;
}

function selectionConfirme() {
  $('#_zoneaffectation').css("left", '31%');
  $('#_zoneresultat').css("left", "-70%");
  $('#_zoneselection').css("left", "0%");
  $('#_commute').toggleClass('fa-arrow-circle-right fa-arrow-circle-left');
  $('#_commuteaction').attr('onclick', "javascript:selectionRetour(); return false;");
  $('#_selectionul li a').css("display", "none");
  selection2affectation();
  return false;
}

function selectionRetour() {
  $('#_selectionul li a').css("display", "inline");
  $('#_zoneresultat').css("left", "0%");
  $('#_zoneselection').css("left", "70%");
  $('#_commute').toggleClass('fa-arrow-circle-left fa-arrow-circle-right');
  $('#_commuteaction').attr('onclick', "javascript:selectionConfirme(); return false;");
  $('#_zoneaffectation').css("left", '101%');
  return false;
}

$(document).ready(function(){
    if (document.getElementById('_zoneresultat')) {
        var res = document.getElementById('_zoneresultat');
        $('#_zoneselection').css('top', res.offsetTop);
        $('#_zoneaffectation').css('top', res.offsetTop);
    }
});

/*
 * variable contenant ce qui figure dans affectation
 * càd horaires-lecteurs
 */
var affectationF = [];

/*
 * initialise la partie affectation avec le résultat
 * des droits de la séléction
 */
function selection2affectation() {
  //console.log("selection2affectation");
  $('#_affectationLoading').attr('style','');
  var code = $.ajax({
      type:'GET',
      url:'index.php',
      data:'profil/encommun/&queryString='+JSON.stringify(selectionF),
      dataType:"json",
      async:true,
      cache:true,
      success:function(e){
        //console.log("retour avec succès")
        affectationF = e;
        // traduction des données en HTML
        affectation2html();
        available2html();
        affectationInitSortable();
        $('#_affectationLoading').attr('style','display:none;');
      }
  });
  return false;
}

/*
 * variable contenant les sites permis à l'utilisateur
 */
var allowedSites = [];
/*
 * initialise la partie affectation avec le résultat
 * des droits de la séléction
 */
function getAllowedSites() {
  $.ajax({
      type:'GET',
      url:'index.php',
      data:'badge/allowedSites/',
      dataType:"json",
      async:true,
      cache:true,
      success:function(e){
        //console.log("retour avec succès")
        allowedSites = e;
      }
  });
  return false;
}

/*
 * rend un code html composé des droits
 * ceslecteurs : liste des lecteurs à afficher
 * site_id : site parent
 * - site
 *   - lecteurs
 */
function affectation2htmlSite(ceslecteurs, site_id) {
  html = "";
  ceslecteurs.forEach(function(item, key) {
    celecteur = lecteurs.find(rec => rec.id == item);
    if (celecteur.site_id == site_id) {
      if (celecteur.statut == 'visible') {
        html += "<li id='lc"+ celecteur.id+ "' class='draggable lecteur'>"+ 
            I_LECTEUR+ celecteur.libelle+ " ("+ celecteur.id+ ")</li>";
      }
    }
  }); // end ceslecteurs.foreach
  sites.forEach(function(siteItem, sitekey) {
    if (siteItem.id!=site_id & siteItem.parent_id == site_id) {
        //console.log(siteItem.id);
      html += affectation2htmlSite(ceslecteurs, siteItem.id);
    }
  }); // end sites.foreach
  if (html!="") {
    ceSite = sites.find(rec => rec.id == site_id);
    html = "<li id='st"+ ceSite.id+ "' class='draggable site'>"+ 
        I_SITE+ ceSite.nom+ "<ul>"+ html;
    html += "</ul></li>";
  }
  return html;
}

/*
 * Affiche dans le div affectation les informations sur le profil commun aux badges :
 * - les horaires
 *   - sites
 *     - lecteurs
 */
function affectation2html() {
  $('#_affectation').empty();
  $('#_affectation').append("<form id='_form' method='post' action='?badge/fromAffect' >");
  $('#_form').append("<p class='message'>N'apparaissent ci-dessous que les droits communs aux agents."+
    " Les changements préparés, une fois validés, viendront"+
    " en remplacement des droits ci-dessous.</p>");
  affectationF.forEach(function(item, key) {
    //console.log(item);
    $('#_form').append("<input type='hidden' name='_horaire_id["+ key+
      "]' value='"+ item.id+ "'/><h4>"+ I_HORAIRE + item.label+ /*" (" +item.id + ")"+*/ "</h4>");
    $('#_form').append("<ul id='hr"+ item.id+ "' class='droppable'>");
    var html = affectation2htmlSite(item.lecteurs, 0);
    $('#hr'+ item.id).append(html);
    if (html=='') {
      $('#_form h4 a').css("display", "inline");
    }
  });
  $('#_form').append("<p>Ajouter&nbsp;: "+
    "<select id='__horaire_id_to_add' name='__horaire_id_to_add' onchange='javascript:affectationSelectHoraireToAdd(this);'></select>"+
    "&nbsp;<a id='__horaire_add' href='#' onclick='javascript:affectationAddHoraire(); return false;'"+
    " title=\"ajouter l'horaire\" class='hidden'><i class='fas fa-plus'></i></a></p>");
  affectationSelectHoraire();
  $('#_form').append("<label for='__prog'>Programmer</label>"
      +"<input type='checkbox' name='__prog' id='__prog' value='oui' onChange='javascript:affectationProgDisplay();'>"
      +"<span id='__prog_prog' style='display:none;'><p class='message'>Attention, les badges"
      +" seront verrouillés (pas de changement de droits possibles) pendant le temps de la programmation."
      +" S'il y a une date de fin, les badges reviendront à leurs droits actuels.</p>"
      +"<p>À partir de <input type='datetime-local' name='__prog_depuis'> jusqu'au <input type='datetime-local' name='__prog_jusque'></p></span>"
      +"<!/p>");
  $('#_form').append("<p><input type='button' value='Envoyer' onClick='affectationSend(); return false;' /></p>");
}

/*
 * Montre/cache le span __prog_prog suivant __prog
 * @returns {undefined}
 */
function affectationProgDisplay() {
    if (document.getElementById('__prog').checked) {
        document.getElementById('__prog_prog').style = 'display:initial';
    } else {
        document.getElementById('__prog_prog').style = 'display:none';
    }
}

/*
 * Envoie le résultat de la sélection et de la nouvelle affectation au
 * script php
 */
function affectationSend() {
  const infos = [/*'allowedSites', */'affectationF', 'selectionF'];
  for (index in infos) {
    $('#_form').append("<input type='hidden' name='"+ infos[index]+ "' value='"+ JSON.stringify(eval(infos[index]))+ "' />");
  }
  $('#_form').submit();
  return false;
}

function affectationSelectHoraire() {
  $('#__horaire_id_to_add').append("<option value=''>Choisir un horaire à ajouter</option>");
  var disabled = "";
  for (item in horaires) {
    //console.log(horaires[item]);
    if (affectationF.find(record => record.id==horaires[item].id)) {
      disabled = " disabled";
    } else {
      disabled = "";
    }
    $('#__horaire_id_to_add').append("<option value="+ horaires[item].id + disabled+ ">"+ horaires[item].value+ "</option>");
  }
}

function affectationSelectHoraireToAdd() {
  if (document.getElementById('__horaire_id_to_add').value>'') {
     document.getElementById('__horaire_add').style.visibility='visible';
   } else {
     document.getElementById('__horaire_add').style.visibility='hidden';
   }
}

// Ajoute un enregistrement Tranche horaire à affectation
function affectationAddHoraire() {
  // quel horaire ajouter
  var horaire_id_to_add = document.getElementById('__horaire_id_to_add').value;
  // déjà présent ?
  if (! affectationF.find(record => record.id==horaire_id_to_add)) {
    const item = {"id": horaire_id_to_add,"label":horaires.find(item0 => item0.id==horaire_id_to_add).value,"lecteurs":[]};
    // ajout horaire
    affectationF.push(item);
    affectation2html();
    affectationInitSortable();
  }
}
// ajoute un lecteur
function affectationAddLecteur(lecteur_id, horaire_id) {
  // trouver horaire
  const aff_id = affectationF.findIndex(record => record.id==horaire_id);
  if (aff_id === -1) return false;
  // ajouter lecteur
  affectationF[aff_id].lecteurs.push(Number(lecteur_id));
  return affectationF.find(rec => rec.id == horaire_id).lecteurs;
}
// retire un lecteur
function affectationDelLecteur(lecteur_id, horaire_id) {
  //console.log(horaire_id, lecteur_id);
  // trouver horaire
  const aff_id = affectationF.findIndex(record => record.id==horaire_id);
  if (aff_id === -1) return false;
  // trouver lecteur
  const lect_id = affectationF[aff_id].lecteurs.findIndex(rec => rec == lecteur_id);
  if (lect_id === -1) return false;
  // retirer de la liste
  affectationF[aff_id].lecteurs.splice(lect_id, 1);
  return affectationF.find(rec => rec.id == horaire_id).lecteurs;
}
// retire tous les lecteurs enfants d'un site (et des sites enfants)
function affectationDelLecteursSite(site_id, horaire_id) {
  // recupérer tous les lecteurs d'un site et ses enfants
  const tousLecteurs = getLecteursSite(site_id);
  // on extrait l'horaire de l'affectation
  const cetHoraire = affectationF.find(rec => rec.id == horaire_id);
  if (cetHoraire === undefined) return false;
  /* faire une copie du tableau, car sinon le foreach est affecté par la
   * suppression d'éléments
   */
  var lect = new Array().concat(cetHoraire.lecteurs);
  // on regarde chaque lecteur de horaire
  lect.forEach(function(item) {
    // le lecteur est-il dans la liste des lecteurs du site ou descendants
    if (tousLecteurs.indexOf(item) != -1) {
      // on le supprime s'il existe
      affectationDelLecteur(item, horaire_id);
    } // end if
  }); // end foreach
  return affectationF.find(rec => rec.id == horaire_id).lecteurs;
}
/* ajoute tous les lecteurs enfants d'un site (et des sites enfants)
 * on prend tous les lecteurs de la liste available ie tous ceux
 * qui ne sont pas déjà dans un horaire
 */
function affectationAddLecteursSite(site_id, horaire_id) {
  // recupérer tous les lecteurs d'un site et ses enfants
  const tousLecteurs = getLecteursSite(site_id);
  // on extrait l'horaire de l'affectation
  const cetHoraire = affectationF.find(rec => rec.id == horaire_id);
  // qui ne sert qu'à vérifier qu'il existe bien
  if (cetHoraire === undefined) return false;
  // on cherche ensuite tous les lecteurs déjà affectés à un horaire
  const lecteursDejaAffectes = affectationF2lecteurs();
  // on boucle sur les lecteurs à affecter
  tousLecteurs.forEach(function(item) {
    // n'est-il pas déjà affecté ?
    if (lecteursDejaAffectes.indexOf(item) == -1) {
      // on l'ajoute
      affectationAddLecteur(item, horaire_id);
    }
  });
  return affectationF.find(rec => rec.id == horaire_id).lecteurs;
}
/*
 * déplace un lecteur d'un horaire à un autre
 */
function affectationMoveLecteur(lecteur_id, horaire_id_from, horaire_id_to) {
  affectationDelLecteur(lecteur_id, horaire_id_from);
  return affectationAddLecteur(lecteur_id, horaire_id_to);
}
/*
 * déplace tous les lecteurs d'un horaire à un autre
 */
function affectationMoveLecteursSite(site_id, horaire_id_from, horaire_id_to) {
  // on extrait l'horaire de l'affectation
  const cetHoraire_to = affectationF.find(rec => rec.id == horaire_id_to);
  if (cetHoraire_to === undefined) return false;
  // on extrait l'horaire source
  const cetHoraire_from = affectationF.find(rec => rec.id == horaire_id_from);
  if (cetHoraire_from === undefined) return false;
  // on récupère la liste du site et de ses descendants
  const cesSites = getSiteChildren(site_id);
  /* faire une copie du tableau, car sinon le foreach est affecté par la
   * suppression d'éléments
   */
  var lect = new Array().concat(cetHoraire_from.lecteurs);
  // on regarde chaque lecteur de horaire
  lect.forEach(function(item) {
    if (cesSites.indexOf(lecteurs.find(rec => rec.id == item).site_id) != -1) {
      affectationMoveLecteur(item, horaire_id_from, horaire_id_to);
    }
  }); // end foreach
  return affectationF.find(rec => rec.id == horaire_id_to).lecteurs;
}


/* Recherche la liste de tous les lecteurs enfants d'un site ou
 * des sites enfants de ce site
 * Cette liste est issue des variables sites et lecteurs
 * qui sont produite dans la méthode badge::HTML_affectation()
 * Le retour est un tableau de lecteur_id
 */
function getLecteursSite(site_id) {
  var list = [];
  const cesSites = getSiteChildren(site_id);
  for(siteItem in cesSites) {
    //console.log(siteItem, cesSites[siteItem]);
    const cesLecteurs = lecteurs.filter(rec => rec.site_id == cesSites[siteItem]);
    //console.log(cesLecteurs);
    for(lecteurItem in cesLecteurs) {
      list.push(cesLecteurs[lecteurItem].id);
    }
  }
  return list;
}
/* rend la liste de site_id (et non l'id de l'objet sites) du site_id
 * et de tous les sites enfants de ce site_id
 */
function getSiteChildren(site_id) {
  if (sites.find(rec => rec.id == site_id)) {
    // le site existe, on ajoute à la liste et on recherche les enfants
    var list = [site_id];
  } else { // le site_id n'existe pas
    return [];
  }
  // recherche des enfants
  const children = sites.filter(record => record.parent_id == site_id);
  if (children.length > 0) { // il y a des enfants
    for(child in children) {
      ceSite = sites.find(rec => rec.id == children[child].id);
      // on ajoute à la liste le résultat de la recherche récursive
      list = list.concat(getSiteChildren(ceSite.id));
    }
  }
  return list;
}

$(document).ready(function(){
  affectation2html();
  getAllowedSites();
  available2html();
  affectationInitSortable();
});


/*
 * affiche dans le cadre aside des affectations la liste des sites
 * et lecteurs auxquels on a droit non déjà choisis dans affectation
 */
function available2html(){
  $('#_lecteurs').empty();
  $('#_lecteurs').append("<ul id='_lecteursListe' class='droppable'>");
  $('#_lecteursListe').append(available2htmlSite(affectationF2lecteurs(), 0));
  return false;
}

/*
 * retourne la liste des lecteurs affectés quel que soit son horaire
 */
function affectationF2lecteurs(){
  var _lecteurs = [];
  for(item in affectationF){
    _lecteurs = _lecteurs.concat(affectationF[item].lecteurs);
  }
  return _lecteurs;
}

/*
 * affiche la listes des lecteurs triés par site pas encore affectés
 * dans la zone Sites et lecteurs (available)
 * ces lecteurs est un tableau simple de lecteur_id
 */
function available2htmlSite(pasCesLecteurs, site_id) {
  html = "";
  // tous les lecteurs du site
  for(item in lecteurs) {
    if (lecteurs[item].site_id==site_id & !pasCesLecteurs.includes(Number(lecteurs[item].id))) {
      if (lecteurs[item].statut == 'visible') {
        html += "<li id='lc"+ lecteurs[item].id+ "' class='draggable lecteur'>"+ I_LECTEUR+ lecteurs[item].libelle+ " ("+ lecteurs[item].id+ ")</li>";
      }
    }
  }; // end for lecteurs
  // tous les sous-sites du site
  sites.forEach(function(siteItem, sitekey) {
    if (siteItem.id>site_id & siteItem.parent_id == site_id) {
      html += available2htmlSite(pasCesLecteurs, siteItem.id);
    }
  }); // end sites.foreach
  if (html!=="") {
      //console.log(site_id);
    var ceSite = sites.find(rec => rec.id === site_id);
    html = "<li id='st"+ ceSite.id+ "' class='draggable site'>"+ I_SITE+ ceSite.nom+ /*" ("+ ceSite.id+ ")"+*/ "<ul>"+ html;
    html += "</ul></li>";
  }
  return html;
}


/*
 * Partie de gestion des mouvements des affectations
 * entre les tranches horaires et avec les lecteurs disponibles
 */

function affectationInitSortable() {
  $( ".draggable" ).draggable({
    revert: true,
    zIndex: 1000
  });
  $( ".droppable" ).droppable({
    revert: "invalid",
    activeClass: "active",
    hoverClass: "hover",
    placeholder: "ui-state-highlight",
    classes: {
      "ui-droppable-active": "ui-state-active",
      "ui-droppable-hover": "ui-state-hover"
    },
    activate: function(event, ui) {
      //console.log(ui);
      var elmt = ui.draggable[0];
      while (elmt.id.substring(0, 2) != 'hr' && elmt.id != '_lecteursListe') {
        elmt = elmt.parentElement;
        // console.log(elmt);
      }
      ui.draggable.startPos = elmt.id;
    },
    drop: function(event, ui) {
      var id = ui.draggable[0].id;
      var oldparentid = ui.draggable.startPos;
      var newparent = this;
      console.log(newparent.id+ " a reçu "+ id+ " de la part de "+ oldparentid);
      if (oldparentid == newparent.id) {
        console.log('annulé');
      } else {
        // d'un horaire à un autre
        if (newparent.id.substring(0, 2) == 'hr' && oldparentid.substring(0, 2) == 'hr') {
          if (id.substring(0, 2) == 'lc') { // un lecteur
            affectationMoveLecteur(id.substring(2), oldparentid.substring(2), newparent.id.substring(2));
          } else { // un site
            affectationMoveLecteursSite(id.substring(2), oldparentid.substring(2), newparent.id.substring(2));
          }
        } else { // ajouter/supprimer à horaire
          if (newparent.id == '_lecteursListe') { // supprimer
            if (id.substring(0, 2) == 'lc') { // un lecteur
              affectationDelLecteur(id.substring(2), oldparentid.substring(2));
            } else { // un site
              affectationDelLecteursSite(id.substring(2), oldparentid.substring(2));
            }
          } else { // ajouter
            if (id.substring(0, 2) == 'lc') { // un lecteur
              affectationAddLecteur(id.substring(2), newparent.id.substring(2));
            } else { // un site
              affectationAddLecteursSite(id.substring(2), newparent.id.substring(2));
            }
          }
        }
      } // end if else annulé
      affectation2html();
      available2html();
      return affectationInitSortable();
    },
  });
}

/*
 * Partie sur la recopie des droits d'un badge à l'autre
 */

function copyDisplaySide(){
    $('aside#_badgeRef').removeAttr('class');
}

/*
 * Action du bouton Voir les droits
 * Affichage des droits du badge source
 */
function copyShowRights() {
    displayHtmlRights('#_badgeRefProfil', document.getElementById('_vid').value);
    $('div#actualRights fieldset').attr('class', 'old');
    /*$('#_add').attr('disabled', 'button hidden disabled');*/
    $('#_add').attr('disabled', '');
    /*$('#_valeur').attr('disabled', '');*/
    $('#_copy input').removeAttr('disabled');
}

/*
 * @param {string} div
 *        {longint} numeroBadge
 * @returns {none}
 */
function displayHtmlRights(div, numeroBadge) {
    var query = btoa(unescape(encodeURIComponent(numeroBadge)));
    if(query in cache) { /* on vérifie que la clé "term" existe dans le tableau "cache", si oui alors on affiche le résultat */
        $(div).empty();
        $(cache[query]).appendTo(div);
    } else { /* sinon on fait une requête ajax vers index.php pour rechercher "term" */
        $(div.concat(' #_resultLoading')).attr('style','');
        
        var code = $.ajax({
            type:'GET',
            url:'index.php',
            data:'badge/htmlRights/&queryString='+query,
            dataType:"html",
            async:true,
            cache:true,
            success:function(e){
                cache[query] = e; /* vide ou non, on stocke la liste des résultats avec en clé "query" */
                /* on vide le div avant de le remplir */
                $(div).empty();
                if(!e.length){ /* si aucun résultats, on renvoi un tableau vide pour informer qu'on a rien trouvé */
                    var result = '<p>Badge non trouvé...</p>'
                    $(result).appendTo(div);
                }else{ /* sinon on renvoie toute la liste */
                    $(div).empty();
                    $(div).append(e);
                }
                $(div.concat(' #_resultLoading')).attr('style','display:none;');
            }
      });
    }
}

function copyRights() {
    // badge à écraser
    var badge = $('#_badgeComplet').val();
    var badgeNumero = $('#_badgeNumero').val();
    // bardge source depuis lequel prendre le profil
    var badgeSource = document.getElementById('_valeur').value;
    var badgeNumeroSource = document.getElementById('_vid').value;
    var profil = $('#_badgeRefProfil fieldset legend').text();
    profil = profil.substring(31, profil.length-4)
    
    if (confirm("Confirmez-vous l'écrasement des droits du badge [" + badge + 
            "] par les droits ["+ profil + "] du badge [" + badgeSource + "] ?" )) {
        var queryString = btoa(unescape(encodeURIComponent(profil+ ";"+ badgeNumeroSource)));
        document.location.href = '?badge/copyRights/'+ badgeNumero+ '/&queryString='+ queryString;
        return true;
    } else {
        return false;
    }
}

/*
 * Fonction pour acceder directement à la modification d'un badge particulier
 */
function editRightsDirect(numero, nomprenom) {
    selectionAdd(numero, nomprenom);
    selection2list();
    selectionConfirme();
}
