/*
 * GestionDeBadges
 * 
 * Copyright 2017-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net>
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

function add(field, nlg) {
  var selectElement = document.getElementById(field+ '_site_id');
  /* c'est bon, on ajoute une ligne au dessus */
  $('#'+ field).append(
    "<li id='"+ field+ nlg+ "'>"+
    selectElement.options[selectElement.selectedIndex].text+
    $('form select[name="'+ field+ '_site_id"].options['+ $('form input[name="'+ field+ '_site_id"]').val()+ ']').text()+
    "<input type='hidden' name='"+ field+ "["+ nlg+ "]' value='"+
    $('form select[name="'+ field+ '_site_id"]').val()+
    "' />"+
    " <a href='#' onClick=\"javascript:_delete('"+ field+ "', "+ nlg+
      "); return false;\"><i class=\'far fa-minus-square\'></i>&nbsp;retirer"+ "</a>"+
    "</li>"
  );
  /* changer la valeur du lien lg_add */
  $('form a[id="'+field+'_add"]').replaceWith('<a id="'+ field+
    '_add" href=\'#\' onclick=\'javascript:add("'+ field+ '", '+ (++nlg)+
    '); return false;\'><i class=\'far fa-plus-square\'></i>&nbsp;ajouter</a>');
  return false;
}

function _delete(field, nlg) {
  //console.log(field+nlg);
  $('#'+field+nlg).remove();
  return false;
}

function valid() {
  // TODO : vérifier que les choix obligatoires sont bien faits
  document.groupeForm.submit();
}

function validnew() {
  s = "__";
  if (document.getElementById(s.concat("nominit"))) {
    if (document.getElementById(s.concat("nom")).value == document.getElementById(s.concat("nominit")).value) {
      alert("Le nom doit être différent de l'original");
      return false;
    }
    document.getElementById(s.concat("id")).value = '';
    document.getElementById("enregistrer").disabled = true;
  }
  return valid();
}
