<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

If(! $session->isLoggedOrHasRight('groupe')){
    return;
}

$groupe = new \Badge\Model\Groupe($objects);

switch ($action) {

  case 'fromForm' :  {
    _echo(_h2("Habilitations de groupes"));
    if ($groupe->fromForm()) {
      _echo(HTML_message("Habilitations pour le groupe bien enregistrées."));
      if ($groupe->save()) {
        header("Location: ". $url_base."?groupe/saved/");
      } else {
        _echo($groupe->message);
      }
    } else {
      _echo($groupe->message);
    }
  }

  case 'add' : {
    _echo(_h2("Ajout d'habilitations de groupe"));
    /*if ($id>0 && $action!='fromForm') {
      $groupe->id = $id;
      if (! $groupe->get()) {
        _echo($this−>error);
        break;
      }
    }*/
    _echo($groupe->HTML_form());
    break;
  }

  case 'edit' : {
    _echo(_h2("Edition d'habilitations de groupe"));
    if ($id>0 && $action!='fromForm') {
      $groupe->id = $id;
      if (! $groupe->get()) {
        _echo($this−>error);
        break;
      }
    }
    _echo($groupe->HTML_form());
    break;
  }

  case 'view' : {
    _echo(_h2("Habilitations du groupe"));
    $groupe->id = $id;
    if (! $groupe->get()) {
      _echo($this−>error);
      break;
    }
    _echo($groupe->HTML_view());
    _echo($groupe->HTML_lastlogs());
    break;
  }

  case 'saved' : {
    /*_echo(_h2("Enregistrement des habilitations d'un groupe"));*/
    _echo(HTML_message("L'enregistrement s'est bien pass&eacute;."));
  }

    case 'list' : {
        _echo (_h2("Habilitations de groupes"));
        _echo($session->HTML_mesGroupes());
        _echo($groupe->HTML_list());
        _echo("<a href='?groupe/add'><i class='far fa-plus-square'></i>Ajouter un groupe d'habilitation</a>\n");
        break;
    }

} // end switch
