<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2023 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

$etat = new \Badge\Model\Prog($objects);

switch ($action) {

    /*
     * Appelé depuis script cron
     * Vérifie que des tâches sont à faire et les exécute
     */
    case 'differ' :
        echo "\n-----\n". date("Y-m-d H:i"). "\n";
        echo "Affectations programmées.\n";
        $prog = new \Badge\Model\Prog($objects);
        $prog->check();
        _echo($prog->message. "\n");
        break;
  
    case 'cancel' :
        _echo(_h2("Affectations programmées"));
        If(! $session->isLoggedOrHasRight('editBadges')){
            return;
        }
        $prog = new \Badge\Model\Prog($objects);
        $prog->numeroBadge = $id;
        $prog->getBadge();
        $prog->cancel();
        _echo($prog->message);
        break;
    
    case 'forestall' :
        _echo(_h2("Affectations programmées"));
        If(! $session->isLoggedOrHasRight('editBadges')){
            return;
        }
        $prog = new \Badge\Model\Prog($objects);
        $prog->numeroBadge = $id;
        $prog->getBadge();
        $prog->forestall();
        _echo($prog->message);
        break;
    
} // end switch