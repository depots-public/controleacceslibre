<?php
/*
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

If(! $session->isLoggedOrHasRight('lireBadges')){
    return;
}

$profil = new Badge\Model\Profil($objects);
if ($id!==null) $profil->id = $id;

switch ($action) {

    // appelé depuis l'api
    case 'encommun' : {
        $queryString = $_GET['queryString'];
        if (strlen($queryString)<2) { http_response_code(500); die; }
        $badges = json_decode($queryString); // array of (id, label)

        // retrouver tous les profils des badges concernés
        $badge = new \Badge\Model\Badge($objects);
        $profils = array();
        foreach($badges as $arrayBadge) {
          $badge->numero=$arrayBadge[0];
          $badge->get();
          if (! in_array($badge->profil_id, $badges)) {
            $profils[] = $badge->profil_id;
          }
        }
        // puis trouver ce qu'ils ont en commun
        $profil = new \Badge\Model\Profil($objects);
        $droitsCommuns = $profil->communDroits($profils);
        $horaire = new \Badge\Model\Horaire($objects);

        $horaireReponse = array();
        foreach($droitsCommuns as $horaire_id=>$lecteurs_id) {
          $horaire->id = $horaire_id;
          $horaire->get();
          $horaireReponse[] = array(
            'id'=>$horaire->id,
            'label'=>$horaire->nom,
            'lecteurs'=>$lecteurs_id
          );
        }

        http_response_code(200);
        header('Content-type: application/json');
        echo(json_encode($horaireReponse));
        break;
    }

} // end switch
