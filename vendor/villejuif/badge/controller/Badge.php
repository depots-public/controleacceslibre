<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2023 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

If(! $session->isLoggedOrHasRight('lireBadges')){
    return;
}

$badge = new \Badge\Model\Badge($objects);
if ($id!==null) { $badge->numero = $id; }

switch ($action) {

    case 'fromForm' :
        If(! $session->isLoggedOrHasRight('editBadges')){
            return;
        }
        _echo(_h2("Enregistrement d'un badge"));
        $badge->get();
        if ($badge->fromForm()) {
            if ($badge->save()) {
                header("Location: ". $url_base."?badge/saved/{$badge->numero}");
            } else {
                _echo(HTML_error($badge->message));
            }
        } else {
            _echo(HTML_error($badge->message));
        }

    case 'edit' :
        If(! $session->isLoggedOrHasRight('editBadges')){
            return;
        }
        if ($badge->numero == null) {
            _echo(_h2("Cr&eacute;ation d'un badge"));
            _echo(HTML_error("Pour le moment impossible."));
        } else {
            _echo(_h2("Edition d'un badge"));
            $badge->get();
            _echo($badge->HTML_form());
        }
        break;

    case 'copyRights' :
        If(! $session->isLoggedOrHasRight('editBadges')){
            return;
        }
        _echo(_h2("Recopie des droits d'un badge"));
        $queryString = base64_decode($_GET['queryString']);
        // badge destination = celui dont on va écraser le profil par la source
        if (! $badge->get()) {
            _echo(HTML_error($badge->message));
            break;
        }
        $profilAncien_id = $badge->profil_id;
        list($profil, $badgeNumeroSource) = explode(";", $queryString);
        $profil_id = substr($profil, strpos($profil, "n°")+3);
        list($profil_id) = explode(" ", $profil_id);
        //_echo(HTML_debug($profil_id));
        // badge source = celui qu'on prend comme modèle pour appliquer à l'autre le profil
        $badgeSource = new \Badge\Model\Badge($objects);
        //_echo(HTML_debug($badgeNumeroSource));
        $badgeSource->numero = $badgeNumeroSource;
        if (! $badgeSource->get()) {
            _echo(HTML_error("Source : ". $badgeSource->message));
            break;
        }
        if ($badgeSource->profil_id != $profil_id) {
            _echo(HTML_error("Erreur dans la requête, le profil badge source ne correspond pas."));
            break;
        }
 
        // affectation du profil au badge
        $badge->profil_id = $badge->profil->id = $badgeSource->profil_id;
        $badge->profil->get();
        _echo(HTML_debug("Affectation d'un profil déjà existant {$badge->profil->nom} ({$badge->profil->id})".
            " au badge {$badge->prenom} {$badge->nom} ({$badge->numero})"));
        $badge->save();
        if ($badge->profil->error) {
            _echo(HTML_error($badge->message));
        }
 
        // regarder si l'ancien profil est devenu inutile
        $badge->profil->id = $profilAncien_id;
        if (! $badge->profil->is_used()) { // pas utilisé ?
            $badge->profil->get();
            _echo(HTML_debug("L'ancien profil {$badge->profil->nom} ({$badge->profil->id}) ne sert plus, on le supprime."));
            $badge->profil->delete(); // alors on fait le ménage
        }
        unset($badge);

        break;

    // appelé depuis l'api.
    case 'search' :
        $queryString = $_GET['queryString'];
        if (strlen($queryString)<2) { http_response_code(500); die; }

        $array = $badge->array_search($queryString);
        if ($badge->error) { http_response_code(500); die; }

        http_response_code(200);
        header('Content-type: application/json');
        echo json_encode($array);
        break;

    // appelé depuis l'api
    case 'results' :
        // retourne du code HTML, la liste dans un tableau
        $queryString = base64_decode($_GET['queryString']);
        if (strlen($queryString)<2) { http_response_code(500); die; }
        $page =
            (isset($_GET['page']) && $_GET['page']>"" && preg_match("~^[0-9]+$~", $_GET['page']) ?  $_GET['page'] : 1);

        $queryResult = $badge->resultQuery_result($queryString);
        $array = $badge->array_results($queryResult, $page);
        if ($badge->error) { echo($badge->message); http_response_code(500); die(); }

        // enregistrer dans le journal
        $log = (new \Badge\Model\Log($objects))->add('api_results', 'badge', null, json_decode($queryString));
        unset($log);

        http_response_code(200);
        header('Content-Type: text/html; charset=utf-8');
        _echo($badge->HTML_resultNav($queryResult, $page));

        if (count($array)==0) {
            _echo(HTML_message("Aucun résultat avec ce filtre."));
        } else {
            _echo(HTML_array2table($array));
        }
        break;

    // appelé depuis l'api
    // renvoie le code html des droits d'un badge particulier
    case 'htmlRights' :
        // retourne du code HTML, la liste dans un tableau
        $queryString = base64_decode($_GET['queryString']);
        if (strlen($queryString)<4) { http_response_code(500); die; }

        $badge->numero = $queryString;
        $badge->get();
        if ($badge->error) {
            echo($badge->message); 
            http_response_code(500); die();
        }
        $badge->profil->get();
        $html = $badge->profil->HTML_view();

        if ($badge->profil->error) { echo($badge->profil->message); http_response_code(500); die(); }

        http_response_code(200);
        header('Content-Type: text/html; charset=utf-8');
        _echo($html);
        break;

    // appelé depuis l'api
    case 'resultsAll' :
        // retourne des éléments (id, value) en JSON
          // ne renvoie pas les badges verrouillés, contrairement à results
        $queryString = base64_decode($_GET['queryString']);
        if (strlen($queryString)<2) { http_response_code(500); die; }

        $queryResult = $badge->resultQuery_result($queryString);
        $array = $badge->array_results($queryResult, false, false);
        if ($badge->error) { http_response_code(500); die; }

        http_response_code(200);
        header('Content-type: application/json');
        //if (count($array)!=0) {
        echo($badge->JSON_array($array));
        //}
        break;

    // appelé depuis l'api
    case 'allowedSites' :
        http_response_code(200);
        header('Content-type: application/json');
        echo $badge->JSON_sitesAutorises();
        break;
       
    case 'edit' :
        _echo(_h2("Edition d'un badge"));
        If(! $session->isLoggedOrHasRight('editBadges')){
            return;
        }
        $badge->get();
        _echo($badge->HTML_form());
        break;

    case 'saved' :
        _echo(_h2("Enregistrement d'un badge"));
        _echo(HTML_message("L'enregistrement s'est bien pass&eacute;."));

    case 'view' :
        _echo(_h2("Information sur un badge"));
        $badge->get();
        _echo($badge->HTML_view());
        _echo($badge->HTML_lastlogs());
        break;

    case 'fromAffect' :
        _echo(_h2("Affectation de droits de badges"));
            If(! $session->isLoggedOrHasRight('editBadges')){
                return;
            }
        $badge->fromAffect();
        if ($badge->error) {
            _echo(HTML_error($badge->message));
        }
        //break;

    case 'editRightsOne' :
    case 'list' :
        _echo(_h2("Badges"));
        // partie de gauche
        _echo("<article id='_zoneresultat'>");
        _echo("<nav id='_zonecritere'>");
        _echo($badge->HTML_filterForm());
        _echo("</nav>");
        _echo($badge->HTML_result());    
        _echo("</article>");
        // partie centrale
        _echo("<aside id='_zoneselection'>");
        _echo($badge->HTML_selection());
        _echo("</aside>");
        // partie de droite
        _echo("<article id='_zoneaffectation'>");
        _echo("<aside id='_zonelecteur'>");
        _echo($badge->HTML_lecteurs());
        _echo("</aside>");
        _echo($badge->HTML_affectation());
        _echo("</article>");
        if ($action == 'editRightsOne') {
            $badge->get();
            _echo($badge->HTML_editRightsOne());
        }
        // ligne suivante retirée, en principe, on ne gère pas les badges ici
        // mais dans Senator ou GLPI ou autre powershell
        //_echo("<a href='?badge/edit'><i class='far fa-plus-square'></i>Ajouter un badge</a>\n");
        break;

} // end switch
