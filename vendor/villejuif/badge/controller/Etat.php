<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

$etat = new \Badge\Model\Etat($objects);

echo "\n-----\n". date("Y-m-d H:i"). "\n";

switch ($action) {

  // appelé depuis le script cron
  case 'save' : {
      echo "Sauvegarde de l'état des droits sur chacun des lecteurs.\n";
      echo $etat->saveAllLecteurs();
      if ($etat->error) {
          echo "ERREUR: ". $etat->message. "\n";
      }
      break;
  }
  
  case 'diff' : {
      echo "Changement dans les droits sur chaque lecteur depuis hier.\n";
      echo $etat->diffLecteurs();
      if ($etat->error) {
          echo "ERREUR: ". $etat->message. "\n";
      }
      break;
  }
  
  case 'archive' : {
      echo "Archivage des états de plus de trois mois.\n";
      echo $etat->archivage();
      if ($etat->error) {
          echo "ERREUR: ". $etat->message. "\n";
      }
      
      echo "Archivage des logs de l'application de plus de 12 mois.\n";
      $log = new \Badge\Model\Log($objects);
      echo $log->archivage();
      if ($log->error) {
          echo "ERREUR: ". $log->message. "\n";
      }
      break;
  }
  
} // end switch