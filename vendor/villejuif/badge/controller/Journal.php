<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

/*
 * Journal est le journal des accès sur les lecteurs
 */

If(! $session->isLoggedOrHasRight('journal')){
    return;
}

$journal = new \Badge\Model\Journal($objects);
if ($id!==null) $journal->id = $id;

switch ($action) {

  // appelé depuis l'api
  case 'apiGet' : {
    http_response_code(200);
    header('Content-Type: text/html; charset=utf-8');
    $journal->get();
    if ($journal->error) {
      _echo(HTML_error($journal->message));
    } else {
      _echo($journal->HTML_view());
    }
    break;
  }

  case 'list' : {
    _echo(_h2("Journal des accès"));
    $journal->fromSearchForm();
    _echo($journal->HTML_searchForm());
    _echo($journal->HTML_searchResult());
    break;
  }

} // end switch
