<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

$site = new Badge\Model\Site($objects);
if ($id !== null) { $site->id = $id; }

switch ($action) {

  case 'move' : { // called by API
        If(! $session->isLoggedOrHasRight('site')){
            http_response_code(500); _echo(HTML_error("Erreur habilitation")); die;
        }
    // on sait quel site est bougé
    if ($site->id === null) { http_response_code(500); _echo(HTML_error("Erreur id")); die; }
    // le site existe
    $site->get();
    if ($site->error) { http_response_code(500); _echo(HTML_error("Erreur récup site")); die; }
    // on sait quel site parent
    if (! isset($url_query[3])) { http_response_code(500); die; }
    $parent_id = $url_query[3];
    // le parent existe
    if ($parent_id>0) {
      $parent = new Badge\Model\Site($objects);
      $parent->id = $parent_id;
      $parent->get();
      if ($parent->error) { http_response_code(500); die; }
      // // Référence circulaire, le parent choisi est le site lui-même.
      if ($parent_id==$site->id) { http_response_code(500); die; }
      // le parent n'est pas descendant du site bougé
      if ($parent->is_enfantde($parent_id, $site->id)) { http_response_code(500); die; }
    }
    $site->parent_id = $parent_id;
    $site->save();
    // la sauvegarde s'est bien passée
    if ($site->error) { http_response_code(500); die; }
    http_response_code(200);
    header('Content-type: application/json');
    echo json_encode(array('message'=>'saved'));
    break;
  }

  case 'fromForm' :  {
    _echo(_h2("Enregistrement d'un site"));
        If(! $session->isLoggedOrHasRight('site')){
            return;
        }
    if ($site->fromForm()) {
      if ($site->save()) {
        header("Location: ". $url_base."?site/saved/{$site->id}");
      } else {
        _echo(HTML_error($site->message));
      }
    } else {
      _echo(HTML_error($site->message));
    }
  }

  case 'edit' : {
        If(! $session->isLoggedOrHasRight('site')){
            return;
        }
    if ($site->id == null) {
      _echo(_h2("Cr&eacute;ation d'un site"));
    } else {
      _echo(_h2("Edition d'un site"));
    }
    $site->get();
    _echo($site->HTML_form());
    break;
  }

  case 'view' : {
    _echo(_h2("Informations sur un site"));
    $site->get();
    _echo($site->HTML_view());
    _echo($site->HTML_lastlogs());
    break;
  }

  // appelé depuis l'api.
  case 'search' : {
    $queryString = $_GET['queryString'];
    if (strlen($queryString)<2) { http_response_code(500); die; }

    $array = $site->array_search($queryString);
    if ($site->error) { http_response_code(500); die; }

    http_response_code(200);
    header('Content-type: application/json');
    echo json_encode($array);
    break;
  }

  case 'saved' : {
    _echo(_h2("Enregistrement d'un site"));
    _echo("<p>L'enregistrement s'est bien pass&eacute;.</p>\n");
    //break;
  }

  case 'list' : {
        If(! $session->isLoggedOrHasRight('site')){
            return;
        }
    _echo(_h2("Sites ou groupes de lecteurs"));
    _echo($site->HTML_interactiveList());
    _echo("<a href='?site/edit'><i class='far fa-plus-square'></i>Ajouter un site ou groupe de lecteurs</a>\n");
    break;
  }  
  
} // end switch