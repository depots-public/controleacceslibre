<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

$lecteur = new \Badge\Model\Lecteur($objects);
if ($id!==null) $lecteur->id = $id;
    
switch ($action) {

    case 'move' : { // called by API
        If(! $session->isLoggedOrHasRight('lecteur')){
            return;
        }
        // on sait quel lecteur est bougé
        if ($lecteur->id === null) { http_response_code(500); die; }
        // le lecteur existe
        $lecteur->get();
        if ($lecteur->error) { http_response_code(500); die; }
        // on sait quel lecteur parent
        if (! isset($url_query[3])) { http_response_code(500); die; }
        $site_id = $url_query[3];
        // le parent existe
        if ($site_id>0) {
            $site = new Badge\Model\Site($objects);
            $site->id = $site_id;
            $site->get();
            if ($site->error) { http_response_code(500); die; }
        }
        $lecteur->site_id = $site_id;
        $lecteur->save();
        // la sauvegarde s'est bien passée
        if ($lecteur->error) { http_response_code(500); die; }
        http_response_code(200);
        header('Content-type: application/json');
        echo json_encode(array('message'=>'saved'));
        break;
    }

    case 'fromForm' :  {
        _echo(_h2("Enregistrement d'une lecteur"));
            If(! $session->isLoggedOrHasRight('lecteur')){
                return;
            }
        if ($lecteur->fromForm()) {
            if ($lecteur->save()) {
                header("Location: ". $url_base."?lecteur/saved/{$lecteur->id}");
            } else {
                _echo(HTML_error($lecteur->message));
            }
        } else {
            _echo(HTML_error($lecteur->message));
        }
    }

    case 'edit' : {
        If(! $session->isLoggedOrHasRight('lecteur')){
            return;
        }
        if ($lecteur->id == null) {
            _echo(_h2("Cr&eacute;ation d'une lecteur"));
        } else {
            _echo(_h2("Edition d'une lecteur"));
        }
        $lecteur->get();
        _echo($lecteur->HTML_form());
        break;
    }

  case 'view' : {
        _echo(_h2("Information sur un lecteur"));
        $lecteur->get();
        _echo($lecteur->HTML_view());
        _echo($lecteur->HTML_lastlogs());
        break;
  }
  
    case 'who' : {
        _echo(_h2("Accès autorisés sur un lecteur"));
        $order = (isset($url_query[3]) && $url_query[3]>'' && preg_match("~^[A-Za-z0-9\-\ ]*$~", $url_query[3]) ? $url_query[3] : null);
        $lecteur->get();
        if (! $session->is_allowed('lireBadges', $lecteur->site_id)) {
            _echo(HTML_error("Vous n'avez pas l'habilitation suffisante."));
        } else {
            _echo($lecteur->HTML_view(false));
            _echo($lecteur->HTML_who($order=='h' ? 'horaire' : 'nom'));
            // enregistrer dans le journal
            $log = (new \Badge\Model\Log($objects))->add('who', 'lecteur', $lecteur->id);
            unset($log);
            if ($lecteur->error) {
                _echo($lecteur->message);
            }
            _echo("<p>Trié <a href='?lecteur/who/{$lecteur->id}/n'>par nom</a> ".
                    "ou <a href='?lecteur/who/{$lecteur->id}/h'>par horaire</a></p>");
        }
        break;
    }
  
    // appelé depuis l'api.
    case 'search' : {
        $queryString = $_GET['queryString'];
        if (strlen($queryString)<2) { http_response_code(500); die; }

        $array = $lecteur->array_search($queryString);
        if ($lecteur->error) { http_response_code(500); die; }

        http_response_code(200);
        header('Content-type: application/json');
        echo json_encode($array);
        break;
    }

    // appelé depuis api
    case 'todrag' : {
        If(! $session->isLoggedOrHasRight('lecteur')){
            return;
        }
        // retourne du code HTML, la liste dans un tableau
        $queryString = base64_decode($_GET['queryString']);
        if (strlen($queryString)<2) { http_response_code(500); die; }
        $page =
            (isset($_GET['page']) && $_GET['page']>"" && preg_match("~^[0-9]+$~", $_GET['page']) ?  $_GET['page'] : 1);

        $queryResult = $badge->resultQuery_result($queryString);
        $array = $badge->array_results($queryResult, $page);
        if ($badge->error) { http_response_code(500); die; }

        http_response_code(200);
        header('Content-Type: text/html; charset=utf-8');
        _echo($badge->HTML_resultNav($queryResult, $page));

        if (count($array)==0) {
            _echo(HTML_message("Aucun résultat avec ce filtre."));
        } else {
            _echo(HTML_array2table($array));
        }
        break;
    }

    case 'saved' : {
        _echo(_h2("Enregistrement d'un lecteur"));
        _echo("<p>L'enregistrement s'est bien pass&eacute;.</p>");
    }

    case 'list' : {    
        _echo(_h2("Lecteurs"));
        If(count($session->droits['lireBadges'])==0){
            If(! $session->isLoggedOrHasRight('lecteur')){
                return;
            }
        }
        _echo($lecteur->HTML_interactiveList());
        //_echo("<a href='?lecteur/edit'><i class='far fa-plus-square'></i>Ajouter un lecteur</a>");
        break;
    }

} // end switch
