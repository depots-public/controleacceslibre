<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

switch ($action) {

    case 'logon' : {
        _echo(_h2("Identification"));
        _echo("<p>Vous &ecirc;tes bien identif&eacute;(e).</p>");
        _echo($session->HTML_mesGroupes());
        _echo($session->HTML_mesDroits());
        break;
    }

    case 'logout' : {
        $session->close();
        header('Location: index.php');
        break;
    }

    case 'fromForm' :  {
        _echo(_h2("Identification"));
        if ($session->fromForm()) {
            _echo("<p>Vous &ecirc;tes bien identifi&eacute;(e)</p>\n");
            if (isset($session->destUrl) && $session->destUrl>'') {
              header("Location: ". $url_base."?{$session->destUrl}");
            } else {
              header("Location: ". $url_base."?session/logon/{$session->id}");
            }
        } else {
            _echo(HTML_error($session->message));
        }
    }

    default :
    case 'login' : {
        _echo(_h2("Formulaire d'identification"));
        if ($session->isLogon()) {
            _echo(HTML_error("Vous &ecirc;tes d&eacute;j&agrave; identifi&eacute;(e)"));
            _echo("<p><a href='".$url_base."?session//logout'>"
                    . "<i class='fa fa-sign-out'></i>&nbsp;Fermer la session pour pouvoir s'identifier de nouveau</a></p>\n");
        } else {
            _echo($session->HTML_form(is_array($url_query) ? array_slice($url_query, 2) : array()));
        }
        break;
    }

} // end switch
