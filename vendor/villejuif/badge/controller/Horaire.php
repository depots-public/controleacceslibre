<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

$horaire = new \Badge\Model\Horaire($objects);
if ($id!==null) { $horaire->id = $id; }

switch ($action) {

  case 'fromForm' :  {
        If(! $session->isLoggedOrHasRight('horaire')){
            return;
        }
    _echo(_h2("Enregistrement d'un profil horaire"));
    if ($horaire->fromForm()) {
      if ($horaire->save()) {
          header("Location: ". $url_base."?horaire/saved/{$horaire->id}");
      } else {
        _echo(HTML_error($horaire->message));
      }
    } else {
      _echo(HTML_error($horaire->message));
    }
  }

    case 'edit' : {
        If(! $session->isLoggedOrHasRight('horaire')){
            return;
        }
        if ($horaire->id == null) {
            _echo(_h2("Cr&eacute;ation d'un profil horaire"));
        } else {
            _echo(_h2("Edition d'un horaire"));
        }
        $horaire->get();
        _echo($horaire->HTML_form());
        break;
    }

  case 'delform' :
        If(! $session->isLoggedOrHasRight('horaire')){
            return;
        }
        _echo(_h2("Suppression d'un horaire"));
        if ($horaire->id == null) {
            _echo(HTML_error("Pas de numéro d'horaire"));
        }
        $horaire->get();
        if ($horaire->is_used()) {
            _echo(HTML_error("Horaire utilisé, impossible de le supprimer"));
        } else {
            _echo($horaire->HTML_delform());
        }
        break;
  
  // appelé depuis l'api.
  case 'search' : {
    $queryString = $_GET['queryString'];
    if (strlen($queryString)<2) { http_response_code(500); die; }

    $array = $horaire->array_search($queryString);
    if ($horaire->error) { http_response_code(500); die; }

    http_response_code(200);
    header('Content-type: application/json');
    echo json_encode($array);
    break;
  }

  case 'view' : {
    _echo(_h2("Profil horaire"));
    $horaire->get();
    _echo($horaire->HTML_view());
    _echo($horaire->HTML_lastlogs());
    break;
  }

    case 'fromDelForm' :
        If(! $session->isLoggedOrHasRight('horaire')){
            return;
        }
        _echo(_h2("Suppression d'un profil horaire"));
        if ($horaire->fromDelForm()) {
            if ($horaire->delete()) {
                header("Location: ". $url_base."?horaire/deleted/{$horaire->id}");
            } else {
                _echo(HTML_error($horaire->message));
            }
        } else {
            _echo(HTML_error($horaire->message));
        }

    case 'deleted' : if ($action == 'deleted') {
        _echo(_h2("Suppression d'un profil horaire"));
        _echo(HTML_message("La suppresison s'est bien pass&eacute;e."));
        // no break
    }

    case 'saved' : if ($action == 'saved') {
        _echo(_h2("Enregistrement d'un profil horaire"));
        _echo(HTML_message("L'enregistrement s'est bien pass&eacute;."));
        // no break
    }

  default :
  case 'list' : {
        If(! $session->isLoggedOrHasRight('horaire')){
            return;
        }
    _echo(_h2("Profils horaires"));
    _echo($horaire->HTML_list());
    _echo("<a href='?horaire/edit'><i class='far fa-plus-square'></i>Ajouter un profil horaire</a>\n");
    break;
  }

} // end switch
