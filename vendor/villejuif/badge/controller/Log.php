<?php
/*
 * GestionDeBadges
 * 
 * Copyright 2014-2020 Loïc Dayot <ldayot CHEZ epnadmin POINT net> Primavera-ESI
 * 2020 MORVAN Jean-Philippe <jp-morvan CHEZ villejuif POINT fr> ville de Villejuif
 * 2020-2022 DAYOT Loïc <l-dayot CHEZ villejuif POINT fr> ville de Villejuif
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

If(! $session->isLoggedOrHasRight('log')){
    return;
}

$log = new Badge\Model\Log($objects);
if ($id !== null) $log->id = $id;

switch ($action) {

  case 'view' : {
    _echo(_h2("Logs de l'application"));
    $log->get();
    if ($log->error) {
      _echo(HTML_error($log->message));
    } else {
      _echo("<p><a class='button' href='?log/view/". ($log->id -1). "'>Précédente</a>");
      _echo("<a class='button' href='?log/view/". ($log->id +1). "'>Suivante</a></p>");
      _echo($log->HTML_view());
    }
    break;
  }

  // appelé depuis api
  case 'apiGet' : {
    http_response_code(200);
    header('Content-Type: text/html; charset=utf-8');
    $log->get();
    if ($log->error) {
      _echo(HTML_error($log->message));
    } else {
      _echo($log->HTML_view());
    }
    break;
  }

  case 'list' : {
    _echo(_h2("Logs de l'application"));
    if ($_POST) {
        $log->fromSearchForm();
    }
    _echo($log->HTML_searchForm());
    if ($_POST) {
        _echo($log->HTML_searchResult());
    }
    break;
  }

} // end switch
