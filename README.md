# Contrôle d'accès libre

* [Licence libre](LICENSE.md)
* [Présentation et utilisation](README.md)
* [Illustrations et copies d'écran](SCREENSHOT.md)
* [Installation](INSTALL.md)

<h1>Objectif de cette application web</h1>

L'application ControleAccesLibre présente une interface web pour la gestion
des droits d'accès à des locaux par badges.

Elle s'adosse à l'application SenatorFX et sa base de données MsSQL pour
permettre une gestion des droits plus souple que l'application du fabriquant.
Elle a été réalisée grâce à l'éditeur Eden Innovations qui nous a fourni une
documentation pour interagir avec la base de données. La documentation ne peut
être redistribuée, à chaque client le soin de la lui demander. Nous nous sommes 
appuyés sur la version 1.4 de la documentation.

La principale caractéristique est la possibilité de s'abstraire des "profils" 
de droits d'accès pour régler directement les droits d'accès ce qui est plus
compréhensible pour un utilisateur et permet une grande souplesse.

L'application ControleAccesLibre ne remplace pas complètement SenatorFX qui est 
toujours nécessaire, notamment pour configurer les lecteurs de badges lors 
de leur installation et l'enregistrement des badges.

Elle est placée sur licence libre GNU-AGLPv3 pour en faciliter l'utilisation
et l'amélioration par les utilisateurs. Merci de rapporter les problèmes ou
idées sur https://gitlab.villejuif.fr/depots-public/controleacceslibre/-/issues


<h1>Installation</h1>

Voir <a href='INSTALL.md'>Installation</a>.


<h1>Accès à l'application</h1>

Dans un navigateur, lancez https://badges.mondomaine/
Identifiez-vous avec un compte LDAP appartenant au groupe APP_BADGES_TOTAL ou 
APP_BADGES_VISU.

Configurez ensuite :
* les Sites et leurs responsables
* les Lecteurs (à associer dans des Sites)
* les Habilitations

Voilà, c'est prêt.


<h1>Quelques précautions</h1>

L'application interagit avec la base de données de SenatorFX. Par conséquent,
vérifiez bien que la version utilisée de ControleAccesLibre est faite pour la
version de SenatorFX que vous utilisez.
Attention si vous apportez des modifications au code, car il y a un risque de
perturber SenatorFX.
Attention également lors d'une mise à jour de SenatorFX, renseignez vous sur
les modifications dans la structure de la base de données.
Comme indiqué dans la licence GNU-AGPL, les auteurs ne peuvent être tenus pour
responsabilité des effets de l'utilisation de l'application.

Dès qu'un nouveau lecteur est créé, nécessairement dans SenatorFX, il convient
de le configurer dans ControleAccesLibre.

Les Badges doivent être créés en premier dans SenatorFX, avant de pouvoir y 
accéder dans l'application pour l'activer, le désactiver et lui attribuer 
des droits.

L'application inclue les Logs des actions réalisées en son sein. On n'y voit
pas les actions réalisées dans SenatorFX.
En revanche, le Journal des accès par badge est celui de SenatorFX, donc tous 
les accès s'y trouvent comme dans Senator FX.


<h1>Les particularités de ControleAccesLibre</h1>

<h2>Les apports</h2>

Il est possible de modifier des accès à tel ou tel Lecteur pour un ensemble de
Badges, alors même que ces derniers n'ont pas 

L'application permet de décentraliser la gestion ou la consultation des 
Habilitations et du Journal d'accès à des tiers, sur la base des Sites.

La saisie du courriel du responsable d'un Site permet de lui envoyer 
quotidiennement les changements d'habilitations opérés la journée précédente
pour un contrôle a posteriori.

On peut consulter tous les Badges ayant accès à tel Lecteur, par nom du porteur
et par Horaires (jours-heures).

<h2>Les limites</h2>

La création des Badges et des Lecteurs doit se faire dans SenatorFX.

La programmation de droits sur des badges verrouille ces derniers jusqu'à ce que
la période de programmation soit passée.